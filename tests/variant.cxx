#include <algorithm>
#include <iterator>
#include <string>
#include <variant>
#include <vector>

#include <nxx/algorithm.hxx>
#include <nxx/variant.hxx>

#include "testing.hxx"

using Catch::Matchers::Equals;
using namespace std::literals;


class S {
public:
	S() = default;
	explicit S(int i)
	    : m_v{i} {}
	explicit S(double d)
	    : m_v{d} {}

	auto v() const -> std::variant<int, double> const& { return m_v; };

	void update() {
		auto vis = nxx::visitor{

		    [this](int& i) {
			    i += 3;
			    if (i > 10)
				    m_v = 7.5;
		    },

		    [this](double& d) {
			    d /= 2.0;
			    if (d < 0.5)
				    m_v = 1;
		    },

		};
		std::visit(vis, m_v);
	}

private:
	std::variant<int, double> m_v{};
};


SCENARIO("A visitor can wrap a functor", "[variant][visitor]") {
	GIVEN("a vector of variants of int and double")
	AND_GIVEN("a generic lambda to double input") {
		auto const vs = std::vector<std::variant<int, double>>{1, 10, 12.25, 32.0, -54, -200.125};
		auto constexpr vis = nxx::visitor{[](auto arg) -> double { return arg * 2.0; }};

		WHEN("visited using the visitor") {
			auto output = std::vector<double>{};
			std::transform(std::begin(vs), std::end(vs), std::back_inserter(output),
			               [&](auto&& v) { return std::visit(vis, v); });

			THEN("the results should all be doubled")
			REQUIRE_THAT(output, Equals(std::vector{2.0, 20.0, 24.5, 64.0, -108.0, -400.25}));
		}
	}
}

SCENARIO("A visitor can wrap multiple functors", "[variant][visitor]") {
	GIVEN("a vector of variants of int, double, and strings")
	AND_GIVEN("a set of lambdas that tripple ints, half doubles, and leave strings") {
		auto const vs = std::vector<std::variant<int, double, std::string>>{
		    1, 10, "fourteen"s, 12.25, "one hundred and twenty"s, 32.0, -54, -200.125, "negative three"};
		auto constexpr vis = nxx::visitor{[](int arg) -> std::string { return std::to_string(arg * 3); },
		                                  [](double arg) -> std::string { return std::to_string(arg / 2.0); },
		                                  [](std::string const& arg) -> std::string { return arg; }};

		WHEN("visited using the visitor") {
			auto output = std::vector<std::string>{};
			std::transform(std::begin(vs), std::end(vs), std::back_inserter(output),
			               [&](auto&& v) { return std::visit(vis, v); });

			THEN("the results should all be doubled")
			REQUIRE_THAT(output,
			             Equals(std::vector{"3"s, "30"s, "fourteen"s, "6.125000"s, "one hundred and twenty"s,
			                                "16.000000"s, "-162"s, "-100.062500"s, "negative three"s}));
		}
	}
}

SCENARIO("A visitor can wrap lambdas capturing this-ptr", "[variant][visitor]") {
	GIVEN("a class with a variant member variable")
	AND_GIVEN("a member function that can change that member variable's alternative") {
		auto s = S{-1};

		WHEN("the object is visited in a manor that will change the alternative") {
			auto output = std::vector<double>{};
			auto extract = [](auto const& v) -> double { return v; };
			output.push_back(std::visit(extract, s.v()));
			nxx::repeat_n(10, [&] {
				s.update();
				output.push_back(std::visit(extract, s.v()));
			});

			THEN("the variant should change accordingly")
			REQUIRE_THAT(output, Equals(std::vector<double>{-1, 2, 5, 8, 7.5, 3.75, 1.875, 0.9375, 1, 4, 7}));
		}
	}
}
