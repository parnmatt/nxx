#include <iostream>
#include <sstream>

#include <nxx/ostream.hxx>

#include "testing.hxx"

using namespace std::literals;


// nxx::ostream::redirect

SCENARIO("The stream redirector switches from stdout to ostringstream", "[iostream][ostream][redirect]") {
	GIVEN("a ostringstream") {
		std::ostringstream ss{};

		WHEN("the user sets the redirection target for stdout to the ostringstream")
		AND_WHEN("the user streams some data, a string, and a few ints with separation chars") {
			nxx::ostream::redirect rs{std::cout, ss};
			std::cout << "this is a test " << 123 << ' ' << 9876543210 << '\t' << 7 << '\n';

			THEN("all output should be within the ostringstream") {
				REQUIRE(ss.str() == "this is a test 123 9876543210\t7\n"s);
			}
		}
	}
}

SCENARIO("The stream redirector switches back to the original stream", "[iostream][ostream][redirect]") {
	GIVEN("two ostringstreams, with the user streaming some text to the first")
	AND_GIVEN("the user redirects and streams some tested to the second") {
		std::ostringstream core{}, alt{};
		core << "Hello World!\n";

		WHEN("the redirector goes out of scope") {
			{
				nxx::ostream::redirect rs{core, alt};
				core << "What a nice day!\n";
			}  // rs goes out of scope

			THEN("the stream should switch back to the original stream") {
				core << "How are you?\n";

				REQUIRE(core.str() == "Hello World!\nHow are you?\n"s);
				REQUIRE(alt.str() == "What a nice day!\n"s);
			}
		}
	}
}
