#include <algorithm>
#include <iterator>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include <nxx/iterator.hxx>

#include "testing.hxx"

using namespace std::literals;
using Catch::Matchers::Equals;


// nxx::iterator::blackhole

SCENARIO("blackhole discards input assigned to it", "[iterator][blackhole]") {
	GIVEN("a vector with some items") {
		auto const v = std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

		WHEN("the items are copied into a blackhole") {
			std::copy(std::begin(v), std::end(v), nxx::iterator::blackhole{});

			THEN("the input should be discarded") { SUCCEED("the output was correctly discarded"); }
		}
	}
}


// nxx::iterator::prefix_ostream

SCENARIO("Prefix output streams pass through with no delimiter", "[iterator][ostream][prefix_ostream]") {
	GIVEN("a vector with some items") {
		auto const v = std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

		WHEN("the items are streamed with no prefix delimiter") {
			std::ostringstream ss;

			std::copy(std::begin(v), std::end(v),
			          nxx::iterator::prefix_ostream<typename decltype(v)::value_type>{ss});

			THEN("the streamed output will be just as if streamed directly") {
				REQUIRE(ss.str() == "0123456789"s);
			}
		}
	}
}

SCENARIO("Prefix output streams interweave input with a delimiter", "[iterator][ostream][prefix_ostream]") {
	GIVEN("a vector with some items") {
		auto const v = std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

		WHEN("the items are streamed with a prefix delimiter") {
			std::ostringstream ss;

			std::copy(std::begin(v), std::end(v),
			          nxx::iterator::prefix_ostream<typename decltype(v)::value_type>{ss, ","});

			THEN("the streamed output will be interweaved with the delimiter, with the delimiter "
			     "starting the stream, and the last input ending the stream") {
				REQUIRE(ss.str() == ",0,1,2,3,4,5,6,7,8,9"s);
			}
		}
	}
}


// nxx::iterator::infix_ostream

SCENARIO("Infix output streams pass through with no delimiter", "[iterator][ostream][infix_ostream]") {
	GIVEN("a vector with some items") {
		auto const v = std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

		WHEN("the items are streamed with no infix delimiter") {
			std::ostringstream ss;

			std::copy(std::begin(v), std::end(v),
			          nxx::iterator::infix_ostream<typename decltype(v)::value_type>{ss});

			THEN("the streamed output will be just as if streamed directly") {
				REQUIRE(ss.str() == "0123456789"s);
			}
		}
	}
}

SCENARIO("Infix output streams interweave input with a delimiter", "[iterator][ostream][infix_ostream]") {
	GIVEN("a vector with some items") {
		auto const v = std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

		WHEN("the items are streamed with an infix delimiter") {
			std::ostringstream ss;

			std::copy(std::begin(v), std::end(v),
			          nxx::iterator::infix_ostream<typename decltype(v)::value_type>{ss, ","});

			THEN("the streamed output will be interweaved with the delimiter, with the delimiter "
			     "only being in between the input") {
				REQUIRE(ss.str() == "0,1,2,3,4,5,6,7,8,9"s);
			}
		}
	}
}


// nxx::iterator::postfix_ostream

SCENARIO("Postfix output streams pass through with no delimiter", "[iterator][ostream][postfix_ostream]") {
	GIVEN("a vector with some items") {
		auto const v = std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		WHEN("the items are streamed with no postfix delimiter") {
			std::ostringstream ss;

			std::copy(std::begin(v), std::end(v),
			          nxx::iterator::postfix_ostream<typename decltype(v)::value_type>{ss});

			THEN("the streamed output will be just as if streamed directly") {
				REQUIRE(ss.str() == "0123456789"s);
			}
		}
	}
}

SCENARIO("Postfix output streams interweave input with a delimiter", "[iterator][ostream][postfix_ostream]") {
	GIVEN("a vector with some items") {
		auto const v = std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

		WHEN("the items are streamed with a postfix delimiter") {
			std::ostringstream ss;

			std::copy(std::begin(v), std::end(v),
			          nxx::iterator::postfix_ostream<typename decltype(v)::value_type>{ss, ","});

			THEN("the streamed output will be interweaved with the delimiter, with the first input "
			     "starting the stream, and a delimiter ending the stream") {
				REQUIRE(ss.str() == "0,1,2,3,4,5,6,7,8,9,"s);
			}
		}
	}
}


// nxx::iterator::suffix_ostream

SCENARIO("Suffix output streams pass through with no delimiter", "[iterator][ostream][suffix_ostream]") {
	GIVEN("a vector with some items") {
		auto const v = std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		WHEN("the items are streamed with no suffix delimiter") {
			std::ostringstream ss;

			std::copy(std::begin(v), std::end(v),
			          nxx::iterator::suffix_ostream<typename decltype(v)::value_type>{ss});

			THEN("the streamed output will be just as if streamed directly") {
				REQUIRE(ss.str() == "0123456789"s);
			}
		}
	}
}

SCENARIO("Suffix output streams interweave input with a delimiter", "[iterator][ostream][suffix_ostream]") {
	GIVEN("a vector with some items") {
		auto const v = std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		WHEN("the items are streamed with a suffix delimiter") {
			std::ostringstream ss;

			std::copy(std::begin(v), std::end(v),
			          nxx::iterator::suffix_ostream<typename decltype(v)::value_type>{ss, ","});

			THEN("the streamed output will be interweaved with the delimiter, with "
			     "the first input "
			     "starting the stream, and a delimiter ending the stream") {
				REQUIRE(ss.str() == "0,1,2,3,4,5,6,7,8,9,"s);
			}
		}
	}
}


// nxx::iterator::insert

SCENARIO("Insert iterators, insert what is given to them into the given container", "[iterator][insert]") {
	GIVEN("a vector with some items") {
		auto const v = std::vector{9, 3, 1, 4, 0, 6, 5, 2, 8, 7};

		WHEN("inserting into a set") {
			auto output = std::set<typename decltype(v)::value_type>{};
			std::copy(std::begin(v), std::end(v), nxx::iterator::insert{output});

			THEN("the set should contain the values in sorted order") {
				REQUIRE((output == std::set{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}));
			}
		}
	}
}

// nxx::iterator::positional_insert

SCENARIO("Positional insert iterators, insert what is given to them into the given container at the given "
         "position",
         "[iterator][insert][positional_insert]") {
	GIVEN("a vector with some items")
	AND_GIVEN("a vector initialised with padding iterms") {
		auto const v = std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		auto output = std::vector<typename decltype(v)::value_type>(4, 99);

		WHEN("inserting into the second vector from the midpoint") {
			std::copy(std::begin(v), std::end(v),
			          nxx::iterator::positional_insert(output, std::begin(output) + output.size() / 2));

			THEN("the vector should contain the values in the original order, surrounded by the padding "
			     "items") {
				REQUIRE_THAT(output, Equals(std::vector{99, 99, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 99, 99}));
			}
		}
	}
}


// nxx::iterator::filter_output_if

SCENARIO("Output iterators can be filtered using a unary predicate", "[iterator][filter][filter_output_if]") {
	GIVEN("a vector of items to be filtered")
	AND_GIVEN("a predicate to filter odd numbers") {
		auto const v = std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		auto output = std::vector<typename decltype(v)::value_type>{};
		// predicate inline

		WHEN("copied and filtered through the predicate") {
			std::copy(std::begin(v), std::end(v),
			          nxx::iterator::filter_output_if{std::back_inserter(output),
			                                          [](auto v) -> bool { return v & 1; }});

			THEN("the resulting vector will only contain the odd values") {
				REQUIRE_THAT(output, Equals(std::vector{1, 3, 5, 7, 9}));
			}
		}
	}
}

// nxx::iterator::filter_output

SCENARIO("Output iterators can be filtered against the 'trueness' of the value itself",
         "[iterator][filter][filter_output]") {
	GIVEN("a vector of optional items to be filtered") {
		auto const v = std::vector<std::optional<int>>{std::nullopt, 0, 1, 2, std::nullopt, 3, 4, 5, 6,
		                                               std::nullopt, 7, 8, 9, std::nullopt};
		auto output = std::vector<typename decltype(v)::value_type>{};

		WHEN("copied and filtered using the value itself") {
			std::copy(std::begin(v), std::end(v), nxx::iterator::filter_output(std::back_inserter(output)));

			THEN("the resulting vector will only contain the optional values with values") {
				REQUIRE_THAT(output, Equals(std::vector<std::optional<int>>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}));
			}
		}
	}
}


// nxx::iterator::unwrap_optional

SCENARIO("Output iterators of std::optionals can be filtered and unwrapped to their value types",
         "[iterator][filter][unwrap_optional]") {
	GIVEN("a vector of optional items to be unwrapped") {
		auto const v = std::vector<std::optional<int>>{std::nullopt, 0, 1, 2, std::nullopt, 3, 4, 5, 6,
		                                               std::nullopt, 7, 8, 9, std::nullopt};
		auto output = std::vector<typename decltype(v)::value_type::value_type>{};

		WHEN("copied into the output through the unwrapper") {

			std::copy(std::begin(v), std::end(v), nxx::iterator::unwrap_optional(std::back_inserter(output)));
			THEN("the resulting vector will only contain the values of those optionals with values") {
				REQUIRE_THAT(output, Equals(std::vector{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}));
			}
		}
	}
}
