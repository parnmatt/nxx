#include <cstdlib>
#include <optional>

#include <nxx/stdlib.hxx>

#include "testing.hxx"

SCENARIO("User can get a variable from the environment", "[stdlib][getenv]") {
	GIVEN("an existing environment variable HOME") {
		// HOME inline

		WHEN("the user requests the variable from the environment") {
			auto const var = nxx::getenv("HOME");

			THEN("the variable should correspond to the users HOME directory") {
				REQUIRE(var.has_value());
				REQUIRE_NOTHROW(var.value() == std::getenv("HOME"));
			}
		}
	}
}

SCENARIO("Non-existent environment variables are empty", "[stdlib][getenv]") {
	GIVEN("the user attempting to retrieve a non-existent environment variable, NXX_FAKE_ENV_VAR") {
		auto const var = nxx::getenv("NXX_FAKE_ENV_VAR");

		WHEN("the user queries the value") {
			// inline

			THEN("the user will find it is empty") {
				REQUIRE_FALSE(var.has_value());
				REQUIRE_THROWS_AS(var.value(), std::bad_optional_access);
			}
		}
	}
}
