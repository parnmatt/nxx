#include <iostream>
#include <sstream>
#include <string>

#include <nxx/algorithm.hxx>

#include "testing.hxx"

using namespace std::literals;

void print_x_to_cout() { std::cout << 'x'; }

class BadCounter {
public:
	BadCounter() = default;
	void operator()() { ++m_count; };
	auto get() const { return m_count; };

private:
	unsigned long long m_count{};
};


// std::repeat_n

SCENARIO("repeat_n repeatedly calls a given function pointer", "[algorithm][repeat_n]") {
	GIVEN("a function that prints 'x' to std::cout") {
		// print_x_to_cout

		WHEN("the user calls for 10 repetitions") {
			// switch cout for something in-memory
			std::ostringstream ss{};
			auto buf = std::cout.rdbuf();
			std::cout.rdbuf(ss.rdbuf());

			nxx::repeat_n(10, print_x_to_cout);

			// switch back
			std::cout.rdbuf(buf);

			THEN("10 'x's should be printed") { REQUIRE(ss.str() == "xxxxxxxxxx"s); }
		}
	}
}

SCENARIO("repeat_n repeatedly calls a given functor", "[algorithm][repeat_n]") {
	GIVEN("a functor that increments an internal counter when called") {
		auto c = BadCounter{};

		WHEN("the user calls for 10 repetitions") {
			nxx::repeat_n(10, c);

			THEN("the internal counter should be 10") { REQUIRE(c.get() == 10); }
		}
	}
}

SCENARIO("repeat_n repeatedly calls a given lambda", "[algorithm][repeat_n]") {
	GIVEN("a lambda that doubles a variable in the parent scope")
	AND_GIVEN("a value of 1") {
		auto value = 1;
		// lambda inline

		WHEN("the user calls for 10 repetitions") {
			nxx::repeat_n(10, [&] { value *= 2; });

			THEN("the variable should now be 1024") { REQUIRE(value == 1024); }
		}
	}
}
