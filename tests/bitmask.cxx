#include <type_traits>

#include <nxx/bitmask.hxx>

#define NXX_USE_STATIC_REQUIRE
#include "testing.hxx"

enum class bad_bm : int {
	a = -1,
	b = 0,
	c = 1,
};

enum class disabled_bm : unsigned int {
	r = 0b100,
	w = 0b010,
	x = 0b001,
};

enum class bm : unsigned int {
	r = 0b100,
	w = 0b010,
	x = 0b001,
};

NXX_ENABLE_AS_BITMASK(bm);

class fake_file {
public:
	struct permissions {
		bm u, g, a;
	};

	using bm_t = std::underlying_type_t<bm>;

	constexpr fake_file() { chmod(0644); }
	void constexpr chmod(bm_t octal_mode) {
		m_p = {bm{(octal_mode & 0700) >> 6}, bm{(octal_mode & 0070) >> 3}, bm{octal_mode & 007}};
	}

	void constexpr add(bm mode) {
		m_p.u |= mode;
		m_p.g |= mode;
		m_p.a |= mode;
	}

	auto constexpr perms() const -> bm_t {
		return static_cast<bm_t>(m_p.u) << 6 | static_cast<bm_t>(m_p.g) << 3 | static_cast<bm_t>(m_p.a);
	}

private:
	permissions m_p{};
};


TEST_CASE("bad_bm is not a valid bitmask", "[bitmask]") {
	STATIC_REQUIRE_FALSE(nxx::bitmask::traits::is_bitmask_v<bad_bm>);
}

TEST_CASE("disabled_bm is a possible bitmask, but invalid", "[bitmask]") {
	STATIC_REQUIRE(nxx::bitmask::traits::is_possible_bitmask_v<disabled_bm>);
	STATIC_REQUIRE_FALSE(nxx::bitmask::traits::is_bitmask_v<disabled_bm>);
}

TEST_CASE("bm is a valid bitmask", "[bitmask]") { STATIC_REQUIRE(nxx::bitmask::traits::is_bitmask_v<bm>); }

TEST_CASE("AND'ing two bitmasks returns common bits", "[bitmask]") {
	STATIC_REQUIRE((bm{0b10101} & bm{0b10011}) == bm{0b10001});
}

TEST_CASE("OR'ing two bitmasks returns all set bits", "[bitmask]") {
	STATIC_REQUIRE((bm{0b10101} | bm{0b10011}) == bm{0b10111});
}

TEST_CASE("XOR'ing two bitmasks returns all bits not incommon", "[bitmask]") {
	STATIC_REQUIRE((bm{0b10101} ^ bm{0b10011}) == bm{0b00110});
}

TEST_CASE("NOT'ing a bitmask returns flips all bits", "[bitmask]") {
	STATIC_REQUIRE(~bm{0b10101} == bm{~static_cast<std::underlying_type_t<bm>>(0b10101)});
}

TEST_CASE("AND-assign'ing a bitmask keeps common bits", "[bitmask]") {
	bm x{0b10101};
	x &= bm{0b10011};
	REQUIRE(x == bm{0b10001});
}

TEST_CASE("OR-assign'ing a bitmask keeps all set bits", "[bitmask]") {
	bm x{0b10101};
	x |= bm{0b10011};
	REQUIRE(x == bm{0b10111});
}

TEST_CASE("XOR-assign'ing a bitmask keeps all bits not in common", "[bitmask]") {
	bm x{0b10101};
	x ^= bm{0b10011};
	REQUIRE(x == bm{0b00110});
}

SCENARIO("A bitmask can be used for file permissions", "[bitmask]") {
	GIVEN("a file on a filesystem") {
		// Gedankenexperiment
		auto file = fake_file{};
		CHECK(file.perms() == 0644);

		WHEN("the user adds execution mode") {
			file.add(bm::x);

			THEN("the file permissions should read as 0755") { REQUIRE(file.perms() == 0755); }
		}
	}
}

TEST_CASE("Can test if bit is set", "[bitmask]") {
	bm const rx{0b101};
	REQUIRE(nxx::bitmask::test(rx, bm::r));
	REQUIRE_FALSE(nxx::bitmask::test(rx, bm::w));
	REQUIRE(nxx::bitmask::test(rx, bm::x));
}

TEST_CASE("Can construct a bitmask from others", "[bitmask]") {
	REQUIRE((nxx::bitmask::construct(bm::x, bm::w, bm::r, bm::x, bm::r, bm::w, bm::r) == bm{0b111}));
}

TEST_CASE("Can set bits using other bitmasks", "[bitmask]") {
	bm x = bm::r;
	nxx::bitmask::set(x, bm::x, bm::r);
	REQUIRE(x == bm{0b101});
}

TEST_CASE("Can unset bits using other bitmasks", "[bitmask]") {
	bm x{0b111};
	nxx::bitmask::unset(x, bm::w, bm::x);
	REQUIRE(x == bm{0b100});
}
