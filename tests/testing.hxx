#ifndef NXX__TESTING_HXX
#define NXX__TESTING_HXX

#define NXX_NOOP (void)(0)

#include <catch2/catch.hpp>

#ifndef CATCH_CONFIG_DISABLE
#	ifdef CATCH_CONFIG_PREFIX_ALL

#		define CATCH_AND_GIVEN(desc) INTERNAL_CATCH_DYNAMIC_SECTION("And given: " << desc)

#		ifdef NXX_USE_STATIC_REQUIRE
#			define CATCH_STATIC_REQUIRE(expr)                                                               \
				static_assert(expr, #expr);                                                                  \
				CATCH_SUCCEED(#expr)
#			define CATCH_STATIC_REQUIRE_FALSE(expr)                                                         \
				static_assert(!(expr), "!(" #expr ")");                                                      \
				CATCH_SUCCEED("!(" #expr ")")
#		else  // ifdef NXX_USE_STATIC_REQUIRE
#			define CATCH_STATIC_REQUIRE(expr) CATCH_REQUIRE(expr)
#			define CATCH_STATIC_REQUIRE_FALSE(expr) CATCH_REQUIRE_FALSE(expr)
#		endif  // ifdef NXX_USE_STATIC_REQUIRE

#	else  // ifdef CATCH_CONFIG_PREFIX_ALL

#		define AND_GIVEN(desc) INTERNAL_CATCH_DYNAMIC_SECTION("And given: " << desc)

#		ifdef NXX_USE_STATIC_REQUIRE
#			define STATIC_REQUIRE(expr)                                                                     \
				static_assert(expr, #expr);                                                                  \
				SUCCEED(#expr)
#			define STATIC_REQUIRE_FALSE(expr)                                                               \
				static_assert(!(expr), "!(" #expr ")");                                                      \
				SUCCEED("!(" #expr ")")
#		else  // ifdef NXX_USE_STATIC_REQUIRE
#			define STATIC_REQUIRE(expr) REQUIRE(expr)
#			define STATIC_REQUIRE_FALSE(expr) REQUIRE_FALSE(expr)
#		endif  // ifdef NXX_USE_STATIC_REQUIRE

#	endif  // ifdef CATCH_CONFIG_PREFIX_ALL
#else       // ifndef CATCH_CONFIG_DISABLE
#	ifdef CATCH_CONFIG_PREFIX_ALL

#		define CATCH_AND_GIVEN(desc)

#		define CATCH_STATIC_REQUIRE(expr) NXX_NOOP
#		define CATCH_STATIC_REQUIRE_FALSE(expr) NXX_NOOP

#	else  // ifdef CATCH_CONFIG_PREFIX_ALL

#		define AND_GIVEN(desc)

#		define STATIC_REQUIRE(expr) NXX_NOOP
#		define STATIC_REQUIRE_FALSE(expr) NXX_NOOP

#	endif  // ifdef CATCH_CONFIG_PREFIX_ALL
#endif      // ifndef CATCH_CONFIG_DISABLE

#endif  // ifndef NXX__TESTING_HXX
