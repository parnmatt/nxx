#include <memory>

#include <nxx/utility.hxx>

#include "testing.hxx"


// nxx::fake_ptr

SCENARIO("fake_ptr holds a value type", "[utility][fake_ptr]") {
	GIVEN("an int with a value of 4") {
		// int inline

		WHEN("constructed") {
			nxx::fake_ptr fp{4};

			THEN("the contained value should be 4") { REQUIRE(*fp == 4); }
		}
	}
}

SCENARIO("fake_ptr holds a pointer type", "[utility][fake_ptr]") {
	GIVEN("a int* pointing to a value of 4") {
		auto p = std::make_unique<int>(4);

		WHEN("the pointer points to an int of 4") {
			nxx::fake_ptr fp{p.get()};

			THEN("the contained value should be 4") { REQUIRE(*fp == 4); }
		}
	}
}
