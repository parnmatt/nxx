#ifndef NXX__META_HXX
#define NXX__META_HXX

#include <type_traits>

#include <nxx/type_traits.hxx>

namespace nxx::meta {

	template <typename...>
	struct pack {};

	template <typename T, typename... Ts>
	struct unique_extend_pack {
		using type = std::conditional_t<traits::contains_v<T, Ts...>, pack<Ts...>, pack<Ts..., T>>;
	};

	template <typename... Ts>
	using unique_extend_pack_t = typename unique_extend_pack<Ts...>::type;

	template <typename T, typename... Ts>
	struct unique_extend_pack<T, pack<Ts...>> : unique_extend_pack<T, Ts...> {};

	template <typename T, typename... Ts>
	struct unique_extend_pack<pack<T>, pack<Ts...>> : unique_extend_pack<T, Ts...> {};

	template <typename T, typename... Tsl, typename... Tsr>
	struct unique_extend_pack<pack<T, Tsl...>, pack<Tsr...>>
	    : unique_extend_pack<pack<Tsl...>, unique_extend_pack_t<T, Tsr...>> {};

	template <typename... Ts>
	struct make_unique_pack {
		using type = unique_extend_pack_t<pack<Ts...>, pack<>>;
	};

	template <typename... Ts>
	using make_unique_pack_t = typename make_unique_pack<Ts...>::type;

	template <typename... Ts>
	struct make_unique_pack<pack<Ts...>> : make_unique_pack<Ts...> {};

}  // namespace nxx::meta

#endif  // ifndef NXX__META_HXX
