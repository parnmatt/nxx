#ifndef NXX__STRONG_TYPEDEF_HXX
#define NXX__STRONG_TYPEDEF_HXX

#include <type_traits>
#include <utility>

namespace nxx {

	template <typename Tag, typename T>
	class strong_typedef {
	public:
		using value_type = T;

		constexpr strong_typedef() = default;

		explicit constexpr strong_typedef(value_type const& value)
		    : m_value{value} {}

		explicit constexpr strong_typedef(value_type&& value) noexcept(
		    std::is_nothrow_move_constructible_v<value_type>)
		    : m_value{std::move(value)} {}

		template <typename... Ts, std::enable_if_t<std::is_constructible_v<value_type, Ts...>, int> = 0>
		explicit constexpr strong_typedef(Ts&&... ts) noexcept(
		    std::is_nothrow_constructible_v<value_type, Ts...>)
		    : m_value{std::forward<Ts>(ts)...} {}

		auto constexpr operator*() & -> value_type& { return m_value; }
		auto constexpr operator*() const& -> value_type const& { return m_value; }
		auto constexpr operator*() && -> value_type&& { return std::move(m_value); }
		auto constexpr operator*() const&& -> value_type const&& { return std::move(m_value); }

		auto constexpr operator-> () & -> value_type* { return &**this; }
		auto constexpr operator-> () const& -> value_type const* { return &**this; }
		auto constexpr operator-> () && -> value_type* { return &**this; }
		auto constexpr operator-> () const&& -> value_type const* { return &**this; }

		constexpr operator value_type&() & noexcept { return m_value; }
		constexpr operator value_type const&() const& noexcept { return m_value; }
		constexpr operator value_type &&() && noexcept { return std::move(m_value); }
		constexpr operator value_type const &&() const&& noexcept { return std::move(m_value); }

		friend void swap(strong_typedef& lhs, strong_typedef& rhs) noexcept {
			std::swap(static_cast<value_type&>(lhs), static_cast<value_type&>(rhs));
		}

	private:
		value_type m_value{};
	};

}  // namespace nxx

#endif  // ifndef NXX__STRONG_TYPEDEF_HXX
