#ifndef NXX__TUPLE_FWD_HXX
#define NXX__TUPLE_FWD_HXX

#include <cstddef>
#include <tuple>
#include <type_traits>
#include <utility>

#include <nxx/functional_fwd.hxx>
#include <nxx/type_traits.hxx>
#include <nxx/utility_fwd.hxx>

namespace nxx::tuple {
	namespace traits {
		template <std::size_t I, typename Tuple>
		struct get
		    : nxx::traits::apply_reference<Tuple, std::tuple_element_t<I, std::remove_reference_t<Tuple>>> {};

		template <std::size_t I, typename Tuple>
		using get_t = typename get<I, Tuple>::type;

		template <typename Tuple>
		using make_index_sequence_from_tuple
		    = util::make_index_sequence_from_range<0, std::tuple_size_v<std::remove_reference_t<Tuple>>>;

	}  // namespace traits


	template <typename... Args>
	auto constexpr make(Args&&...) noexcept(std::is_nothrow_constructible_v<std::tuple<Args...>, Args...>)
	    -> std::tuple<Args...>;

	// tuple from indices from tuple-like

	template <typename Tuple, std::size_t... Is>
	using from_index_sequence = std::tuple<traits::get_t<Is, Tuple>...>;

	namespace detail {
		template <typename, typename>
		struct from_index_range;

		template <typename Tuple, typename Seq>
		using from_index_range_t = typename from_index_range<Tuple, Seq>::type;

		template <typename Tuple, std::size_t... Is>
		struct from_index_range<Tuple, std::index_sequence<Is...>> {
			using type = from_index_sequence<Tuple, Is...>;
		};


		template <std::size_t I, typename... Tuples>
		struct from_shared_index {
			using type = std::tuple<traits::get_t<I, Tuples>...>;
		};

		template <std::size_t I, typename... Tuples>
		using from_shared_index_t = typename from_shared_index<I, Tuples...>::type;

		template <std::size_t, typename, typename>
		struct from_shared_index_tuple_of_tuples;

		template <std::size_t I, typename TupleOfTuples, typename Seq>
		using from_shared_index_tuple_of_tuples_t =
		    typename from_shared_index_tuple_of_tuples<I, TupleOfTuples, Seq>::type;

		template <std::size_t I, typename TupleOfTuples, std::size_t... Is>
		struct from_shared_index_tuple_of_tuples<I, TupleOfTuples, std::index_sequence<Is...>>
		    : from_shared_index<I, std::tuple_element_t<Is, std::remove_reference_t<TupleOfTuples>>...> {};

		template <std::size_t I, typename TupleOfTuples>
		struct from_shared_index<I, TupleOfTuples>
		    : from_shared_index_tuple_of_tuples<I, TupleOfTuples,
		                                        traits::make_index_sequence_from_tuple<TupleOfTuples>> {};

	}  // namespace detail

	template <typename Tuple, std::size_t B, std::size_t E>
	using from_index_range = detail::from_index_range_t<Tuple, util::make_index_sequence_from_range<B, E>>;

	template <std::size_t... Is, typename Tuple>
	auto constexpr make_from_index_sequence(Tuple&& tuple) noexcept -> from_index_sequence<Tuple, Is...>;

	template <std::size_t... Is, typename Tuple>
	auto constexpr make_from_index_sequence(Tuple&&, std::index_sequence<Is...>) noexcept
	    -> from_index_sequence<Tuple, Is...>;

	template <std::size_t B, std::size_t E, typename Tuple>
	auto constexpr make_from_index_range(Tuple&&) noexcept -> from_index_range<Tuple, B, E>;

	template <std::size_t I, typename... Tuples>
	using from_shared_index = detail::from_shared_index_t<I, Tuples...>;

	template <std::size_t I, typename Tuple, typename... Tuples>
	auto constexpr make_from_shared_index(Tuple&&, Tuples&&...) noexcept
	    -> from_shared_index<I, Tuple, Tuples...>;

	template <std::size_t I, typename TupleOfTuples>
	auto constexpr make_from_shared_index(TupleOfTuples&&) noexcept -> from_shared_index<I, TupleOfTuples>;


	template <typename>
	struct apply;


	// map

	template <typename, typename, typename>
	struct map_result;

	template <typename UnaryOp, typename Tuple, typename Seq>
	using map_result_t = typename map_result<UnaryOp, Tuple, Seq>::type;

	template <typename UnaryOp, typename Tuple, typename Seq>
	bool constexpr map_result_v = map_result<UnaryOp, Tuple, Seq>::value;

	template <typename UnaryOp, typename Tuple, std::size_t... Is>
	struct map_result<UnaryOp, Tuple, std::index_sequence<Is...>>
	    : functional::map_result<UnaryOp, traits::get_t<Is, Tuple>...> {};

	template <typename UnaryOp, typename Tuple>
	auto constexpr map(UnaryOp&&, Tuple&&) noexcept(
	    map_result_v<UnaryOp, Tuple, traits::make_index_sequence_from_tuple<Tuple>>)
	    -> map_result_t<UnaryOp, Tuple, traits::make_index_sequence_from_tuple<Tuple>>;


	// folds

	template <typename, typename, typename, typename, typename>
	class fold_result;

	template <typename Init, typename BinaryOp, typename UnaryOp, typename Tuple, typename Seq>
	using fold_result_t = typename fold_result<Init, BinaryOp, UnaryOp, Tuple, Seq>::type;

	template <typename Init, typename BinaryOp, typename UnaryOp, typename Tuple, typename Seq>
	bool constexpr fold_result_v = fold_result<Init, BinaryOp, UnaryOp, Tuple, Seq>::value;

	template <typename Init, typename BinaryOp, typename UnaryOp, typename Tuple, std::size_t... Is>
	struct fold_result<Init, BinaryOp, UnaryOp, Tuple, std::index_sequence<Is...>>
	    : functional::fold_result<Init, BinaryOp, UnaryOp, traits::get_t<Is, Tuple>...> {};


	template <typename Init, typename BinaryOp, typename Tuple>
	auto constexpr foldl(BinaryOp&&, Init&&, Tuple&&) noexcept(
	    fold_result_v<Init, BinaryOp, functional::id, Tuple, traits::make_index_sequence_from_tuple<Tuple>>)
	    -> fold_result_t<Init, BinaryOp, functional::id, Tuple,
	                     traits::make_index_sequence_from_tuple<Tuple>>;

	template <typename BinaryOp, typename Tuple>
	auto constexpr foldl(BinaryOp&&, Tuple&&) noexcept(
	    fold_result_v<
	        traits::get_t<0, Tuple>, BinaryOp, functional::id, Tuple,
	        util::make_index_sequence_from_range<1, std::tuple_size_v<std::remove_reference_t<Tuple>>>>)
	    -> fold_result_t<
	        traits::get_t<0, Tuple>, BinaryOp, functional::id, Tuple,
	        util::make_index_sequence_from_range<1, std::tuple_size_v<std::remove_reference_t<Tuple>>>>;


	template <typename Init, typename BinaryOp, typename Tuple>
	auto constexpr foldr(BinaryOp&&, Init&&, Tuple&&) noexcept(
	    fold_result_v<
	        Init, functional::flip<BinaryOp>, functional::id, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>>, 0>>)
	    -> fold_result_t<
	        Init, functional::flip<BinaryOp>, functional::id, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>>, 0>>;

	template <typename BinaryOp, typename Tuple>
	auto constexpr foldr(BinaryOp&&, Tuple&&) noexcept(
	    fold_result_v<
	        nxx::traits::remove_cvref_t<
	            traits::get_t<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, Tuple>>,
	        functional::flip<BinaryOp>, functional::id, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, 0>>)
	    -> fold_result_t<
	        nxx::traits::remove_cvref_t<
	            traits::get_t<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, Tuple>>,
	        functional::flip<BinaryOp>, functional::id, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, 0>>;


	template <typename Init, typename BinaryOp, typename UnaryOp, typename Tuple>
	auto constexpr foldl_map(BinaryOp&&, UnaryOp&&, Init&&, Tuple&&) noexcept(
	    fold_result_v<Init, BinaryOp, UnaryOp, Tuple, traits::make_index_sequence_from_tuple<Tuple>>)
	    -> fold_result_t<Init, BinaryOp, UnaryOp, Tuple, traits::make_index_sequence_from_tuple<Tuple>>;

	template <typename BinaryOp, typename UnaryOp, typename Tuple>
	auto constexpr foldl_map(BinaryOp&&, UnaryOp&&, Tuple&& tuple) noexcept(
	    fold_result_v<
	        traits::get_t<0, Tuple>, BinaryOp, UnaryOp, Tuple,
	        util::make_index_sequence_from_range<1, std::tuple_size_v<std::remove_reference_t<Tuple>>>>)
	    -> fold_result_t<
	        traits::get_t<0, Tuple>, BinaryOp, UnaryOp, Tuple,
	        util::make_index_sequence_from_range<1, std::tuple_size_v<std::remove_reference_t<Tuple>>>>;


	template <typename Init, typename BinaryOp, typename UnaryOp, typename Tuple>
	auto constexpr foldr_map(BinaryOp&&, UnaryOp&&, Init&&, Tuple&&) noexcept(
	    fold_result_v<
	        Init, functional::flip<BinaryOp>, UnaryOp, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>>, 0>>)
	    -> fold_result_t<
	        Init, functional::flip<BinaryOp>, UnaryOp, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>>, 0>>;

	template <typename BinaryOp, typename UnaryOp, typename Tuple>
	auto constexpr foldr_map(BinaryOp&&, UnaryOp&&, Tuple&&) noexcept(
	    fold_result_v<
	        traits::get_t<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, Tuple>,
	        functional::flip<BinaryOp>, UnaryOp, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, 0>>)
	    -> fold_result_t<
	        traits::get_t<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, Tuple>,
	        functional::flip<BinaryOp>, UnaryOp, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, 0>>;


	// boolean folds

	namespace traits {
		template <typename, typename, typename>
		struct is_nothrow_boolean_fold;

		template <typename UnaryPred, typename Tuple, typename Seq>
		bool constexpr is_nothrow_boolean_fold_v = is_nothrow_boolean_fold<UnaryPred, Tuple, Seq>::value;

		template <typename UnaryPred, typename Tuple, std::size_t... Is>
		struct is_nothrow_boolean_fold<UnaryPred, Tuple, std::index_sequence<Is...>>
		    : functional::traits::is_nothrow_boolean_fold<UnaryPred, traits::get_t<Is, Tuple>...> {};

	}  // namespace traits

	template <typename Tuple>
	bool constexpr all(Tuple&&) noexcept(
	    traits::is_nothrow_boolean_fold_v<functional::id, Tuple,
	                                      traits::make_index_sequence_from_tuple<Tuple>>);

	template <typename UnaryPred, typename Tuple>
	bool constexpr all(UnaryPred&&, Tuple&&) noexcept(
	    traits::is_nothrow_boolean_fold_v<UnaryPred, Tuple, traits::make_index_sequence_from_tuple<Tuple>>);

	template <typename Tuple>
	bool constexpr any(Tuple&&) noexcept(
	    traits::is_nothrow_boolean_fold_v<functional::id, Tuple,
	                                      traits::make_index_sequence_from_tuple<Tuple>>);

	template <typename UnaryPred, typename Tuple>
	bool constexpr any(UnaryPred&&, Tuple&&) noexcept(
	    traits::is_nothrow_boolean_fold_v<UnaryPred, Tuple, traits::make_index_sequence_from_tuple<Tuple>>);

	template <typename Tuple>
	bool constexpr none(Tuple&&) noexcept(
	    traits::is_nothrow_boolean_fold_v<functional::id, Tuple,
	                                      traits::make_index_sequence_from_tuple<Tuple>>);

	template <typename UnaryPred, typename Tuple>
	bool constexpr none(UnaryPred&&, Tuple&&) noexcept(
	    traits::is_nothrow_boolean_fold_v<UnaryPred, Tuple, traits::make_index_sequence_from_tuple<Tuple>>);

	// no forward for old reductions; they are to be deprecated
}  // namespace nxx::tuple

#endif  // ifndef NXX__TUPLE_FWD_HXX
