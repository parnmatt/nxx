#ifndef NXX__OSTREAM_HXX
#define NXX__OSTREAM_HXX

#include <ostream>

#include <streambuf>
#include <string>
#include <type_traits>
#include <utility>

namespace nxx::ostream {

	template <typename CharT = char, typename Traits = std::char_traits<CharT>>
	class redirect {
	public:
		using char_type = CharT;
		using traits_type = Traits;
		using ostream_type = std::basic_ostream<char_type, traits_type>;
		using streambuf_type = std::basic_streambuf<char_type, traits_type>;

		redirect() = default;

		explicit redirect(ostream_type& old_target, ostream_type& new_target) noexcept
		    : m_os{&old_target}
		    , m_sb{old_target.rdbuf(new_target.rdbuf())} {}

		explicit redirect(redirect&& other) noexcept { *this = std::forward<redirect>(other); }

		auto operator=(redirect&& other) noexcept -> redirect& {
			std::swap(m_os, other.m_os);
			std::swap(m_sb, other.m_sb);
			return *this;
		}

		~redirect() noexcept {
			if (m_os)
				m_os->rdbuf(m_sb);
		}

	private:
		ostream_type* m_os{};
		streambuf_type* m_sb{};
	};

}  // namespace nxx::ostream

#endif  // ifndef NXX__OSTREAM_HXX
