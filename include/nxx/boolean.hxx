#ifndef NXX__BOOLEAN_HXX
#define NXX__BOOLEAN_HXX

#include <functional>

namespace nxx::boolean {

	template <bool B>
	struct static_boolean;

	using static_true_t = static_boolean<true>;
	using static_false_t = static_boolean<false>;
	// note: both static_true and static_false constexpr objects are defined

	struct boolean;

	template <typename F>
	struct boolean_fn;

	template <typename C = void, typename IR = void, typename IL = void>
	struct placeholder_t;


	// relational

	template <typename R, typename T, typename IR = void, typename IL = void>
	class relational;

	template <typename T, typename C = void, typename IR = void, typename IL = void>
	using less = relational<std::less<C>, T, IR, IL>;

	template <typename T, typename C = void, typename IR = void, typename IL = void>
	using less_equal = relational<std::less_equal<C>, T, IR, IL>;

	template <typename T, typename C = void, typename IR = void, typename IL = void>
	using greater = relational<std::greater<C>, T, IR, IL>;

	template <typename T, typename C = void, typename IR = void, typename IL = void>
	using greater_equal = relational<std::greater_equal<C>, T, IR, IL>;

	template <typename T, typename C = void, typename IL = void>
	using equal_to = relational<std::equal_to<C>, T, void, IL>;

	template <typename T, typename C = void, typename IL = void>
	using not_equal_to = relational<std::not_equal_to<C>, T, void, IL>;


	// logical

	template <typename L, typename... Ts>
	struct logical;

	template <typename L, typename... Ts>
	using logical_and = logical<std::logical_and<L>, Ts...>;

	template <typename L, typename... Ts>
	using logical_or = logical<std::logical_or<L>, Ts...>;


	// intervals

	template <typename Tl, typename Tr, typename Cc = void, typename Cl = void, typename Cr = void>
	using open_interval = logical_and<Cc, greater<Tl, Cl>, less<Tr, Cr>>;

	template <typename Tl, typename Tr, typename Cc = void, typename Cl = void, typename Cr = void>
	using left_half_open_interval = logical_and<Cc, greater<Tl, Cl>, less_equal<Tr, Cr>>;

	template <typename Tl, typename Tr, typename Cc = void, typename Cl = void, typename Cr = void>
	using right_half_closed_interval = left_half_open_interval<Tl, Tr, Cc, Cl, Cr>;

	template <typename Tl, typename Tr, typename Cc = void, typename Cl = void, typename Cr = void>
	using right_half_open_interval = logical_and<Cc, greater_equal<Tl, Cl>, less<Tr, Cr>>;

	template <typename Tl, typename Tr, typename Cc = void, typename Cl = void, typename Cr = void>
	using left_half_closed_interval = right_half_open_interval<Tl, Tr, Cc, Cl, Cr>;

	template <typename Tl, typename Tr, typename Cc = void, typename Cl = void, typename Cr = void>
	using closed_interval = logical_and<Cc, greater_equal<Tl, Cl>, less_equal<Tr, Cr>>;


}  // namespace nxx::boolean

#include <tuple>
#include <type_traits>
#include <utility>

#include <nxx/functional.hxx>
#include <nxx/strong_typedef.hxx>
#include <nxx/tuple.hxx>
#include <nxx/type_traits.hxx>
#include <nxx/utility.hxx>

// clang-format off
#include <nxx/boolean/type_traits.hxx>
#include <nxx/boolean/boolean_fn.hxx>
#include <nxx/boolean/static_boolean.hxx>
#include <nxx/boolean/boolean.hxx>
#include <nxx/boolean/relational.hxx>
#include <nxx/boolean/placeholder.hxx>
#include <nxx/boolean/logical.hxx>
#include <nxx/boolean/intervals.hxx>

#endif  // ifndef NXX__BOOLEAN_HXX
