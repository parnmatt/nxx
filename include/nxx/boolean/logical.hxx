#ifndef NXX__BOOLEAN_LOGICAL_HXX
#define NXX__BOOLEAN_LOGICAL_HXX
#ifndef NXX__BOOLEAN_HXX
#	error "please only include nxx/boolean.hxx; this is an implementation header"
#else  // ifndef NXX__BOOLEAN_HXX

namespace nxx::boolean {

	namespace detail {

		template <typename>
		struct logical_id;

		template <typename T>
		using logical_id_t = typename logical_id<T>::type;

		template <typename T>
		struct logical_id<std::logical_and<T>> {
			using type = static_true_t;
		};

		template <typename T>
		struct logical_id<std::logical_or<T>> {
			using type = static_false_t;
		};


		template <typename, typename...>
		struct logical_tuple;

		template <typename L, typename... Ts>
		using logical_tuple_t = typename logical_tuple<L, Ts...>::type;

		template <typename L>
		struct logical_tuple<L> : std::tuple<logical_id_t<L>, logical_id_t<L>> {
			using type = std::tuple<logical_id_t<L>, logical_id_t<L>>;
			using type::tuple;
		};

		template <typename L, typename Singular>
		struct logical_tuple<L, Singular> : std::tuple<logical_id_t<L>, Singular> {
			using type = std::tuple<logical_id_t<L>, Singular>;

			using type::tuple;
			explicit constexpr logical_tuple(Singular&& singular)
			    : type{logical_id_t<L>{}, std::forward<Singular>(singular)} {}
		};

		template <typename L, typename First, typename Second, typename... Rest>
		struct logical_tuple<L, First, Second, Rest...> : std::tuple<First, Second, Rest...> {
			using type = std::tuple<First, Second, Rest...>;

			using type::tuple;
			explicit constexpr logical_tuple(First&& first, Second&& second, Rest&&... rest)
			    : type{std::forward<First>(first), std::forward<Second>(second),
			           std::forward<Rest>(rest)...} {}
		};

	}  // namespace detail


	template <typename L, typename... Ts>
	class logical
	    : public std::enable_if_t<traits::is_logical_fn_v<L>,
	                              strong_typedef<logical<L, Ts...>, detail::logical_tuple<L, Ts...>>>
	    , public boolean_fn<void> {
	private:
		using logical_tuple_t = detail::logical_tuple<L, Ts...>;
		using tuple_t = typename logical_tuple_t::type;

	public:
		using type = L;
		using strong_typedef<logical, logical_tuple_t>::strong_typedef;

		template <typename... Args>
		auto constexpr operator()(Args&&... args) const
		    noexcept(tuple::fold_result_v<
		             tuple::traits::get_t<0, tuple_t>, type, functional::wrap_args<Args...>, tuple_t,
		             util::make_index_sequence_from_range<1, std::tuple_size_v<tuple_t>>>) -> decltype(auto) {

			return tuple::foldl_map(type{}, nxx::functional::wrap_args{std::forward<Args>(args)...},
			                        static_cast<tuple_t const&>(**this));
		}
	};


	// helpers for functors

	template <typename L, typename... Ts>
	auto constexpr make_logical(Ts... ts) noexcept(
	    std::conjunction_v<std::is_nothrow_constructible<logical<L, Ts...>, std::tuple<Ts&&...>>,
	                       std::is_nothrow_move_constructible<Ts>...>) {
		return logical<L, Ts...>{std::forward_as_tuple(std::move(ts)...)};
	}

	template <typename L, typename... Ts>
	auto constexpr make_logical(std::tuple<Ts...> t) noexcept(
	    std::is_nothrow_constructible_v<logical<L, Ts...>, std::tuple<Ts...>&&>) {
		return logical<L, Ts...>{std::move(t)};
	}


	// logical_and

	template <typename L, typename... Ts>
	auto constexpr make_logical_and(Ts&&... ts) noexcept(
	    noexcept(make_logical<std::logical_and<L>>(std::forward<Ts>(ts)...))) -> decltype(auto) {
		return make_logical<std::logical_and<L>>(std::forward<Ts>(ts)...);
	}

	template <typename... Ts>
	auto constexpr make_logical_and(Ts&&... ts) noexcept(
	    noexcept(make_logical_and<void>(std::forward<Ts>(ts)...))) -> decltype(auto) {
		return make_logical_and<void>(std::forward<Ts>(ts)...);
	}


	// logical_or

	template <typename L, typename... Ts>
	auto constexpr make_logical_or(Ts&&... ts) noexcept(
	    noexcept(make_logical<std::logical_or<L>>(std::forward<Ts>(ts)...))) -> decltype(auto) {
		return make_logical<std::logical_or<L>>(std::forward<Ts>(ts)...);
	}

	template <typename... Ts>
	auto constexpr make_logical_or(Ts&&... ts) noexcept(
	    noexcept(make_logical_or<void>(std::forward<Ts>(ts)...))) -> decltype(auto) {
		return make_logical_or<void>(std::forward<Ts>(ts)...);
	}


	// operators

	// logical and

	template <typename R, typename T, typename IR, typename IL, typename L, typename... Ts>
	auto constexpr operator&&(relational<R, T, IR, IL> lhs, logical_and<L, Ts...> rhs) noexcept(
	    std::is_nothrow_constructible_v<logical_and<L, decltype(lhs), Ts...>,
	                                    std::tuple<decltype(lhs)&&, Ts&&...>>) {
		return logical_and<L, decltype(lhs), Ts...>{
		    std::tuple_cat(std::forward_as_tuple(std::move(lhs)), *std::move(rhs))};
	}

	template <typename L, typename... Ts, typename R, typename T, typename IR, typename IL>
	auto constexpr operator&&(logical_and<L, Ts...> lhs, relational<R, T, IR, IL> rhs) noexcept(
	    std::is_nothrow_constructible_v<logical_and<L, Ts..., decltype(rhs)&&>,
	                                    std::tuple<Ts&&..., decltype(rhs)&&>>) {
		return logical_and<L, Ts..., decltype(rhs)>{
		    std::tuple_cat(*std::move(lhs), std::forward_as_tuple(std::move(rhs)))};
	}

	template <typename L, typename... Tsl, typename... Tsr>
	auto constexpr operator&&(logical_and<L, Tsl...> lhs, logical_and<L, Tsr...> rhs) noexcept(
	    std::is_nothrow_constructible_v<logical_and<L, Tsl..., Tsr...>, std::tuple<Tsl&&..., Tsr&&...>>) {
		return logical_and<L, Tsl..., Tsr...>{std::tuple_cat(*std::move(lhs), *std::move(rhs))};
	}


	// logical or

	template <typename R, typename T, typename IR, typename IL, typename L, typename... Ts>
	auto constexpr operator||(relational<R, T, IR, IL> lhs, logical_or<L, Ts...> rhs) noexcept(
	    std::is_nothrow_constructible_v<logical_or<L, decltype(lhs), Ts...>,
	                                    std::tuple<decltype(lhs)&&, Ts&&...>>) {
		return logical_or<L, decltype(lhs), Ts...>{
		    std::tuple_cat(std::forward_as_tuple(std::move(lhs)), *std::move(rhs))};
	}

	template <typename L, typename... Ts, typename R, typename T, typename IR, typename IL>
	auto constexpr operator||(logical_or<L, Ts...> lhs, relational<R, T, IR, IL> rhs) noexcept(
	    std::is_nothrow_constructible_v<logical_or<L, Ts..., decltype(rhs)&&>,
	                                    std::tuple<Ts&&..., decltype(rhs)&&>>) {
		return logical_or<L, Ts..., decltype(rhs)>{
		    std::tuple_cat(*std::move(lhs), std::forward_as_tuple(std::move(rhs)))};
	}

	template <typename L, typename... Tsl, typename... Tsr>
	auto constexpr operator||(logical_or<L, Tsl...> lhs, logical_or<L, Tsr...> rhs) noexcept(
	    std::is_nothrow_constructible_v<logical_or<L, Tsl..., Tsr...>, std::tuple<Tsl&&..., Tsr&&...>>) {
		return logical_or<L, Tsl..., Tsr...>{std::tuple_cat(*std::move(lhs), *std::move(rhs))};
	}


	// logical not

	namespace detail {
		template <typename T, typename = std::enable_if_t<traits::is_logical_fn_v<T>>>
		struct negate_logical;

		template <typename T>
		using negate_logical_t = typename negate_logical<T>::type;

		template <typename T>
		struct negate_logical<std::logical_and<T>> {
			using type = std::logical_or<T>;
		};

		template <typename T>
		struct negate_logical<std::logical_or<T>> {
			using type = std::logical_and<T>;
		};


	}  // namespace detail

	template <typename L, std::enable_if_t<traits::is_logical_v<L>, int> = 0>
	auto constexpr operator!(L&& l) noexcept(
	    noexcept(nxx::tuple::transform(std::logical_not{}, *l))
	    && noexcept(make_logical<detail::negate_logical_t<typename std::decay_t<L>::type>>(
	        std::move(nxx::tuple::transform(std::logical_not{}, *l))))) -> decltype(auto) {
		using logical_t = detail::negate_logical_t<typename std::decay_t<L>::type>;
		auto not_ts = nxx::tuple::transform(std::logical_not{}, *l);
		return make_logical<logical_t>(std::move(not_ts));
	}


	// free function to get the values of a logical of relationals
	template <typename L, typename... Ts,
	          std::enable_if_t<std::conjunction_v<traits::is_relational<Ts>...>, int> = 0>
	auto constexpr get_values(logical<L, Ts...> rs) noexcept(noexcept(nxx::tuple::transform(detail::get_value,
	                                                                                        *rs)))
	    -> decltype(auto) {
		return nxx::tuple::transform(detail::get_value, *rs);
	}

}  // namespace nxx::boolean

#endif  // ifndef NXX__BOOLEAN_HXX
#endif  // ifndef NXX__BOOLEAN_LOGICAL_HXX
