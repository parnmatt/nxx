#ifndef NXX__BOOLEAN_TYPE_TRAITS_HXX
#define NXX__BOOLEAN_TYPE_TRAITS_HXX
#ifndef NXX__BOOLEAN_HXX
#	error "please only include nxx/boolean.hxx; this is an implementation header"
#else  // ifndef NXX__BOOLEAN_HXX

namespace nxx::boolean::traits {

	template <typename F, typename... Args>
	using callable_to_bool = std::is_convertible<std::invoke_result_t<F, Args...>, bool>;

	template <typename F, typename... Args>
	bool constexpr callable_to_bool_v = callable_to_bool<F, Args...>::value;


	// boolean

	template <typename T>
	using is_boolean = std::disjunction<std::is_same<static_boolean<true>, std::decay_t<T>>,
	                                    std::is_same<static_boolean<false>, std::decay_t<T>>,
	                                    std::is_same<boolean, std::decay_t<T>>>;

	template <typename T>
	bool constexpr is_boolean_v = is_boolean<T>::value;

	template <typename T>
	using is_bool_like = std::conjunction<std::is_convertible<T, bool>, std::negation<is_boolean<T>>>;

	template <typename T>
	bool constexpr is_bool_like_v = is_bool_like<T>::value;


	// boolean_fn

	template <typename T>
	using is_boolean_fn = std::disjunction<nxx::traits::is_specialization_of<boolean_fn, std::decay_t<T>>,
	                                       std::is_base_of<boolean_fn<void>, std::decay_t<T>>>;

	template <typename T>
	bool constexpr is_boolean_fn_v = is_boolean_fn<T>::value;


	// relational

	template <typename T>
	using is_relational_fn = std::disjunction<nxx::traits::is_specialization_of<std::less, T>,
	                                          nxx::traits::is_specialization_of<std::less_equal, T>,
	                                          nxx::traits::is_specialization_of<std::greater, T>,
	                                          nxx::traits::is_specialization_of<std::greater_equal, T>,
	                                          nxx::traits::is_specialization_of<std::equal_to, T>,
	                                          nxx::traits::is_specialization_of<std::not_equal_to, T>>;
	template <typename T>
	bool constexpr is_relational_fn_v = is_relational_fn<T>::value;

	template <typename T>
	using is_relational = nxx::traits::is_specialization_of<relational, std::decay_t<T>>;

	template <typename T>
	bool constexpr is_relational_v = is_relational<T>::value;


	// logical

	template <typename T>
	using is_logical_fn = std::disjunction<nxx::traits::is_specialization_of<std::logical_and, T>,
	                                       nxx::traits::is_specialization_of<std::logical_or, T>>;

	template <typename T>
	bool constexpr is_logical_fn_v = is_logical_fn<T>::value;

	template <typename T>
	using is_logical = nxx::traits::is_specialization_of<logical, std::decay_t<T>>;

	template <typename T>
	bool constexpr is_logical_v = is_logical<T>::value;


	// intervals

	template <typename>
	struct is_interval : std::false_type {};

	template <typename T>
	bool constexpr is_interval_v = is_interval<T>::value;

	template <typename Tl, typename Tr, typename Cc, typename Cl, typename Cr>
	struct is_interval<open_interval<Tl, Tr, Cc, Cl, Cr>> : std::true_type {};

	template <typename Tl, typename Tr, typename Cc, typename Cl, typename Cr>
	struct is_interval<left_half_open_interval<Tl, Tr, Cc, Cl, Cr>> : std::true_type {};

	template <typename Tl, typename Tr, typename Cc, typename Cl, typename Cr>
	struct is_interval<right_half_open_interval<Tl, Tr, Cc, Cl, Cr>> : std::true_type {};

	template <typename Tl, typename Tr, typename Cc, typename Cl, typename Cr>
	struct is_interval<closed_interval<Tl, Tr, Cc, Cl, Cr>> : std::true_type {};

}  // namespace nxx::boolean::traits

#endif  // ifndef NXX__BOOLEAN_HXX
#endif  // ifndef NXX__BOOLEAN_TYPE_TRAITS_HXX
