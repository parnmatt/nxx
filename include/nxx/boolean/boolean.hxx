#ifndef NXX__BOOLEAN_BOOLEAN_HXX
#define NXX__BOOLEAN_BOOLEAN_HXX
#ifndef NXX__BOOLEAN_HXX
#	error "please only include nxx/boolean.hxx; this is an implementation header"
#else  // ifndef NXX__BOOLEAN_HXX

namespace nxx::boolean {

	struct boolean
	    : nxx::strong_typedef<boolean, bool>
	    , boolean_fn<void> {
		using nxx::strong_typedef<boolean, bool>::strong_typedef;

		constexpr operator bool() const noexcept { return **this; }

		template <typename... Args>
		bool constexpr operator()(Args&&...) const noexcept {
			return **this;
		}
	};


	// operators

	// logical and

	auto constexpr operator&&(boolean lhs,
	                          boolean rhs) noexcept(std::is_nothrow_constructible_v<boolean, bool>) {
		return boolean{*std::move(lhs) && *std::move(rhs)};
	}

	template <typename B, std::enable_if_t<traits::is_bool_like_v<B>, int> = 0>
	auto constexpr operator&&(boolean lhs, B&& rhs) noexcept(
	    std::is_nothrow_constructible_v<boolean, decltype(rhs)>&& noexcept(std::move(lhs)
	                                                                       && std::declval<boolean>()))
	    -> decltype(auto) {
		return std::move(lhs) && boolean{std::forward<B>(rhs)};
	}

	template <typename B, std::enable_if_t<traits::is_bool_like_v<B>, int> = 0>
	auto constexpr operator&&(B&& lhs, boolean rhs) noexcept(
	    std::is_nothrow_constructible_v<boolean, decltype(lhs)>&& noexcept(std::declval<boolean>()
	                                                                       && std::move(rhs)))
	    -> decltype(auto) {
		return boolean{std::forward<B>(lhs)} && std::move(rhs);
	}


	// logical or

	auto constexpr operator||(boolean lhs,
	                          boolean rhs) noexcept(std::is_nothrow_constructible_v<boolean, bool>) {
		return boolean{*std::move(lhs) || *std::move(rhs)};
	}

	template <typename B, std::enable_if_t<traits::is_bool_like_v<B>, int> = 0>
	auto constexpr operator||(boolean lhs, B&& rhs) noexcept(
	    std::is_nothrow_constructible_v<boolean, decltype(rhs)>&& noexcept(std::move(lhs)
	                                                                       || std::declval<boolean>()))
	    -> decltype(auto) {
		return std::move(lhs) || boolean{std::forward<B>(rhs)};
	}

	template <typename B, std::enable_if_t<traits::is_bool_like_v<B>, int> = 0>
	auto constexpr operator||(B&& lhs, boolean rhs) noexcept(
	    std::is_nothrow_constructible_v<boolean, decltype(lhs)>&& noexcept(std::declval<boolean>()
	                                                                       || std::move(rhs)))
	    -> decltype(auto) {
		return boolean{std::forward<B>(lhs)} || std::move(rhs);
	}


	// logical not

	auto constexpr operator!(boolean b) noexcept(std::is_nothrow_constructible_v<boolean, bool>) {
		return boolean{std::logical_not{}(*std::move(b))};
	}

}  // namespace nxx::boolean

#endif  // ifndef NXX__BOOLEAN_HXX
#endif  // ifndef NXX__BOOLEAN_BOOLEAN_HXX
