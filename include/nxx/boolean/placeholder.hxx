#ifndef NXX__BOOLEAN_PLACEHOLDER_HXX
#define NXX__BOOLEAN_PLACEHOLDER_HXX
#ifndef NXX__BOOLEAN_HXX
#	error "please only include nxx/boolean.hxx; this is an implementation header"
#else  // ifndef NXX__BOOLEAN_HXX

namespace nxx::boolean {

	template <typename C, typename IR, typename IL>
	struct placeholder_t {};

	template <typename C = void, typename IR = void, typename IL = void>
	auto constexpr placeholder_v = placeholder_t<C, IR, IL>{};

	auto constexpr placeholder = placeholder_v<>;


	template <typename T, typename C, typename IR, typename IL>
	auto constexpr operator<(placeholder_t<C, IR, IL>,
	                         T&& t) noexcept(noexcept(less<T, C, IR, IL>{std::forward<T>(t)})) {
		return less<T, C, IR, IL>{std::forward<T>(t)};
	}

	template <typename T, typename C, typename IR, typename IL>
	auto constexpr
	operator<(T&& t, placeholder_t<C, IR, IL>) noexcept(noexcept(greater<T, C, IR, IL>{std::forward<T>(t)})) {
		return greater<T, C, IR, IL>{std::forward<T>(t)};
	}

	template <typename T, typename C, typename IR, typename IL>
	auto constexpr operator<=(placeholder_t<C, IR, IL>,
	                          T&& t) noexcept(noexcept(less_equal<T, C, IR, IL>{std::forward<T>(t)})) {
		return less_equal<T, C, IR, IL>{std::forward<T>(t)};
	}

	template <typename T, typename C, typename IR, typename IL>
	auto constexpr operator<=(T&& t, placeholder_t<C, IR, IL>) noexcept(noexcept(greater_equal<T, C, IR, IL>{
	    std::forward<T>(t)})) {
		return greater_equal<T, C, IR, IL>{std::forward<T>(t)};
	}

	template <typename T, typename C, typename IR, typename IL>
	auto constexpr operator>(placeholder_t<C, IR, IL>,
	                         T&& t) noexcept(noexcept(greater<T, C, IR, IL>{std::forward<T>(t)})) {
		return greater<T, C, IR, IL>{std::forward<T>(t)};
	}

	template <typename T, typename C, typename IR, typename IL>
	auto constexpr
	operator>(T&& t, placeholder_t<C, IR, IL>) noexcept(noexcept(less<T, C, IR, IL>{std::forward<T>(t)})) {
		return less<T, C, IR, IL>{std::forward<T>(t)};
	}

	template <typename T, typename C, typename IR, typename IL>
	auto constexpr operator>=(placeholder_t<C, IR, IL>,
	                          T&& t) noexcept(noexcept(greater_equal<T, C, IR, IL>{std::forward<T>(t)})) {
		return greater_equal<T, C, IR, IL>{std::forward<T>(t)};
	}

	template <typename T, typename C, typename IR, typename IL>
	auto constexpr operator>=(T&& t, placeholder_t<C, IR, IL>) noexcept(noexcept(less_equal<T, C, IR, IL>{
	    std::forward<T>(t)})) {
		return less_equal<T, C, IR, IL>{std::forward<T>(t)};
	}

	template <typename T, typename C, typename IR, typename IL>
	auto constexpr operator==(placeholder_t<C, IR, IL>,
	                          T&& t) noexcept(noexcept(equal_to<T, C, IL>{std::forward<T>(t)})) {
		return equal_to<T, C, IL>{std::forward<T>(t)};
	}

	template <typename T, typename C, typename IR, typename IL>
	auto constexpr
	operator==(T&& t, placeholder_t<C, IR, IL>) noexcept(noexcept(equal_to<T, C, IL>{std::forward<T>(t)})) {
		return equal_to<T, C, IL>{std::forward<T>(t)};
	}

	template <typename T, typename C, typename IR, typename IL>
	auto constexpr operator!=(placeholder_t<C, IR, IL>,
	                          T&& t) noexcept(noexcept(not_equal_to<T, C, IL>{std::forward<T>(t)})) {
		return not_equal_to<T, C, IL>{std::forward<T>(t)};
	}

	template <typename T, typename C, typename IR, typename IL>
	auto constexpr operator!=(T&& t, placeholder_t<C, IR, IL>) noexcept(noexcept(not_equal_to<T, C, IL>{
	    std::forward<T>(t)})) {
		return not_equal_to<T, C, IL>{std::forward<T>(t)};
	}

}  // namespace nxx::boolean

#endif  // ifndef NXX__BOOLEAN_HXX
#endif  // ifndef NXX__BOOLEAN_PLACEHOLDER_HXX
