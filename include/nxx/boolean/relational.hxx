#ifndef NXX__BOOLEAN_RELATIONAL_HXX
#define NXX__BOOLEAN_RELATIONAL_HXX
#ifndef NXX__BOOLEAN_HXX
#	error "please only include nxx/boolean.hxx; this is an implementation header"
#else  // ifndef NXX__BOOLEAN_HXX

namespace nxx::boolean {

	template <typename R, typename T, typename IR, typename IL>
	class relational : public std::enable_if_t<traits::is_relational_fn_v<R>, boolean_fn<void>> {
	public:
		constexpr relational() noexcept = delete;

		explicit constexpr relational(T const& value) noexcept(std::is_nothrow_copy_constructible_v<T>)
		    : m_value{value} {}

		explicit constexpr relational(T&& value) noexcept(std::is_nothrow_move_constructible_v<T>)
		    : m_value{std::move(value)} {}

		auto constexpr value() const noexcept -> T const& { return m_value; }

		template <typename V, std::enable_if_t<traits::callable_to_bool_v<R, V&&, T&>, int> = 0>
		auto constexpr operator()(V&& value) const
		    noexcept(noexcept(std::declval<R>()(value, std::declval<T&>()))) -> decltype(auto) {
			return R{}(std::forward<V>(value), m_value);
		}

	private:
		T m_value;
	};


	// helpers for functors

	template <typename R, typename T, typename IR = void, typename IL = void>
	auto constexpr make_relational(T&& value) noexcept(
	    std::is_nothrow_constructible_v<relational<R, T, IR, IL>, decltype(value)>) {
		return relational<R, T, IR, IL>{std::forward<T>(value)};
	}

	template <typename T, typename C = void, typename IR = void, typename IL = void>
	auto constexpr make_less(T&& value) noexcept(
	    std::is_nothrow_constructible_v<less<T, C, IR, IL>, decltype(value)>) {
		return less<T, C, IR, IL>{std::forward<T>(value)};
	}

	template <typename T, typename C = void, typename IR = void, typename IL = void>
	auto constexpr make_less_equal(T&& value) noexcept(
	    std::is_nothrow_constructible_v<less_equal<T, C, IR, IL>, decltype(value)>) {
		return less_equal<T, C, IR, IL>{std::forward<T>(value)};
	}

	template <typename T, typename C = void, typename IR = void, typename IL = void>
	auto constexpr make_greater(T&& value) noexcept(
	    std::is_nothrow_constructible_v<greater<T, C, IR, IL>, decltype(value)>) {
		return greater<T, C, IR, IL>{std::forward<T>(value)};
	}

	template <typename T, typename C = void, typename IR = void, typename IL = void>
	auto constexpr make_greater_equal(T&& value) noexcept(
	    std::is_nothrow_constructible_v<greater_equal<T, C, IR, IL>, decltype(value)>) {
		return greater_equal<T, C, IR, IL>{std::forward<T>(value)};
	}

	template <typename T, typename C = void, typename IL = void>
	auto constexpr make_equal_to(T&& value) noexcept(
	    std::is_nothrow_constructible_v<equal_to<T, C, IL>, decltype(value)>) {
		return equal_to<T, C, IL>{std::forward<T>(value)};
	}

	template <typename T, typename C = void, typename IL = void>
	auto constexpr make_not_equal_to(T&& value) noexcept(
	    std::is_nothrow_constructible_v<not_equal_to<T, C, IL>, decltype(value)>) {
		return not_equal_to<T, C, IL>{std::forward<T>(value)};
	}


	// operators

	// logical and

	template <typename R, std::enable_if_t<traits::is_relational_v<R>, int> = 0>
	auto constexpr operator&&(R&& lhs, R&& rhs) noexcept(
	    noexcept(typename std::decay_t<R>::relational_type{}(lhs.value(), rhs.value()))) -> decltype(auto) {
		using relational_t = typename std::decay_t<R>::relational_type;
		return relational_t{}(lhs.value(), rhs.value()) ? std::forward<R>(lhs) : std::forward<R>(rhs);
	}


	// logical or

	template <typename R, std::enable_if_t<traits::is_relational_v<R>, int> = 0>
	auto constexpr operator||(R&& lhs, R&& rhs) noexcept(
	    noexcept(typename std::decay_t<R>::relational_type{}(lhs.value(), rhs.value()))) -> decltype(auto) {
		using relational_t = typename std::decay_t<R>::relational_type;
		return relational_t{}(lhs.value(), rhs.value()) ? std::forward<R>(rhs) : std::forward<R>(lhs);
	}


	// logical not

	namespace detail {

		template <typename T, typename = std::enable_if_t<
		                          std::disjunction_v<traits::is_relational_fn<T>, traits::is_relational<T>>>>
		struct negate_relational;

		template <typename T>
		using negate_relational_t = typename negate_relational<T>::type;

		template <typename T>
		struct negate_relational<std::less<T>> {
			using type = std::greater_equal<T>;
		};

		template <typename T>
		struct negate_relational<std::less_equal<T>> {
			using type = std::greater<T>;
		};

		template <typename T>
		struct negate_relational<std::greater<T>> {
			using type = std::less_equal<T>;
		};

		template <typename T>
		struct negate_relational<std::greater_equal<T>> {
			using type = std::less<T>;
		};

		template <typename T>
		struct negate_relational<std::equal_to<T>> {
			using type = std::not_equal_to<T>;
		};

		template <typename T>
		struct negate_relational<std::not_equal_to<T>> {
			using type = std::equal_to<T>;
		};

		template <typename R, typename T, typename IR, typename IL>
		struct negate_relational<relational<R, T, IR, IL>> {
			using type = relational<detail::negate_relational_t<R>, T, IR, IL>;
		};

	}  // namespace detail

	template <typename R, std::enable_if_t<traits::is_relational_v<R>, int> = 0>
	auto constexpr operator!(R&& r) noexcept(
	    std::is_nothrow_constructible_v<detail::negate_relational_t<std::decay_t<R>>, decltype(r.value())>) {
		using relational_t = detail::negate_relational_t<std::decay_t<R>>;
		return relational_t{r.value()};
	}


	// free function for getting relational value
	template <typename R, std::enable_if_t<traits::is_relational_v<R>, int> = 0>
	auto constexpr get_value(R&& r) noexcept(noexcept(r.value())) -> decltype(auto) {
		return r.value();
	}

	namespace detail {
		auto constexpr get_value = [](auto&& r) noexcept(noexcept(
		    nxx::boolean::get_value<decltype(r)>(std::forward<decltype(r)>(r)))) -> decltype(auto) {
			using R = decltype(r);
			return nxx::boolean::get_value<R>(std::forward<R>(r));
		};
	}  // namespace detail

}  // namespace nxx::boolean

#endif  // ifndef NXX__BOOLEAN_HXX
#endif  // ifndef NXX__BOOLEAN_RELATIONAL_HXX
