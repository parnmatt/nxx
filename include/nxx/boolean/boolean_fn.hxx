#ifndef NXX__BOOLEAN_BOOLEAN_FN_HXX
#define NXX__BOOLEAN_BOOLEAN_FN_HXX
#ifndef NXX__BOOLEAN_HXX
#	error "please only include nxx/boolean.hxx; this is an implementation header"
#else  // ifndef NXX__BOOLEAN_HXX

namespace nxx::boolean {

	template <typename F>
	class boolean_fn {
	public:
		constexpr boolean_fn() = delete;
		constexpr boolean_fn(F f) noexcept
		    : m_f{std::move(f)} {}

		template <typename... Args>
		auto constexpr operator()(Args&&... args) const
		    noexcept(noexcept(std::declval<F>()(std::forward<Args>(args)...))) -> decltype(auto) {
			return m_f(std::forward<Args>(args)...);
		}

	private:
		F m_f;
	};

	template <>
	struct boolean_fn<void> {};


	// operators

	// logical and

	template <
	    typename B, typename F,
	    std::enable_if_t<std::conjunction_v<traits::is_bool_like<B>, traits::is_boolean_fn<F>>, int> = 0>
	auto constexpr
	operator&&(B&& lhs, F&& rhs) noexcept(std::is_nothrow_constructible_v<boolean, decltype(lhs)>&& noexcept(
	    std::declval<boolean>() && std::forward<F>(rhs))) -> decltype(auto) {
		return boolean{std::forward<B>(lhs)} && std::forward<F>(rhs);
	}

	template <
	    typename F, typename B,
	    std::enable_if_t<std::conjunction_v<traits::is_boolean_fn<F>, traits::is_bool_like<B>>, int> = 0>
	auto constexpr
	operator&&(F&& lhs, B&& rhs) noexcept(std::is_nothrow_constructible_v<boolean, decltype(rhs)>&& noexcept(
	    std::forward<F>(lhs) && std::declval<boolean>())) -> decltype(auto) {
		return std::forward<F>(lhs) && boolean{std::forward<B>(rhs)};
	}

	template <
	    typename Fl, typename Fr,
	    std::enable_if_t<std::conjunction_v<traits::is_boolean_fn<Fl>, traits::is_boolean_fn<Fr>>, int> = 0>
	auto constexpr operator&&(Fl&& lhs, Fr&& rhs) noexcept(noexcept(make_logical_and(std::forward<Fl>(lhs),
	                                                                                 std::forward<Fr>(rhs))))
	    -> decltype(auto) {
		return make_logical_and(std::forward<Fl>(lhs), std::forward<Fr>(rhs));
	}


	// logical or

	template <
	    typename B, typename F,
	    std::enable_if_t<std::conjunction_v<traits::is_bool_like<B>, traits::is_boolean_fn<F>>, int> = 0>
	auto constexpr
	operator||(B&& lhs, F&& rhs) noexcept(std::is_nothrow_constructible_v<boolean, decltype(lhs)>&& noexcept(
	    std::declval<boolean>() || std::forward<F>(rhs))) -> decltype(auto) {
		return boolean{std::forward<B>(lhs)} || std::forward<F>(rhs);
	}

	template <
	    typename F, typename B,
	    std::enable_if_t<std::conjunction_v<traits::is_boolean_fn<F>, traits::is_bool_like<B>>, int> = 0>
	auto constexpr
	operator||(F&& lhs, B&& rhs) noexcept(std::is_nothrow_constructible_v<boolean, decltype(rhs)>&& noexcept(
	    std::forward<F>(lhs) || std::declval<boolean>())) -> decltype(auto) {
		return std::forward<F>(lhs) || boolean{std::forward<B>(rhs)};
	}

	template <
	    typename Fl, typename Fr,
	    std::enable_if_t<std::conjunction_v<traits::is_boolean_fn<Fl>, traits::is_boolean_fn<Fr>>, int> = 0>
	auto constexpr operator||(Fl&& lhs, Fr&& rhs) noexcept(noexcept(make_logical_or(std::forward<Fl>(lhs),
	                                                                                std::forward<Fr>(rhs))))
	    -> decltype(auto) {
		return make_logical_or(std::forward<Fl>(lhs), std::forward<Fr>(rhs));
	}


	// logical not

	template <
	    typename F,
	    std::enable_if_t<
	        std::conjunction_v<traits::is_boolean_fn<F>, std::negation<traits::is_boolean<F>>,
	                           std::negation<traits::is_relational<F>>, std::negation<traits::is_logical<F>>>,
	        int> = 0>
	auto constexpr operator!(F&& f) noexcept(noexcept(boolean_fn{nxx::functional::not_fn{f}})) {
		return boolean_fn{nxx::functional::not_fn{f}};
	}


}  // namespace nxx::boolean

#endif  // ifndef NXX__BOOLEAN_HXX
#endif  // ifndef NXX__BOOLEAN_BOOLEAN_FN_HXX
