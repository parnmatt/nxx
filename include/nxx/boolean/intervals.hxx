#ifndef NXX__BOOLEAN_INTERVALS_HXX
#define NXX__BOOLEAN_INTERVALS_HXX
#ifndef NXX__BOOLEAN_HXX
#	error "please only include nxx/boolean.hxx; this is an implementation header"
#else  // ifndef NXX__BOOLEAN_HXX

namespace nxx::boolean {

	// open_interval
	// V in (Tl, Tr)  ==  V in ]Tl, Tr[  ==  Tl < V < Tr

	template <typename Tl, typename Tr, typename Cc = void, typename Cl = void, typename Cr = void>
	auto constexpr make_open_interval(Tl&& left, Tr&& right) noexcept(noexcept(make_logical_and<Cc>(
	    greater<Tl, Cl>{std::forward<Tl>(left)}, less<Tr, Cr>{std::forward<Tr>(right)}))) -> decltype(auto) {
		return make_logical_and<Cc>(greater<Tl, Cl>{std::forward<Tl>(left)},
		                            less<Tr, Cr>{std::forward<Tr>(right)});
	}

	template <typename Tl, typename Tr, typename Cc, typename Cl, typename Cr>
	auto constexpr operator<(greater<Tl, Cl, Cr, Cc> gt, Tr&& t) noexcept(noexcept(make_logical_and<Cc>(
	    std::move(*reinterpret_cast<greater<Tl, Cl>*>(&gt)), less<Tr, Cr>{std::forward<Tr>(t)})))
	    -> decltype(auto) {
		return make_logical_and<Cc>(std::move(*reinterpret_cast<greater<Tl, Cl>*>(&gt)),
		                            less<Tr, Cr>{std::forward<Tr>(t)});
	}

	template <typename Tl, typename Tr, typename Cc, typename Cl, typename Cr>
	auto constexpr operator<(Tl&& t, less<Tr, Cr, Cl, Cc> lt) noexcept(noexcept(make_logical_and<Cc>(
	    greater<Tl, Cl>{std::forward<Tl>(t)}, std::move(*reinterpret_cast<less<Tr, Cr>*>(lt)))))
	    -> decltype(auto) {
		return make_logical_and<Cc>(greater<Tl, Cl>{std::forward<Tl>(t)},
		                            std::move(*reinterpret_cast<less<Tr, Cr>*>(lt)));
	}


	// left_half_open_interval
	// right_half_closed_interval
	// V in (Tl, Tr]  ==  V in ]Tl, Tr]  ==  Tl < V <= Tr

	template <typename Tl, typename Tr, typename Cc = void, typename Cl = void, typename Cr = void>
	auto constexpr make_left_half_open_interval(Tl&& left, Tr&& right) noexcept(noexcept(make_logical_and<Cc>(
	    greater<Tl, Cl>{std::forward<Tl>(left)}, less_equal<Tr, Cr>{std::forward<Tr>(right)})))
	    -> decltype(auto) {
		return make_logical_and<Cc>(greater<Tl, Cl>{std::forward<Tl>(left)},
		                            less_equal<Tr, Cr>{std::forward<Tr>(right)});
	}

	template <typename Tl, typename Tr, typename Cc = void, typename Cl = void, typename Cr = void>
	auto constexpr make_right_half_closed_interval(Tl&& left, Tr&& right) noexcept(noexcept(
	    make_left_half_open_interval<Tr, Tl, Cc, Cl, Cr>(std::forward<Tl>(left), std::forward<Tr>(right))))
	    -> decltype(auto) {
		return make_left_half_open_interval<Tr, Tl, Cc, Cl, Cr>(std::forward<Tl>(left),
		                                                        std::forward<Tr>(right));
	}

	template <typename Tl, typename Tr, typename Cc, typename Cl, typename Cr>
	auto constexpr operator<=(greater<Tl, Cl, Cr, Cc> gt, Tr&& t) noexcept(noexcept(make_logical_and<Cc>(
	    std::move(*reinterpret_cast<greater<Tl, Cl>*>(&gt)), less_equal<Tr, Cr>{std::forward<Tr>(t)})))
	    -> decltype(auto) {
		return make_logical_and<Cc>(std::move(*reinterpret_cast<greater<Tl, Cl>*>(&gt)),
		                            less_equal<Tr, Cr>{std::forward<Tr>(t)});
	}

	template <typename Tl, typename Tr, typename Cc, typename Cl, typename Cr>
	auto constexpr operator<(Tl&& t, less_equal<Tr, Cr, Cl, Cc> le) noexcept(noexcept(make_logical_and<Cc>(
	    greater<Tl, Cl>{std::forward<Tl>(t)}, std::move(*reinterpret_cast<less_equal<Tr, Cr>*>(&le)))))
	    -> decltype(auto) {
		return make_logical_and<Cc>(greater<Tl, Cl>{std::forward<Tl>(t)},
		                            std::move(*reinterpret_cast<less_equal<Tr, Cr>*>(&le)));
	}


	// right_half_open_interval
	// left_half_closed_interval
	// V in [Tl, Tr)  ==  V in [Tl, Tr[  ==  Tl <= V < Tr

	template <typename Tl, typename Tr, typename Cc = void, typename Cl = void, typename Cr = void>
	auto constexpr make_right_half_open_interval(Tl&& left, Tr&& right) noexcept(
	    noexcept(make_logical_and<Cc>(greater_equal<Tl, Cc>{std::forward<Tl>(left)},
	                                  less<Tr, Cr>{std::forward<Tr>(right)}))) -> decltype(auto) {
		return make_logical_and<Cc>(greater_equal<Tl, Cc>{std::forward<Tl>(left)},
		                            less<Tr, Cr>{std::forward<Tr>(right)});
	}

	template <typename Tl, typename Tr, typename Cc = void, typename Cl = void, typename Cr = void>
	using left_half_closed_interval = right_half_open_interval<Tl, Tr, Cc, Cl, Cr>;

	template <typename Tl, typename Tr, typename Cc = void, typename Cl = void, typename Cr = void>
	auto constexpr make_left_half_closed_interval(Tl&& left, Tr&& right) noexcept(noexcept(
	    make_right_half_open_interval<Tl, Tr, Cc, Cl, Cr>(std::forward<Tl>(left), std::forward<Tr>(right))))
	    -> decltype(auto) {
		return make_right_half_open_interval<Tl, Tr, Cc, Cl, Cr>(std::forward<Tl>(left),
		                                                         std::forward<Tr>(right));
	}

	template <typename Tl, typename Tr, typename Cc, typename Cl, typename Cr>
	auto constexpr operator<(greater_equal<Tl, Cl, Cr, Cc> ge, Tr&& t) noexcept(noexcept(make_logical_and<Cc>(
	    std::move(*reinterpret_cast<greater_equal<Tl, Cl>*>(&ge)), less<Tr, Cr>{std::forward<Tr>(t)})))
	    -> decltype(auto) {
		return make_logical_and<Cc>(std::move(*reinterpret_cast<greater_equal<Tl, Cl>*>(&ge)),
		                            less<Tr, Cr>{std::forward<Tr>(t)});
	}

	template <typename Tl, typename Tr, typename Cc, typename Cl, typename Cr>
	auto constexpr operator<=(Tl&& t, less<Tr, Cr, Cl, Cc> lt) noexcept(noexcept(make_logical_and<Cc>(
	    greater_equal<Tl, Cl>(std::forward<Tl>(t)), std::move(*reinterpret_cast<less<Tr, Cr>*>(&lt)))))
	    -> decltype(auto) {
		return make_logical_and<Cc>(greater_equal<Tl, Cl>(std::forward<Tl>(t)),
		                            std::move(*reinterpret_cast<less<Tr, Cr>*>(&lt)));
	}


	// closed_interval
	// V in [Tl, Tr]  ==  Tl <= V <= Tr

	template <typename Tl, typename Tr, typename Cc = void, typename Cl = void, typename Cr = void>
	auto constexpr make_closed_interval(Tl&& left, Tr&& right) noexcept(noexcept(make_logical_and<Cc>(
	    greater_equal<Tl, Cl>{std::forward<Tl>(left)}, less_equal<Tr, Cr>{std::forward<Tr>(right)})))
	    -> decltype(auto) {
		return make_logical_and<Cc>(greater_equal<Tl, Cl>{std::forward<Tl>(left)},
		                            less_equal<Tr, Cr>{std::forward<Tr>(right)});
	}

	template <typename Tl, typename Tr, typename Cc, typename Cl, typename Cr>
	auto constexpr operator<=(greater_equal<Tl, Cl, Cr, Cc> ge, Tr&& t) noexcept(
	    noexcept(make_logical_and<Cc>(std::move(*reinterpret_cast<greater_equal<Tl, Cl>*>(&ge)),
	                                  less_equal<Tr, Cr>{std::forward<Tr>(t)}))) -> decltype(auto) {
		return make_logical_and<Cc>(std::move(*reinterpret_cast<greater_equal<Tl, Cl>*>(&ge)),
		                            less_equal<Tr, Cr>{std::forward<Tr>(t)});
	}

	template <typename Tl, typename Tr, typename Cc, typename Cl, typename Cr>
	auto constexpr operator<=(Tl&& t, less_equal<Tr, Cr, Cl, Cc> le) noexcept(noexcept(make_logical_and<Cc>(
	    greater_equal<Tl, Cl>{std::forward<Tl>(t)}, std::move(*reinterpret_cast<less_equal<Tr, Cr>*>(&le)))))
	    -> decltype(auto) {
		return make_logical_and<Cc>(greater_equal<Tl, Cl>{std::forward<Tl>(t)},
		                            std::move(*reinterpret_cast<less_equal<Tr, Cr>*>(&le)));
	}

}  // namespace nxx::boolean

#endif  // ifndef NXX__BOOLEAN_HXX
#endif  // ifndef NXX__BOOLEAN_INTERVALS_HXX
