#ifndef NXX__BOOLEAN_STATIC_BOOLEAN_HXX
#define NXX__BOOLEAN_STATIC_BOOLEAN_HXX
#ifndef NXX__BOOLEAN_HXX
#	error "please only include nxx/boolean.hxx; this is an implementation header"
#else  // ifndef NXX__BOOLEAN_HXX

namespace nxx::boolean {

	template <bool B>
	struct static_boolean : boolean_fn<void> {
		bool constexpr static value = B;

		constexpr operator bool() const noexcept { return value; }

		template <typename... Args>
		bool constexpr operator()(Args&&...) const noexcept {
			return value;
		}
	};

	auto constexpr static_true = static_true_t{};
	auto constexpr static_false = static_false_t{};

	// operators

	// logical and

	template <bool Lhs, bool Rhs>
	auto constexpr operator&&(static_boolean<Lhs>, static_boolean<Lhs>) noexcept {
		return static_boolean<Lhs && Rhs>{};
	}

	template <typename Rhs>
	auto constexpr operator&&(static_boolean<true>, Rhs&& rhs) noexcept -> decltype(auto) {
		return std::forward<Rhs>(rhs);
	}

	template <typename Rhs>
	auto constexpr operator&&(static_boolean<false>, Rhs&& rhs) noexcept {
		return static_boolean<false>{};
	}

	template <typename Lhs>
	auto constexpr operator&&(Lhs&& lhs, static_boolean<true>) noexcept -> decltype(auto) {
		return std::forward<Lhs>(lhs);
	}

	template <typename Lhs>
	auto constexpr operator&&(Lhs&& lhs, static_boolean<false>) noexcept {
		return static_boolean<false>{};
	}


	// logical or

	template <bool Lhs, bool Rhs>
	auto constexpr operator||(static_boolean<Lhs>, static_boolean<Lhs>) noexcept {
		return static_boolean < Lhs || Rhs > {};
	}

	template <typename Rhs>
	auto constexpr operator||(static_boolean<true>, Rhs&& rhs) noexcept {
		return static_boolean<true>{};
	}

	template <typename Rhs>
	auto constexpr operator||(static_boolean<false>, Rhs&& rhs) noexcept -> decltype(auto) {
		return std::forward<Rhs>(rhs);
	}

	template <typename Lhs>
	auto constexpr operator||(Lhs&& lhs, static_boolean<true>) noexcept {
		return static_boolean<true>{};
	}

	template <typename Lhs>
	auto constexpr operator||(Lhs&& lhs, static_boolean<false>) noexcept -> decltype(auto) {
		return std::forward<Lhs>(lhs);
	}

	// logical not

	template <bool B>
	auto constexpr operator!(static_boolean<B>) noexcept {
		return static_boolean<!B>{};
	}

}  // namespace nxx::boolean

#endif  // ifndef NXX__BOOLEAN_HXX
#endif  // ifndef NXX__BOOLEAN_STATIC_BOOLEAN_HXX
