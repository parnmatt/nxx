#ifndef NXX__ITERATOR_HXX
#define NXX__ITERATOR_HXX

#include <iterator>

#include <functional>
#include <optional>
#include <ostream>
#include <string>
#include <type_traits>
#include <utility>
#include <variant>

namespace nxx::iterator {
	namespace detail {

		template <typename T>
		struct value_type {
			using type = T;
		};

		template <typename T>
		using value_type_t = typename value_type<T>::type;

		template <typename T>
		struct value_type<std::reference_wrapper<T>> : value_type<T> {};

	}  // namespace detail

	template <typename T>
	class value {
	public:
		using iterator_category = std::forward_iterator_tag;
		using size_type = std::size_t;
		using difference_type = std::make_signed_t<size_type>;
		using value_type = detail::value_type_t<T>;
		using reference = value_type&;
		using const_reference = value_type const&;
		using pointer = value_type*;

		value() = default;

		explicit value(T&& val, size_type count)
		    : m_value{std::forward<T>(val)}
		    , m_count{count} {}

		explicit value(T&& val)
		    : m_value{std::forward<T>(val)} {}

		auto operator++() -> value& {
			if (++m_index == m_count)
				*this = value{};
			return *this;
		}

		auto operator++(int) -> value {
			auto prev = *this;
			++(*this);
			return prev;
		}

		auto operator*() -> reference { return *m_value; }
		auto operator*() const -> const_reference { return *m_value; }
		auto operator-> () -> pointer { return &**this; }

		friend bool operator==(value const& lhs, value const& rhs) {
			return lhs.m_value == rhs.m_value && lhs.m_index == rhs.m_index && lhs.m_count == rhs.m_count;
		}

		friend bool operator!=(value const& lhs, value const& rhs) { return !(lhs == rhs); }

	private:
		size_type m_index{};
		size_type m_count{static_cast<size_type>(-1)};
		std::optional<T> m_value{};
	};

	template <typename T>
	value(T&)->value<std::reference_wrapper<T>>;

	template <typename T>
	value(T &&)->value<T>;

	template <typename Container, typename Reference = typename Container::reference,
	          typename Size = typename Container::size_type>
	class subscript {
	public:
		using container_type = Container;
		using iterator_category = std::input_iterator_tag;
		using reference = Reference;
		using value_type = std::remove_const_t<std::remove_reference_t<reference>>;
		using size_type = Size;
		using difference_type = std::make_signed_t<size_type>;
		using pointer = value_type*;

		subscript() = default;

		explicit subscript(container_type& c, size_type index, size_type count)
		    : m_c{&c}
		    , m_index{index}
		    , m_count{count} {}

		explicit subscript(container_type& c, size_type count)
		    : subscript{c, {}, count} {}

		explicit subscript(container_type& c)
		    : subscript{c, c.size()} {}

		auto operator++() -> subscript& {
			if (m_c && (++m_index == m_count))
				*this = subscript{};
			return *this;
		}

		auto operator++(int) -> subscript {
			auto prev = *this;
			++(*this);
			return prev;
		}

		auto operator*() -> reference { return (*m_c)[m_index]; }
		auto operator-> () -> pointer { return &**this; }

		friend bool operator==(subscript const& lhs, subscript const& rhs) {
			return lhs.m_c == rhs.m_c && lhs.m_count == rhs.m_count && lhs.m_index == rhs.m_index;
		}

		friend bool operator!=(subscript const& lhs, subscript const& rhs) { return !(lhs == rhs); }

	private:
		size_type m_index{};
		size_type m_count{};
		container_type* m_c{};
	};

	// output iterators

	struct blackhole {
		using iterator_category = std::output_iterator_tag;
		using value_type = void;
		using difference_type = void;
		using pointer = void;
		using reference = void;

		template <typename T>
		auto operator=(T const&) -> blackhole& {
			return *this;
		}

		auto operator++() -> blackhole& { return *this; }
		auto operator++(int) -> blackhole& { return *this; }
		auto operator*() -> blackhole& { return *this; }
	};


	// ostream delimiters

	template <typename T, typename CharT = char, typename Traits = std::char_traits<CharT>>
	class prefix_ostream {
	public:
		using iterator_category = std::output_iterator_tag;
		using value_type = void;
		using difference_type = void;
		using pointer = void;
		using reference = void;

		using char_type = CharT;
		using traits_type = Traits;
		using ostream_type = std::basic_ostream<char_type, traits_type>;

		explicit prefix_ostream(ostream_type& stream)
		    : m_os{&stream} {}
		explicit prefix_ostream(ostream_type& stream, char_type const delim)
		    : m_os{&stream}
		    , m_delim{delim} {}
		explicit prefix_ostream(ostream_type& stream, char_type const* delim)
		    : m_os{&stream}
		    , m_delim{delim} {}

		auto operator=(T const& value) -> prefix_ostream& {
			if (m_delim)
				std::visit([&](auto&& delim) { *m_os << delim; }, *m_delim);
			*m_os << value;
			return *this;
		}

		auto operator++() -> prefix_ostream& { return *this; }
		auto operator++(int) -> prefix_ostream& { return *this; }
		auto operator*() -> prefix_ostream& { return *this; }

	private:
		ostream_type* m_os{};
		std::optional<std::variant<char_type const, char_type const*>> m_delim{};
	};


	template <typename T, typename CharT = char, typename Traits = std::char_traits<CharT>>
	class infix_ostream {
	public:
		using iterator_category = std::output_iterator_tag;
		using value_type = void;
		using difference_type = void;
		using pointer = void;
		using reference = void;

		using char_type = CharT;
		using traits_type = Traits;
		using ostream_type = std::basic_ostream<char_type, traits_type>;

		explicit infix_ostream(ostream_type& stream)
		    : m_os{&stream} {}
		explicit infix_ostream(ostream_type& stream, char_type const delim)
		    : m_os{&stream}
		    , m_delim{delim} {}
		explicit infix_ostream(ostream_type& stream, char_type const* delim)
		    : m_os{&stream}
		    , m_delim{delim} {}

		auto operator=(T const& value) -> infix_ostream& {
			if (m_delim && !m_first)
				std::visit([&](auto&& delim) { *m_os << delim; }, *m_delim);
			*m_os << value;
			m_first = false;
			return *this;
		}

		auto operator++() -> infix_ostream& { return *this; }
		auto operator++(int) -> infix_ostream& { return *this; }
		auto operator*() -> infix_ostream& { return *this; }

	private:
		ostream_type* m_os{};
		std::optional<std::variant<char_type const, char_type const*>> m_delim{};
		bool m_first = true;
	};


	template <typename T, typename CharT = char, typename Traits = std::char_traits<CharT>>
	class postfix_ostream {
	public:
		using iterator_category = std::output_iterator_tag;
		using value_type = void;
		using difference_type = void;
		using pointer = void;
		using reference = void;

		using char_type = CharT;
		using traits_type = Traits;
		using ostream_type = std::basic_ostream<char_type, traits_type>;

		explicit postfix_ostream(ostream_type& stream)
		    : m_os{&stream} {}
		explicit postfix_ostream(ostream_type& stream, char_type const delim)
		    : m_os{&stream}
		    , m_delim{delim} {}
		explicit postfix_ostream(ostream_type& stream, char_type const* delim)
		    : m_os{&stream}
		    , m_delim{delim} {}

		auto operator=(T const& value) -> postfix_ostream& {
			*m_os << value;
			if (m_delim)
				std::visit([&](auto&& delim) { *m_os << delim; }, *m_delim);
			return *this;
		}

		auto operator++() -> postfix_ostream& { return *this; }
		auto operator++(int) -> postfix_ostream& { return *this; }
		auto operator*() -> postfix_ostream& { return *this; }

	private:
		ostream_type* m_os{};
		std::optional<std::variant<char_type const, char_type const*>> m_delim{};
	};

	template <typename T, typename CharT = char, typename Traits = std::char_traits<CharT>>
	using suffix_ostream = postfix_ostream<T, CharT, Traits>;


	// inserters

	template <typename Container>
	class insert {
	public:
		using iterator_category = std::output_iterator_tag;
		using value_type = void;
		using difference_type = void;
		using pointer = void;
		using reference = void;

		using container_type = Container;

		explicit insert(container_type& c, std::optional<typename container_type::iterator> hint = {})
		    : m_container{&c}
		    , m_hint{hint} {}

		auto operator=(typename container_type::value_type const& value) -> insert& {
			if (m_hint)
				m_container->insert(*m_hint, value);
			else
				m_container->insert(value);
			return *this;
		}

		auto operator=(typename container_type::value_type&& value) -> insert& {
			if (m_hint)
				m_container->insert(*m_hint, std::move(value));
			else
				m_container->insert(std::move(value));
			return *this;
		}

		auto operator++() -> insert& { return *this; }
		auto operator++(int) -> insert& { return *this; }
		auto operator*() -> insert& { return *this; }

	private:
		container_type* m_container{};
		std::optional<typename container_type::iterator> m_hint{};
	};


	namespace detail {
		template <typename Container>
		using positional_insert = std::insert_iterator<Container>;
	}  // namespace detail

	template <typename Container>
	auto positional_insert(Container& c, typename Container::iterator i) {
		return detail::positional_insert<Container>{c, i};
	}


	// filters

	template <
	    typename OutputIterator, typename UnaryPredicate,
	    typename = std::enable_if_t<std::is_same_v<
	        typename std::iterator_traits<OutputIterator>::iterator_category, std::output_iterator_tag>>>
	class filter_output_if {
	public:
		using iterator_category = std::output_iterator_tag;
		using value_type = void;
		using difference_type = void;
		using pointer = void;
		using reference = void;

		explicit filter_output_if(OutputIterator it, UnaryPredicate p)
		    : m_it{it}
		    , m_p{p} {}

		template <typename T>
		auto operator=(T const& value)
		    -> std::enable_if_t<std::is_invocable_r_v<bool, UnaryPredicate, decltype(value)>,
		                        filter_output_if&> {
			if (m_p(value)) {
				*m_it = value;
				++m_it;
			}

			return *this;
		}

		auto operator++() -> filter_output_if& { return *this; }
		auto operator++(int) -> filter_output_if& { return *this; }
		auto operator*() -> filter_output_if& { return *this; }

	private:
		OutputIterator m_it;
		UnaryPredicate m_p;
	};


	template <typename OutputIterator>
	auto filter_output(OutputIterator it) {
		return filter_output_if{it, [](auto const& value) { return static_cast<bool>(value); }};
	}


	template <
	    typename OutputIterator,
	    typename = std::enable_if_t<std::is_same_v<
	        typename std::iterator_traits<OutputIterator>::iterator_category, std::output_iterator_tag>>>
	class unwrap_optional {
	public:
		using iterator_category = std::output_iterator_tag;
		using value_type = void;
		using difference_type = void;
		using pointer = void;
		using reference = void;

		explicit unwrap_optional(OutputIterator it)
		    : m_it{it} {}

		template <typename T>
		auto operator=(std::optional<T> const& opt) -> unwrap_optional& {
			if (opt.has_value())
				*m_it = opt.value();
			return *this;
		}

		auto operator++() -> unwrap_optional& { return *this; }
		auto operator++(int) -> unwrap_optional& { return *this; }
		auto operator*() -> unwrap_optional& { return *this; }

	private:
		OutputIterator m_it;
	};


	// transformers

	template <
	    typename OutputIterator, typename UnaryTransformFunction,
	    typename = std::enable_if_t<std::is_same_v<
	        typename std::iterator_traits<OutputIterator>::iterator_category, std::output_iterator_tag>>>
	class transform_output {
	public:
		using iterator_category = std::output_iterator_tag;
		using value_type = void;
		using difference_type = void;
		using pointer = void;
		using reference = void;

		explicit transform_output(OutputIterator it, UnaryTransformFunction f)
		    : m_it{it}
		    , m_f{f} {}

		template <typename T>
		auto operator=(T const& value) -> transform_output& {
			*m_it = std::invoke(m_f, value);
			return *this;
		}

		auto operator++() -> transform_output& { return *this; }
		auto operator++(int) -> transform_output& { return *this; }
		auto operator*() -> transform_output& { return *this; }


	private:
		OutputIterator m_it;
		UnaryTransformFunction m_f;
	};

}  // namespace nxx::iterator

#endif  // #ifndef NXX__ITERATOR_HXX
