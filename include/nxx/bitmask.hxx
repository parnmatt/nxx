#ifndef NXX__BITMASK_HXX
#define NXX__BITMASK_HXX

#include <type_traits>

namespace nxx::bitmask {

	template <typename T>
	struct enable : std::false_type {};

	template <typename T>
	bool constexpr enable_v = enable<T>::value;

	namespace traits {

		template <typename T, typename = void>
		struct is_possible_bitmask : std::false_type {};

		template <typename T>
		struct is_possible_bitmask<T, std::enable_if_t<std::is_enum_v<T>>>
		    : std::is_unsigned<std::underlying_type_t<T>> {};

		template <typename T>
		bool constexpr is_possible_bitmask_v = is_possible_bitmask<T>::value;

		template <typename T>
		using is_bitmask = std::conjunction<is_possible_bitmask<T>, enable<T>>;

		template <typename T>
		bool constexpr is_bitmask_v = is_bitmask<T>::value;

	}  // namespace traits


	template <typename Bitmask>
	auto constexpr test(Bitmask mask, Bitmask test) {
		return (mask & test) == test;
	}

	template <typename... Bitmasks>
	auto constexpr construct(Bitmasks... masks) {
		return (... | masks);
	}

	template <typename Bitmask, typename... Bitmasks>
	void constexpr set(Bitmask& mask, Bitmasks... masks) {
		mask |= construct(masks...);
	}

	template <typename Bitmask, typename... Bitmasks>
	void constexpr unset(Bitmask& mask, Bitmasks... masks) {
		mask &= ~construct(masks...);
	}

}  // namespace nxx::bitmask


template <typename Bitmask>
auto constexpr operator&(Bitmask lhs, Bitmask rhs)
    -> std::enable_if_t<nxx::bitmask::traits::is_bitmask_v<Bitmask>, Bitmask> {
	using bm_t = std::underlying_type_t<Bitmask>;
	return static_cast<Bitmask>(static_cast<bm_t>(lhs) & static_cast<bm_t>(rhs));
}

template <typename Bitmask>
auto constexpr operator|(Bitmask lhs, Bitmask rhs)
    -> std::enable_if_t<nxx::bitmask::traits::is_bitmask_v<Bitmask>, Bitmask> {
	using bm_t = std::underlying_type_t<Bitmask>;
	return static_cast<Bitmask>(static_cast<bm_t>(lhs) | static_cast<bm_t>(rhs));
}

template <typename Bitmask>
auto constexpr operator^(Bitmask lhs, Bitmask rhs)
    -> std::enable_if_t<nxx::bitmask::traits::is_bitmask_v<Bitmask>, Bitmask> {
	using bm_t = std::underlying_type_t<Bitmask>;
	return static_cast<Bitmask>(static_cast<bm_t>(lhs) ^ static_cast<bm_t>(rhs));
}

template <typename Bitmask>
auto constexpr operator~(Bitmask rhs)
    -> std::enable_if_t<nxx::bitmask::traits::is_bitmask_v<Bitmask>, Bitmask> {
	using bm_t = std::underlying_type_t<Bitmask>;
	return static_cast<Bitmask>(~static_cast<bm_t>(rhs));
}

template <typename Bitmask>
auto constexpr operator&=(Bitmask& lhs, Bitmask rhs)
    -> std::enable_if_t<nxx::bitmask::traits::is_bitmask_v<Bitmask>, Bitmask&> {
	lhs = lhs & rhs;
	return lhs;
}

template <typename Bitmask>
auto constexpr operator|=(Bitmask& lhs, Bitmask rhs)
    -> std::enable_if_t<nxx::bitmask::traits::is_bitmask_v<Bitmask>, Bitmask&> {
	lhs = lhs | rhs;
	return lhs;
}

template <typename Bitmask>
auto constexpr operator^=(Bitmask& lhs, Bitmask rhs)
    -> std::enable_if_t<nxx::bitmask::traits::is_bitmask_v<Bitmask>, Bitmask&> {
	lhs = lhs ^ rhs;
	return lhs;
}

#endif  // ifndef NXX__BITMASK_HXX
