#ifndef NXX__VARIANT_HXX
#define NXX__VARIANT_HXX

namespace nxx {

	template <typename... Ts>
	struct visitor : Ts... {
		using Ts::operator()...;
	};

	template <typename... Ts>
	visitor(Ts...)->visitor<Ts...>;

}  // namespace nxx

#endif  // ifndef NXX__VARIANT_HXX
