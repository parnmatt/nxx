#ifndef NXX__TYPE_TRAITS_HXX
#define NXX__TYPE_TRAITS_HXX

#include <cstddef>
#include <type_traits>

#include <memory>

namespace nxx {

	template <typename Tag = void>
	struct empty_base {};

	template <bool B, typename T>
	using optional_base = std::conditional_t<B, T, empty_base<T>>;

}  // namespace nxx

namespace nxx::traits {

	template <typename T>
	struct identity {
		using type = T;
	};

	template <typename T>
	using identity_t = typename identity<T>::type;


	template <std::size_t I>
	using index_constant = std::integral_constant<std::size_t, I>;

	template <std::size_t I>
	auto constexpr index_constant_v = index_constant<I>::value;


	template <template <typename...> typename T, typename S>
	struct is_specialization_of : std::false_type {};

	template <template <typename...> typename T, typename S>
	bool constexpr is_specialization_of_v = is_specialization_of<T, S>::value;

	template <template <typename...> typename T, typename... Args>
	struct is_specialization_of<T, T<Args...>> : std::true_type {};

	template <typename T, typename... Ts>
	using contains = std::disjunction<std::is_same<T, Ts>...>;

	template <typename T, typename... Ts>
	bool constexpr contains_v = contains<T, Ts...>::value;


	template <typename C, typename T>
	struct apply_const : std::remove_const<T> {};

	template <typename C, typename T>
	using apply_const_t = typename apply_const<C, T>::type;

	template <typename C, typename T>
	struct apply_const<C const, T> : std::add_const<T> {};


	template <typename V, typename T>
	struct apply_volatile : std::remove_volatile<T> {};

	template <typename V, typename T>
	using apply_volatile_t = typename apply_volatile<V, T>::type;

	template <typename V, typename T>
	struct apply_volatile<V volatile, T> : std::add_volatile<T> {};


	template <typename CV, typename T>
	using apply_cv = apply_const<CV, apply_volatile_t<CV, T>>;

	template <typename CV, typename T>
	using apply_cv_t = typename apply_cv<CV, T>::type;


	template <typename R, typename T>
	struct apply_reference : std::remove_reference<T> {};

	template <typename R, typename T>
	using apply_reference_t = typename apply_reference<R, T>::type;

	template <typename R, typename T>
	struct apply_reference<R&, T> : std::add_lvalue_reference<T> {};

	template <typename R, typename T>
	struct apply_reference<R&&, T> : std::add_rvalue_reference<T> {};


	template <typename CVR, typename T>
	using apply_cvref
	    = apply_reference<CVR, apply_cv_t<std::remove_reference_t<CVR>, std::remove_reference_t<T>>>;

	template <typename CVR, typename T>
	using apply_cvref_t = typename apply_cvref<CVR, T>::type;


	template <typename T>
	struct remove_cvref : std::remove_cv<std::remove_reference_t<T>> {};

	template <typename T>
	using remove_cvref_t = typename remove_cvref<T>::type;


	template <typename T>
	using is_value
	    = std::disjunction<std::is_arithmetic<T>, std::is_enum<T>, std::is_class<T>, std::is_union<T>>;

	template <typename T>
	bool constexpr is_value_v = is_value<T>::value;

	template <typename T>
	using is_value_pointer = std::conjunction<std::is_pointer<T>, is_value<std::remove_pointer<T>>>;

	template <typename T>
	bool constexpr is_value_pointer_v = is_value_pointer<T>::value;

	template <typename T>
	using is_function_pointer
	    = std::conjunction<std::is_pointer<T>, std::is_function<std::remove_pointer_t<T>>>;

	template <typename T>
	bool constexpr is_function_pointer_v = is_function_pointer<T>::value;


	namespace detail {
		template <typename T>
		void test_noexcept(T) noexcept;
	}

	template <typename From, typename To>
	struct is_nothrow_convertible {
		bool constexpr static value
		    = std::is_convertible_v<From, To> && noexcept(detail::test_noexcept<To>(std::declval<From>()));
	};

	template <typename From, typename To>
	bool constexpr is_nothrow_convertible_v = is_nothrow_convertible<From, To>::value;


	namespace detail {

		struct nonesuch {
			nonesuch() = delete;
			nonesuch(nonesuch const&) = delete;
			void operator=(nonesuch const&) = delete;
			~nonesuch() = delete;
		};

		template <typename Default, typename AlwaysVoid, template <typename...> class Op, typename... Args>
		struct detector {
			using value_t = std::false_type;
			using type = Default;
		};

		template <typename Default, template <typename...> class Op, typename... Args>
		struct detector<Default, std::void_t<Op<Args...>>, Op, Args...> {
			using value_t = std::true_type;
			using type = Op<Args...>;
		};

	}  // namespace detail

	template <template <typename...> class Op, typename... Args>
	using is_detected = typename detail::detector<detail::nonesuch, void, Op, Args...>::value_t;

	template <template <typename...> class Op, typename... Args>
	bool constexpr is_detected_v = is_detected<Op, Args...>::value;

	template <template <typename...> class Op, typename... Args>
	using detected_t = typename detail::detector<detail::nonesuch, void, Op, Args...>::type;

	template <typename Default, template <typename...> class Op, typename... Args>
	using detected_or = typename detail::detector<Default, void, Op, Args...>;

	template <typename Default, template <typename...> class Op, typename... Args>
	using detected_or_t = typename detected_or<Default, Op, Args...>::type;

	template <typename Expected, template <typename...> class Op, typename... Args>
	using is_detected_exact = std::is_same<Expected, detected_t<Op, Args...>>;

	template <typename Expected, template <typename...> class Op, typename... Args>
	bool constexpr is_detected_exact_v = std::is_same<Expected, detected_t<Op, Args...>>::value;

	template <typename To, template <typename...> class Op, typename... Args>
	using is_detected_convertible = std::is_convertible<detected_t<Op, Args...>, To>;

	template <typename To, template <typename...> class Op, typename... Args>
	bool constexpr is_detected_convertible_v = is_detected_convertible<To, Op, Args...>::value;


	template <typename T>
	using has_type_t = typename T::type;

	template <typename T>
	using has_type = is_detected<has_type_t, T>;

	template <typename T>
	bool constexpr has_type_v = has_type<T>::value;


	template <typename T>
	using has_value_t = decltype(T::value);

	template <typename T>
	using has_value = is_detected<has_value_t, T>;

	template <typename T>
	bool constexpr has_value_v = has_value<T>::value;


	template <typename T>
	using has_value_type_t = typename T::value_type;

	template <typename T>
	using has_value_type = is_detected<has_value_type_t, T>;

	template <typename T>
	bool constexpr has_value_type_v = has_value_type<T>::value;


	template <typename T>
	using has_size_type_t = typename T::size_type;

	template <typename T>
	using has_size_type = is_detected<has_size_type_t, T>;

	template <typename T>
	bool constexpr has_size_type_v = has_size_type<T>::value;


	template <typename T>
	using has_reference_t = typename T::reference;

	template <typename T>
	using has_reference = is_detected<has_reference_t, T>;

	template <typename T>
	bool constexpr has_reference_v = has_reference<T>::value;


	template <typename T>
	using has_const_reference_t = typename T::const_reference;

	template <typename T>
	using has_const_reference = is_detected<has_const_reference_t, T>;

	template <typename T>
	bool constexpr has_const_reference_v = has_const_reference<T>::value;


	template <typename T>
	using has_iterator_t = typename T::iterator;

	template <typename T>
	using has_iterator = is_detected<has_iterator_t, T>;

	template <typename T>
	bool constexpr has_iterator_v = has_iterator<T>::value;


	template <typename T>
	using has_const_iterator_t = typename T::const_iterator;

	template <typename T>
	using has_const_iterator = is_detected<has_const_iterator_t, T>;

	template <typename T>
	bool constexpr has_const_iterator_v = has_const_iterator<T>::value;


	template <typename T>
	using has_reverse_iterator_t = typename T::reverse_iterator;

	template <typename T>
	using has_reverse_iterator = is_detected<has_reverse_iterator_t, T>;

	template <typename T>
	bool constexpr has_reverse_iterator_v = has_reverse_iterator<T>::value;


	template <typename T>
	using has_const_reverse_iterator_t = typename T::const_reverse_iterator;

	template <typename T>
	using has_const_reverse_iterator = is_detected<has_const_reverse_iterator_t, T>;

	template <typename T>
	bool constexpr has_const_reverse_iterator_v = has_const_reverse_iterator<T>::value;


	template <typename T>
	using has_iterator_category_t = typename T::iterator_category;

	template <typename T>
	using has_iterator_category = is_detected<has_iterator_category_t, T>;

	template <typename T>
	bool constexpr has_iterator_category_v = has_iterator_category<T>::value;

	template <typename T>
	using is_iterator = has_iterator_category<std::iterator_traits<T>>;

	template <typename T>
	bool constexpr is_iterator_v = is_iterator<T>::value;

}  // namespace nxx::traits

#endif  // #ifndef NXX__TYPE_TRAITS_HXX
