#ifndef NXX__STDLIB_HXX
#define NXX__STDLIB_HXX

#include <cstdlib>
#include <optional>
#include <string_view>

namespace nxx {

	auto inline getenv(std::string_view const env_var) -> std::optional<std::string> {
		auto var = std::getenv(env_var.data());
		if (!var)
			return {};
		return var;
	}

}  // namespace nxx

#endif  // ifndef NXX__STDLIB_HXX
