#ifndef NXX__MEMORY_HXX
#define NXX__MEMORY_HXX

#include <memory>

namespace nxx::memory {

	template <typename T, typename D = std::default_delete<T>, typename OT, typename OD>
	auto dynamic_pointer_cast_via_ref(std::unique_ptr<OT, OD> ptr) {
		auto& value = dynamic_cast<T&>(*ptr);
		ptr.release();
		return std::unique_ptr<T, D>{std::addressof(value)};
	}

	template <typename T, typename D = std::default_delete<T>, typename OT, typename OD>
	auto dynamic_pointer_cast_via_ptr(std::unique_ptr<OT, OD> ptr) noexcept {
		return std::unique_ptr<T, D>{dynamic_cast<T*>(ptr.release())};
	}

}  // namespace nxx::memory

#endif  // ifndef NXX__MEMORY_HXX
