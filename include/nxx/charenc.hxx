#ifndef NXX__CHARENC_HXX
#define NXX__CHARENC_HXX

#include <cuchar>
#include <iterator>
#include <locale>
#include <memory>
#include <optional>
#include <string>
#include <string_view>
#include <type_traits>
#include <utility>

#include <nxx/functional.hxx>

namespace nxx::charenc {

	using char8_t = char;  // added in C++20 as unsigned char (char needed C++17)


	// C conversion functions to overloaded function

	auto inline cconvert(char8_t* to, char16_t from, std::mbstate_t* state) noexcept {
		return std::c16rtomb(to, from, state);
	}

	auto inline cconvert(char16_t* to, char8_t const* from, std::size_t n, std::mbstate_t* state) noexcept {
		return std::mbrtoc16(to, from, n, state);
	}

	auto inline cconvert(char8_t* to, char32_t from, std::mbstate_t* state) noexcept {
		return std::c32rtomb(to, from, state);
	}

	auto inline cconvert(char32_t* to, char8_t const* from, std::size_t n, std::mbstate_t* state) noexcept {
		return std::mbrtoc32(to, from, n, state);
	}

	auto inline cconvert(char8_t* to, wchar_t from, std::mbstate_t* state) noexcept {
		return std::wcrtomb(to, from, state);
	}

	auto inline cconvert(wchar_t* to, char8_t const* from, std::size_t n, std::mbstate_t* state) noexcept {
		return std::mbrtowc(to, from, n, state);
	}


	// useful std::codecvt aliases

	template <typename CharT>
	using convert_t = std::codecvt<CharT, char8_t, std::mbstate_t>;

	using convert8_t = convert_t<char8_t>;
	using convert16_t = convert_t<char16_t>;
	using convert32_t = convert_t<char32_t>;
	using wconvert_t = convert_t<wchar_t>;


	// conversions

	namespace detail {

		// if ToCharT is not char8_t; FromCharT is assumed to be
		template <typename ToCharT, typename FromCharT, typename Facet>
		auto convert_u8_impl(std::basic_string_view<FromCharT> from, Facet const& facet) noexcept
		    -> std::optional<std::basic_string<ToCharT>> {
			auto const from_size = std::size(from);
			auto from_first = std::data(from);
			auto from_last = from_first + from_size;

			auto to_size = from_size * facet.max_length();
			auto to = std::basic_string<ToCharT>(to_size, 0);
			auto to_first = std::data(to);
			auto to_last = to_first + to_size;

			bool constexpr to_u8 = std::is_same_v<ToCharT, char8_t>;
			using convert_t = std::conditional_t<to_u8, decltype(&Facet::out), decltype(&Facet::in)>;

			auto convert = convert_t{};
			if constexpr (to_u8)
				convert = &Facet::out;
			else
				convert = &Facet::in;

			auto state = std::mbstate_t{};
			auto const result = functional::invoke(convert, facet, state, from_first, from_last, from_last,
			                                       to_first, to_last, to_last);

			if (result != std::codecvt_base::ok)
				return {};

			to_size = to_last - to_first;
			to.resize(to_size);

			return to;
		}


		// to char8_t
		template <typename CharT>
		auto inline convert_to_u8_helper(std::basic_string_view<CharT> from,
		                                 std::locale const& loc) noexcept {
			auto const& facet = std::use_facet<convert_t<CharT>>(loc);
			return convert_u8_impl<char8_t, CharT>(from, facet);
		}

		auto inline convert_to_u8(std::string_view from, std::locale const&) noexcept {
			return std::string{from};
		}

		auto inline convert_to_u8(std::u16string_view from, std::locale const& loc) noexcept {
			return convert_to_u8_helper(from, loc);
		}

		auto inline convert_to_u8(std::u32string_view from, std::locale const& loc) noexcept {
			return convert_to_u8_helper(from, loc);
		}

		auto inline convert_to_u8(std::wstring_view from, std::locale const& loc) noexcept {
			return convert_to_u8_helper(from, loc);
		}


		// from char8_t
		template <typename CharT>
		auto inline convert_from_u8(std::string_view from, std::locale const& loc) noexcept {
			auto const& facet = std::use_facet<convert_t<CharT>>(loc);
			return convert_u8_impl<CharT, char8_t>(from, facet);
		}


		// general conversions
		template <typename ToCharT, typename FromCharT>
		auto inline convert_impl(std::basic_string_view<FromCharT> from, std::locale const& loc) noexcept
		    -> std::optional<std::basic_string<ToCharT>> {
			if constexpr (std::is_same_v<FromCharT, ToCharT>)
				return std::basic_string<ToCharT>{from};

			auto via = convert_to_u8(from, loc);
			if (!via)
				return {};

			if constexpr (std::is_same_v<ToCharT, char8_t>)
				return via;

			return convert_from_u8<ToCharT>(*via, loc);
		}

		template <typename CharT>
		auto inline convert(std::string_view from, std::locale const& loc) noexcept {
			return convert_impl<CharT>(from, loc);
		}

		template <typename CharT>
		auto inline convert(std::u16string_view from, std::locale const& loc) noexcept {
			return convert_impl<CharT>(from, loc);
		}

		template <typename CharT>
		auto inline convert(std::u32string_view from, std::locale const& loc) noexcept {
			return convert_impl<CharT>(from, loc);
		}

		template <typename CharT>
		auto inline convert(std::wstring_view from, std::locale const& loc) noexcept {
			return convert_impl<CharT>(from, loc);
		}


		// characters

		template <typename ToCharT, typename FromCharT>
		auto inline convert_char(FromCharT from, std::locale const& loc) noexcept {
			auto const view = std::basic_string_view<FromCharT>{&from, 1};
			return convert<ToCharT>(view, loc);
		}

		template <typename CharT>
		auto inline convert(char8_t from, std::locale const& loc) noexcept {
			return convert_char<CharT>(from, loc);
		}

		template <typename CharT>
		auto inline convert(char16_t from, std::locale const& loc) noexcept {
			return convert_char<CharT>(from, loc);
		}

		template <typename CharT>
		auto inline convert(char32_t from, std::locale const& loc) noexcept {
			return convert_char<CharT>(from, loc);
		}

		template <typename CharT>
		auto inline convert(wchar_t from, std::locale const& loc) noexcept {
			return convert_char<CharT>(from, loc);
		}

	}  // namespace detail


	template <typename CharT, typename From>
	auto inline convert(From&& from, std::locale const& loc = {}) noexcept {
		return detail::convert<CharT>(std::forward<From>(from), loc);
	}

	template <typename From>
	auto inline convert8(From&& from, std::locale const& loc = {}) noexcept {
		return convert<char8_t>(std::forward<From>(from), loc);
	}

	template <typename From>
	auto inline convert16(From&& from, std::locale const& loc = {}) noexcept {
		return convert<char16_t>(std::forward<From>(from), loc);
	}

	template <typename From>
	auto inline convert32(From&& from, std::locale const& loc = {}) noexcept {
		return convert<char32_t>(std::forward<From>(from), loc);
	}

	template <typename From>
	auto inline wconvert(From&& from, std::locale const& loc = {}) noexcept {
		return convert<wchar_t>(std::forward<From>(from), loc);
	}


}  // namespace nxx::charenc

#endif  // ifndef NXX__CHARENC_HXX
