#ifndef NXX__FUNCTIONAL_FWD_HXX
#define NXX__FUNCTIONAL_FWD_HXX

#include <type_traits>
#include <utility>

#include <nxx/type_traits.hxx>

namespace nxx::functional {

	template <typename F, typename... Args>
	auto constexpr invoke(F&&, Args&&...) noexcept(std::is_nothrow_invocable_v<F, Args...>)
	    -> std::invoke_result_t<F, Args...>;

	template <typename...>
	struct wrap_args;

	struct id;
	using identity = id;

	template <typename = void>
	struct assign;

	template <typename...>
	struct compose;

	template <typename, typename...>
	struct partial;

	template <typename, typename...>
	struct curry;

	template <typename>
	struct flip;

	template <typename>
	struct discard;


	// map

	template <typename UnaryOp, typename... Args>
	struct map_result {
		using type
		    = std::conditional_t<std::disjunction_v<std::is_void<std::invoke_result_t<UnaryOp, Args>>...>,
		                         void, std::tuple<std::invoke_result_t<UnaryOp, Args>...>>;

		bool constexpr static value = std::conjunction_v<std::is_nothrow_invocable<UnaryOp, Args>...>;
	};

	template <typename UnaryOp, typename... Args>
	using map_result_t = typename map_result<UnaryOp, Args...>::type;

	template <typename UnaryOp, typename... Args>
	bool constexpr map_result_v = map_result<UnaryOp, Args...>::value;


	template <typename UnaryOp, typename... Args>
	auto constexpr map(UnaryOp&&, Args&&...) noexcept(map_result_v<UnaryOp, Args...>)
	    -> map_result_t<UnaryOp, Args...>;


	// folds

	template <typename Init, typename BinaryOp, typename UnaryOp, typename... Args>
	struct fold_result {
		using type = nxx::traits::remove_cvref_t<std::invoke_result_t<UnaryOp, Init>>;
		bool constexpr static value = std::conjunction_v<
		    std::is_nothrow_invocable<UnaryOp, Init>, std::is_nothrow_invocable<UnaryOp, Args>...,
		    std::is_nothrow_invocable<BinaryOp, type&, std::invoke_result_t<UnaryOp, Args>>...,
		    std::is_nothrow_invocable<
		        assign<>, type&,
		        std::invoke_result_t<BinaryOp, type&, std::invoke_result_t<UnaryOp, Args>>>...>;
	};

	template <typename Init, typename BinaryOp, typename UnaryOp, typename... Args>
	using fold_result_t = typename fold_result<Init, BinaryOp, UnaryOp, Args...>::type;

	template <typename Init, typename BinaryOp, typename UnaryOp, typename... Args>
	bool constexpr fold_result_v = fold_result<Init, BinaryOp, UnaryOp, Args...>::value;


	template <typename Init, typename BinaryOp, typename... Args>
	auto constexpr foldl(BinaryOp&&, Init&&, Args&&...) noexcept(fold_result_v<Init, BinaryOp, id, Args...>)
	    -> fold_result_t<Init, BinaryOp, id, Args...>;

	template <typename Init, typename BinaryOp, typename... Args>
	auto constexpr foldr(BinaryOp&&, Init&&,
	                     Args&&...) noexcept(fold_result_v<Init, flip<BinaryOp>, id, Args...>)
	    -> fold_result_t<Init, flip<BinaryOp>, id, Args...>;

	template <typename Init, typename BinaryOp, typename UnaryOp, typename... Args>
	auto constexpr foldl_map(BinaryOp&&, UnaryOp&&, Init&&,
	                         Args&&...) noexcept(fold_result_v<Init, BinaryOp, UnaryOp, Args...>)
	    -> fold_result_t<Init, BinaryOp, UnaryOp, Args...>;

	template <typename Init, typename BinaryOp, typename UnaryOp, typename... Args>
	auto constexpr foldr_map(BinaryOp&&, UnaryOp&&, Init&&,
	                         Args&&...) noexcept(fold_result_v<Init, flip<BinaryOp>, UnaryOp, Args...>)
	    -> fold_result_t<Init, flip<BinaryOp>, UnaryOp, Args...>;


	// boolean folds
	namespace traits {
		template <typename UnaryPred, typename... Args>
		struct is_nothrow_boolean_fold
		    : std::conjunction<
		          std::is_nothrow_invocable<UnaryPred, Args>...,
		          nxx::traits::is_nothrow_convertible<std::invoke_result_t<UnaryPred, Args>, bool>...> {};

		template <typename UnaryPred, typename... Args>
		bool constexpr is_nothrow_boolean_fold_v = is_nothrow_boolean_fold<UnaryPred, Args...>::value;

	}  // namespace traits

	template <typename UnaryPred, typename... Args>
	bool constexpr all(UnaryPred&&,
	                   Args&&...) noexcept(traits::is_nothrow_boolean_fold_v<UnaryPred, Args...>);

	template <typename UnaryPred, typename... Args>
	bool constexpr any(UnaryPred&&,
	                   Args&&...) noexcept(traits::is_nothrow_boolean_fold_v<UnaryPred, Args...>);

	template <typename UnaryPred, typename... Args>
	bool constexpr none(UnaryPred&&,
	                    Args&&...) noexcept(traits::is_nothrow_boolean_fold_v<UnaryPred, Args...>);


	template <typename, typename...>
	struct bind_front;

	template <typename, typename = void>
	struct not_fn;

}  // namespace nxx::functional

#endif  // ifndef NXX__FUNCTIONAL_FWD_HXX
