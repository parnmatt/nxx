#ifndef NXX__TUPLE_HXX
#define NXX__TUPLE_HXX

#include <cstddef>
#include <functional>
#include <limits>
#include <numeric>
#include <tuple>
#include <type_traits>
#include <utility>

#include <nxx/functional_fwd.hxx>
#include <nxx/tuple_fwd.hxx>
#include <nxx/type_traits.hxx>
#include <nxx/utility_fwd.hxx>

namespace nxx::tuple {

	template <typename... Args>
	auto constexpr make(Args&&... args) noexcept(
	    std::is_nothrow_constructible_v<std::tuple<Args...>, Args...>) -> std::tuple<Args...> {
		return {std::forward<Args>(args)...};
	}

	// tuple from indices from tuple-like

	template <std::size_t... Is, typename Tuple>
	auto constexpr make_from_index_sequence(Tuple&& tuple) noexcept -> from_index_sequence<Tuple, Is...> {
		return {std::get<Is>(std::forward<Tuple>(tuple))...};
	}

	template <std::size_t... Is, typename Tuple>
	auto constexpr make_from_index_sequence(Tuple&& tuple, std::index_sequence<Is...>) noexcept
	    -> from_index_sequence<Tuple, Is...> {
		return {std::forward<Tuple>(tuple)};
	}

	template <std::size_t B, std::size_t E, typename Tuple>
	auto constexpr make_from_index_range(Tuple&& tuple) noexcept -> from_index_range<Tuple, B, E> {
		return {std::forward<Tuple>(tuple)};
	}


	template <std::size_t I, typename Tuple, typename... Tuples>
	auto constexpr make_from_shared_index(Tuple&& tuple, Tuples&&... tuples) noexcept
	    -> from_shared_index<I, Tuple, Tuples...> {
		return {std::get<I>(std::forward<Tuple>(tuple)), std::get<I>(std::forward<Tuples>(tuples))...};
	}

	namespace detail {
		template <std::size_t I, typename TupleOfTuples, std::size_t... Is>
		auto constexpr make_from_shared_index(TupleOfTuples&& tuple, std::index_sequence<Is...>)
		    -> decltype(auto) {
			return tuple::make_from_shared_index<I>(std::get<Is>(std::forward<TupleOfTuples>(tuple))...);
		}

	}  // namespace detail

	template <std::size_t I, typename TupleOfTuples>
	auto constexpr make_from_shared_index(TupleOfTuples&& tuple) noexcept
	    -> from_shared_index<I, TupleOfTuples> {
		return detail::make_from_shared_index<I>(
		    std::forward<TupleOfTuples>(tuple),
		    util::make_index_sequence_from_range<
		        0, std::tuple_size_v<std::remove_reference_t<TupleOfTuples>>>{});
	}

	namespace traits {
		namespace detail {
			template <typename, typename, typename>
			struct is_nothrow_appliable;

			template <typename F, typename Tuple, typename Seq>
			bool constexpr is_nothrow_appliable_v = is_nothrow_appliable<F, Tuple, Seq>::value;

			template <typename F, typename Tuple, std::size_t... Is>
			struct is_nothrow_appliable<F, Tuple, std::index_sequence<Is...>>
			    : std::is_nothrow_invocable<F, traits::get_t<Is, Tuple>...> {};
		}  // namespace detail

		template <typename F, typename Tuple>
		using is_nothrow_appliable = detail::is_nothrow_appliable<
		    F, Tuple,
		    util::make_index_sequence_from_range<0, std::tuple_size_v<std::remove_reference_t<Tuple>>>>;

		template <typename F, typename Tuple>
		bool constexpr is_nothrow_appliable_v = is_nothrow_appliable<F, Tuple>::value;

	}  // namespace traits


	template <typename F>
	class apply {
	public:
		constexpr apply() = default;

		explicit constexpr apply(F&& f) noexcept(std::is_nothrow_move_constructible_v<F>)
		    : m_f{std::forward<F>(f)} {}

		template <typename Tuple>
		auto operator()(Tuple&& tuple) const noexcept(traits::is_nothrow_appliable_v<F, Tuple>)
		    -> decltype(auto) {
			return std::apply(m_f, std::forward<Tuple>(tuple));
		}

	private:
		F m_f;
	};


	// map

	namespace detail {
		template <typename UnaryOp, typename Tuple, std::size_t... Is>
		auto constexpr map(UnaryOp&& op, Tuple&& tuple, std::index_sequence<Is...>) -> decltype(auto) {
			return functional::map(std::forward<UnaryOp>(op), std::get<Is>(std::forward<Tuple>(tuple))...);
		}

	}  // namespace detail

	template <typename UnaryOp, typename Tuple>
	auto constexpr map(UnaryOp&& op, Tuple&& tuple) noexcept(
	    map_result_v<UnaryOp, Tuple, traits::make_index_sequence_from_tuple<Tuple>>)
	    -> map_result_t<UnaryOp, Tuple, traits::make_index_sequence_from_tuple<Tuple>> {

		auto constexpr size = std::tuple_size_v<std::remove_reference_t<Tuple>>;
		using seq_t = util::make_index_sequence_from_range<0, size>;
		return detail::map(std::forward<UnaryOp>(op), std::forward<Tuple>(tuple), seq_t{});
	}

	// folds

	namespace detail {
		template <typename Init, typename BinaryOp, typename UnaryOp, typename Tuple, std::size_t... Is>
		auto constexpr foldl_map(BinaryOp&& b_op, UnaryOp&& u_op, Init&& init, Tuple&& tuple,
		                         std::index_sequence<Is...>) -> decltype(auto) {

			return functional::foldl_map(std::forward<BinaryOp>(b_op), std::forward<UnaryOp>(u_op),
			                             std::forward<Init>(init),
			                             std::get<Is>(std::forward<Tuple>(tuple))...);
		}

	}  // namespace detail


	// foldl : op(op(op(op(i, e_1), e_2), e_3), ... e_n)
	template <typename Init, typename BinaryOp, typename Tuple>
	auto constexpr foldl(BinaryOp&& op, Init&& init, Tuple&& tuple) noexcept(
	    fold_result_v<Init, BinaryOp, functional::id, Tuple, traits::make_index_sequence_from_tuple<Tuple>>)
	    -> fold_result_t<Init, BinaryOp, functional::id, Tuple,
	                     traits::make_index_sequence_from_tuple<Tuple>> {

		return tuple::foldl_map(std::forward<BinaryOp>(op), functional::id{}, std::forward<Init>(init),
		                        std::forward<Tuple>(tuple));
	}

	// foldl : op(op(op(op(e_1, e_2), e_3), e_4), ... e_n)
	template <typename BinaryOp, typename Tuple>
	auto constexpr foldl(BinaryOp&& op, Tuple&& tuple) noexcept(
	    fold_result_v<
	        traits::get_t<0, Tuple>, BinaryOp, functional::id, Tuple,
	        util::make_index_sequence_from_range<1, std::tuple_size_v<std::remove_reference_t<Tuple>>>>)
	    -> fold_result_t<
	        traits::get_t<0, Tuple>, BinaryOp, functional::id, Tuple,
	        util::make_index_sequence_from_range<1, std::tuple_size_v<std::remove_reference_t<Tuple>>>> {

		return tuple::foldl_map(std::forward<BinaryOp>(op), functional::id{}, std::forward<Tuple>(tuple));
	}


	// foldr : op(e_1, op(e_2, op(e_3, ... op(e_n, i))))
	template <typename Init, typename BinaryOp, typename Tuple>
	auto constexpr foldr(BinaryOp&& op, Init&& init, Tuple&& tuple) noexcept(
	    fold_result_v<
	        Init, functional::flip<BinaryOp>, functional::id, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>>, 0>>)
	    -> fold_result_t<
	        Init, functional::flip<BinaryOp>, functional::id, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>>, 0>> {

		return tuple::foldr_map(functional::flip{std::forward<BinaryOp>(op)}, functional::id{},
		                        std::forward<Init>(init), std::forward<Tuple>(tuple));
	}

	// foldr : op(e_1, op(e_2, op(e_3, ... op(e_n-1, e_n))))
	template <typename BinaryOp, typename Tuple>
	auto constexpr foldr(BinaryOp&& op, Tuple&& tuple) noexcept(
	    fold_result_v<
	        nxx::traits::remove_cvref_t<
	            traits::get_t<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, Tuple>>,
	        functional::flip<BinaryOp>, functional::id, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, 0>>)
	    -> fold_result_t<
	        nxx::traits::remove_cvref_t<
	            traits::get_t<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, Tuple>>,
	        functional::flip<BinaryOp>, functional::id, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, 0>> {

		return tuple::foldr_map(functional::flip{std::forward<BinaryOp>(op)}, functional::id{},
		                        std::forward<Tuple>(tuple));
	}


	// foldl_map : b_op(b_op(b_op(b_op(u_op(i), u_op(e_1)), u_op(e_2)), u_op(e_3)), ... u_op(e_n))
	template <typename Init, typename BinaryOp, typename UnaryOp, typename Tuple>
	auto constexpr foldl_map(BinaryOp&& b_op, UnaryOp&& u_op, Init&& init, Tuple&& tuple) noexcept(
	    fold_result_v<Init, BinaryOp, UnaryOp, Tuple, traits::make_index_sequence_from_tuple<Tuple>>)
	    -> fold_result_t<Init, BinaryOp, UnaryOp, Tuple, traits::make_index_sequence_from_tuple<Tuple>> {

		using seq_t = traits::make_index_sequence_from_tuple<Tuple>;
		return detail::foldl_map(std::forward<BinaryOp>(b_op), std::forward<UnaryOp>(u_op),
		                         std::forward<Init>(init), std::forward<Tuple>(tuple), seq_t{});
	}

	// foldl_map : b_op(b_op(b_op(b_op(u_op(e_1), u_op(e_2)), u_op(e_3)), u_op(e_4)), ... u_op(e_n))
	template <typename BinaryOp, typename UnaryOp, typename Tuple>
	auto constexpr foldl_map(BinaryOp&& b_op, UnaryOp&& u_op, Tuple&& tuple) noexcept(
	    fold_result_v<
	        traits::get_t<0, Tuple>, BinaryOp, UnaryOp, Tuple,
	        util::make_index_sequence_from_range<1, std::tuple_size_v<std::remove_reference_t<Tuple>>>>)
	    -> fold_result_t<
	        traits::get_t<0, Tuple>, BinaryOp, UnaryOp, Tuple,
	        util::make_index_sequence_from_range<1, std::tuple_size_v<std::remove_reference_t<Tuple>>>> {

		auto constexpr size = std::tuple_size_v<std::remove_reference_t<Tuple>>;
		using seq_t = util::make_index_sequence_from_range<1, size>;
		using init_t = traits::get_t<0, Tuple>;
		init_t init{std::get<0>(std::forward<Tuple>(tuple))};
		return detail::foldl_map(std::forward<BinaryOp>(b_op), std::forward<UnaryOp>(u_op),
		                         std::forward<init_t>(init), std::forward<Tuple>(tuple), seq_t{});
	}


	// foldr_map : b_op(u_op(e_1), b_op(u_op(e_2), b_op(u_op(e_3), ... b_op(u_op(e_n), u_op(i)))))
	template <typename Init, typename BinaryOp, typename UnaryOp, typename Tuple>
	auto constexpr foldr_map(BinaryOp&& b_op, UnaryOp&& u_op, Init&& init, Tuple&& tuple) noexcept(
	    fold_result_v<
	        Init, functional::flip<BinaryOp>, UnaryOp, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>>, 0>>)
	    -> fold_result_t<
	        Init, functional::flip<BinaryOp>, UnaryOp, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>>, 0>> {

		auto constexpr size = std::tuple_size_v<std::remove_reference_t<Tuple>>;
		using seq_t = util::make_index_sequence_from_range<size, 0>;
		return detail::foldl_map(functional::flip{std::forward<BinaryOp>(b_op)}, std::forward<UnaryOp>(u_op),
		                         std::forward<Init>(init), std::forward<Tuple>(tuple), seq_t{});
	}

	// foldr_map : b_op(u_op(e_1), b_op(u_op(e_2), b_op(u_op(e_3), ... b_op(u_op(e_n-1), u_op(e_n)))))
	template <typename BinaryOp, typename UnaryOp, typename Tuple>
	auto constexpr foldr_map(BinaryOp&& b_op, UnaryOp&& u_op, Tuple&& tuple) noexcept(
	    fold_result_v<
	        traits::get_t<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, Tuple>,
	        functional::flip<BinaryOp>, UnaryOp, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, 0>>)
	    -> fold_result_t<
	        traits::get_t<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, Tuple>,
	        functional::flip<BinaryOp>, UnaryOp, Tuple,
	        util::make_index_sequence_from_range<std::tuple_size_v<std::remove_reference_t<Tuple>> - 1, 0>> {

		auto constexpr size = std::tuple_size_v<std::remove_reference_t<Tuple>>;
		using seq_t = util::make_index_sequence_from_range<size - 1, 0>;
		using init_t = traits::get_t<size - 1, Tuple>;
		init_t init{std::get<size - 1>(std::forward<Tuple>(tuple))};
		return detail::foldl_map(functional::flip{std::forward<BinaryOp>(b_op)}, std::forward<UnaryOp>(u_op),
		                         std::forward<init_t>(init), std::forward<Tuple>(tuple), seq_t{});
	}

	// boolean folds

	template <typename Tuple>
	bool constexpr all(Tuple&& tuple) noexcept(
	    traits::is_nothrow_boolean_fold_v<functional::id, Tuple,
	                                      traits::make_index_sequence_from_tuple<Tuple>>) {

		return tuple::all(functional::id{}, std::forward<Tuple>(tuple));
	}

	template <typename UnaryPred, typename Tuple>
	bool constexpr all(UnaryPred&& pred, Tuple&& tuple) noexcept(
	    traits::is_nothrow_boolean_fold_v<UnaryPred, Tuple, traits::make_index_sequence_from_tuple<Tuple>>) {

		return tuple::foldl_map(std::logical_and<bool>{}, std::forward<UnaryPred>(pred),
		                        std::forward<Tuple>(tuple));
	}

	template <typename Tuple>
	bool constexpr any(Tuple&& tuple) noexcept(
	    traits::is_nothrow_boolean_fold_v<functional::id, Tuple,
	                                      traits::make_index_sequence_from_tuple<Tuple>>) {

		return tuple::any(functional::id{}, std::forward<Tuple>(tuple));
	}

	template <typename UnaryPred, typename Tuple>
	bool constexpr any(UnaryPred&& pred, Tuple&& tuple) noexcept(
	    traits::is_nothrow_boolean_fold_v<UnaryPred, Tuple, traits::make_index_sequence_from_tuple<Tuple>>) {

		return tuple::foldl_map(std::logical_or<bool>{}, std::forward<UnaryPred>(pred),
		                        std::forward<Tuple>(tuple));
	}

	template <typename Tuple>
	bool constexpr none(Tuple&& tuple) noexcept(
	    traits::is_nothrow_boolean_fold_v<functional::id, Tuple,
	                                      traits::make_index_sequence_from_tuple<Tuple>>) {

		return tuple::none(functional::id{}, std::forward<Tuple>(tuple));
	}

	template <typename UnaryPred, typename Tuple>
	bool constexpr none(UnaryPred&& pred, Tuple&& tuple) noexcept(
	    traits::is_nothrow_boolean_fold_v<UnaryPred, Tuple, traits::make_index_sequence_from_tuple<Tuple>>) {

		return tuple::all(functional::not_fn{std::forward<UnaryPred>(pred)}, std::forward<Tuple>(tuple));
	}


	// refactor / deprecate the previous functions

	template <typename UnaryOp, typename Tuple>
	auto constexpr transform(UnaryOp&& op, Tuple&& tuple) noexcept(
	    map_result_v<UnaryOp, Tuple, traits::make_index_sequence_from_tuple<Tuple>>) -> decltype(auto) {

		return tuple::map(std::forward<UnaryOp>(op), std::forward<Tuple>(tuple));
	};

	template <typename UnaryOp, typename Tuple>
	void constexpr for_each(UnaryOp&& op, Tuple&& tuple) noexcept(
	    map_result_v<UnaryOp, Tuple, traits::make_index_sequence_from_tuple<Tuple>>) {

		return tuple::map(std::forward<UnaryOp>(op), std::forward<Tuple>(tuple));
	}

	template <typename Tuple, typename BinaryOp>
	auto constexpr reduce(Tuple&& tuple, BinaryOp&& op) noexcept(
	    fold_result_v<
	        traits::get_t<0, Tuple>, BinaryOp, functional::id, Tuple,
	        util::make_index_sequence_from_range<1, std::tuple_size_v<std::remove_reference_t<Tuple>>>>)
	    -> fold_result_t<
	        traits::get_t<0, Tuple>, BinaryOp, functional::id, Tuple,
	        util::make_index_sequence_from_range<1, std::tuple_size_v<std::remove_reference_t<Tuple>>>> {

		return tuple::foldl(std::forward<BinaryOp>(op), std::forward<Tuple>(tuple));
	}

	template <typename Tuple, typename BinaryOp, typename UnaryOp>
	auto constexpr transform_reduce(Tuple&& tuple, BinaryOp&& b_op, UnaryOp&& u_op) noexcept(
	    fold_result_v<
	        traits::get_t<0, Tuple>, BinaryOp, UnaryOp, Tuple,
	        util::make_index_sequence_from_range<1, std::tuple_size_v<std::remove_reference_t<Tuple>>>>)
	    -> fold_result_t<
	        traits::get_t<0, Tuple>, BinaryOp, UnaryOp, Tuple,
	        util::make_index_sequence_from_range<1, std::tuple_size_v<std::remove_reference_t<Tuple>>>> {

		return tuple::foldl_map(std::forward<BinaryOp>(b_op), std::forward<UnaryOp>(u_op),
		                        std::forward<Tuple>(tuple));
	}

	template <typename Tuple, typename UnaryPred = functional::id>
	bool constexpr all_of(Tuple&& tuple, UnaryPred&& pred = {}) noexcept(
	    traits::is_nothrow_boolean_fold_v<
	        functional::id, Tuple,
	        util::make_index_sequence_from_range<0, std::tuple_size_v<std::remove_reference_t<Tuple>>>>) {

		return tuple::all(std::forward<UnaryPred>(pred), std::forward<Tuple>(tuple));
	}

	template <typename Tuple, typename UnaryPred = functional::id>
	bool constexpr any_of(Tuple&& tuple, UnaryPred&& pred = {}) noexcept(
	    traits::is_nothrow_boolean_fold_v<
	        functional::id, Tuple,
	        util::make_index_sequence_from_range<0, std::tuple_size_v<std::remove_reference_t<Tuple>>>>) {

		return tuple::any(std::forward<UnaryPred>(pred), std::forward<Tuple>(tuple));
	}

	template <typename Tuple, typename UnaryPred = functional::id>
	bool constexpr none_of(Tuple&& tuple, UnaryPred&& pred = {}) noexcept(
	    traits::is_nothrow_boolean_fold_v<
	        functional::id, Tuple,
	        util::make_index_sequence_from_range<0, std::tuple_size_v<std::remove_reference_t<Tuple>>>>) {

		return tuple::none(std::forward<UnaryPred>(pred), std::forward<Tuple>(tuple));
	}


	// zips

	namespace detail {
		template <typename F, typename TupleOfTuples, std::size_t... Is>
		auto constexpr zip_with(F&& f, TupleOfTuples&& tuple, std::index_sequence<Is...>) -> decltype(auto) {
			return std::tuple{
			    functional::invoke(apply{std::forward<F>(f)},
			                       tuple::make_from_shared_index<Is>(std::forward<TupleOfTuples>(tuple)))...};
		}

	}  // namespace detail

	template <typename F, typename Tuple, typename... Tuples>
	auto constexpr zip_with(F&& f, Tuple&& tuple, Tuples&&... tuples) {

		using seq_t = util::make_index_sequence_from_range<
		    0, std::min({std::tuple_size_v<std::remove_reference_t<Tuple>>,
		                 std::tuple_size_v<std::remove_reference_t<Tuples>>...})>;

		return detail::zip_with(
		    std::forward<F>(f),
		    std::forward_as_tuple(std::forward<Tuple>(tuple), std::forward<Tuples>(tuples)...), seq_t{});
	}


	namespace detail {
		template <typename F, typename TupleOfTuples, std::size_t... Is>
		auto constexpr zip_with_tuple_of_tuples(F&& f, TupleOfTuples&& tuple, std::index_sequence<Is...>) {
			return tuple::zip_with(std::forward<F>(f), std::get<Is>(std::forward<TupleOfTuples>(tuple))...);
		}

	}  // namespace detail

	template <typename F, typename TupleOfTuples>
	auto constexpr zip_with(F&& f, TupleOfTuples&& tuple) {
		return detail::zip_with_tuple_of_tuples(std::forward<F>(f), std::forward<TupleOfTuples>(tuple),
		                                        traits::make_index_sequence_from_tuple<TupleOfTuples>{});
	}

	template <typename Tuple, typename... Tuples>
	auto constexpr zip(Tuple&& tuple, Tuples&&... tuples) {
		return tuple::zip_with([](auto&&... args) { return tuple::make(args...); },
		                       std::forward<Tuple>(tuple), std::forward<Tuples>(tuples)...);
	}

	namespace detail {
		template <typename TupleOfTuples, std::size_t... Is>
		auto constexpr zip_tuple_of_tuples(TupleOfTuples&& tuple, std::index_sequence<Is...>) {
			return tuple::zip(std::get<Is>(std::forward<TupleOfTuples>(tuple))...);
		}

	}  // namespace detail

	template <typename TupleOfTuples>
	auto constexpr zip(TupleOfTuples&& tuple) {
		return detail::zip_tuple_of_tuples(std::forward<TupleOfTuples>(tuple),
		                                   traits::make_index_sequence_from_tuple<TupleOfTuples>{});
	}


}  // namespace nxx::tuple

#include <nxx/functional.hxx>
#include <nxx/utility.hxx>

#endif  // ifndef NXX__TUPLE_HXX
