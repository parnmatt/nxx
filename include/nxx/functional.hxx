#ifndef NXX__FUNCTIONAL_HXX
#define NXX__FUNCTIONAL_HXX

#include <functional>
#include <tuple>
#include <type_traits>
#include <utility>

#include <nxx/functional_fwd.hxx>
#include <nxx/tuple_fwd.hxx>
#include <nxx/type_traits.hxx>

namespace nxx::functional {
	namespace detail {

		template <typename F, typename... Args>
		auto constexpr invoke(F&& f, Args&&... args) noexcept(std::is_nothrow_invocable_v<F, Args...>)
		    -> decltype(auto) {
			return std::forward<F>(f)(std::forward<Args>(args)...);
		}

		template <typename R, typename T, typename S, typename... Args>
		auto constexpr invoke(R T::*f, S&& s,
		                      Args&&... args) noexcept(std::is_nothrow_invocable_v<R T::*, Args...>)
		    -> decltype(auto) {
			using arg_t = std::decay_t<S>;
			using func_t = decltype(f);

			if constexpr (std::is_member_function_pointer_v<func_t>) {
				if constexpr (std::is_base_of_v<T, arg_t>)
					return (std::forward<S>(s).*f)(std::forward<Args>(args)...);

				else if constexpr (nxx::traits::is_specialization_of_v<std::reference_wrapper, arg_t>)
					return (std::forward<S>(s).get().*f)(std::forward<Args>(args)...);

				else
					return ((*std::forward<S>(s)).*f)(std::forward<Args>(args)...);

			} else {
				static_assert(std::is_member_object_pointer_v<func_t>);
				static_assert(sizeof...(Args) == 0);

				if constexpr (std::is_base_of_v<T, arg_t>)
					return std::forward<S>(s).*f;

				else if constexpr (nxx::traits::is_specialization_of_v<std::reference_wrapper, arg_t>)
					return std::forward<S>(s).get().*f;

				else
					return (*std::forward<S>(s)).*f;
			}
		}

	}  // namespace detail

	template <typename F, typename... Args>
	auto constexpr invoke(F&& f, Args&&... args) noexcept(std::is_nothrow_invocable_v<F, Args...>)
	    -> std::invoke_result_t<F, Args...> {
		return detail::invoke(std::forward<F>(f), std::forward<Args>(args)...);
	}


	// complement to tuple::apply
	template <typename... Args>
	class wrap_args {
	private:
		using tuple_t = std::tuple<Args...>;

	public:
		explicit constexpr wrap_args(Args&&... args) noexcept(
		    std::is_nothrow_constructible_v<tuple_t, Args...>)
		    : m_args{std::forward_as_tuple(std::forward<Args>(args)...)} {}

		template <typename F>
		auto constexpr operator()(F&& f) const noexcept(std::is_nothrow_invocable_v<F, Args...>)
		    -> decltype(auto) {
			return std::apply(std::forward<F>(f), m_args);
		}

	private:
		tuple_t m_args;
	};

	template <typename... Args>
	wrap_args(Args&&...)->wrap_args<Args...>;


	struct id {
		using is_transparent = void;

		template <typename T>
		auto constexpr operator()(T&& t) const noexcept -> decltype(auto) {
			return std::forward<T>(t);
		}
	};


	template <typename T>
	struct assign {
		auto constexpr operator()(T& lhs, T&& rhs) const noexcept(std::is_nothrow_move_assignable_v<T>)
		    -> T& {
			return lhs = rhs;
		}
	};

	template <>
	struct assign<void> {
		template <typename Lhs, typename Rhs>
		auto constexpr operator()(Lhs& lhs, Rhs&& rhs) const
		    noexcept(std::is_nothrow_assignable_v<Lhs&, Rhs&&>) -> decltype(auto) {
			return lhs = rhs;
		}
	};


	template <typename... Fs>
	class compose : std::enable_if_t<(sizeof...(Fs) > 1), nxx::empty_base<compose<Fs...>>> {
	private:
		using tuple_t = std::tuple<Fs...>;

		template <std::size_t I, typename... Args>
		bool constexpr static nothrow() noexcept {
			using func_t = tuple::traits::get_t<I, tuple_t const>;
			using result_t = std::invoke_result_t<func_t, Args...>;
			bool constexpr result_noexcept = std::is_nothrow_invocable_v<func_t, Args...>;

			if constexpr (I == 0)
				return result_noexcept;
			else
				return result_noexcept && nothrow<I - 1, result_t>();
		}

	public:
		explicit constexpr compose(Fs&&... fs) noexcept(std::is_nothrow_constructible_v<tuple_t, Fs...>)
		    : m_fs{std::forward<Fs>(fs)...} {}

		template <typename... Args>
		auto constexpr operator()(Args&&... args) const
		    noexcept(compose::nothrow<sizeof...(Fs) - 1, Args...>()) -> decltype(auto) {

			return invoke<sizeof...(Fs) - 2>(
			    functional::invoke(std::get<sizeof...(Fs) - 1>(m_fs), std::forward<Args>(args)...));
		}


	private:
		template <std::size_t I, typename T>
		auto constexpr invoke(T&& t) const -> decltype(auto) {
			decltype(auto) ft = functional::invoke(std::get<I>(m_fs), std::forward<T>(t));
			if constexpr (I == 0)
				return ft;
			else
				return invoke<I - 1>(std::forward<decltype(ft)>(ft));
		}

		tuple_t m_fs;
	};

	template <typename... Fs>
	compose(Fs&&...)->compose<Fs...>;


	template <typename F, typename... BoundArgs>
	class partial {
	protected:
		using tuple_t = std::tuple<BoundArgs...>;

	public:
		explicit constexpr partial(F&& f, BoundArgs&&... args) noexcept(
		    std::conjunction_v<std::is_nothrow_move_constructible<F>,
		                       std::is_nothrow_constructible<tuple_t, BoundArgs...>>)
		    : m_f{std::forward<F>(f)}
		    , m_args{std::forward<BoundArgs>(args)...} {}

		template <typename... Args>
		auto constexpr operator()(Args&&... args) const
		    noexcept(std::is_nothrow_invocable_v<F, BoundArgs..., Args...>) -> decltype(auto) {

			return invoke(tuple::traits::make_index_sequence_from_tuple<tuple_t>{},
			              std::forward<Args>(args)...);
		}

	private:
		template <std::size_t... Is, typename... Args>
		auto constexpr invoke(std::index_sequence<Is...>, Args&&... args) const
		    noexcept(std::is_nothrow_invocable_v<F, BoundArgs..., Args...>) -> decltype(auto) {

			return functional::invoke(m_f, std::get<Is>(m_args)..., std::forward<Args>(args)...);
		}

	protected:
		F m_f;
		tuple_t m_args;
	};

	template <typename F, typename... BoundArgs>
	partial(F&&, BoundArgs&&...)->partial<F, BoundArgs...>;


	template <typename F, typename... BoundArgs>
	class curry : public partial<F, BoundArgs...> {
	private:
		using partial_t = partial<F, BoundArgs...>;

	public:
		using partial_t::partial;

		template <typename... Args>
		auto constexpr operator()(Args&&... args) const noexcept(
		    std::disjunction_v<
		        std::is_nothrow_invocable<decltype(&partial_t::template operator()<Args...>), curry, Args...>,
		        std::conjunction_v<std::is_invocable<F, BoundArgs..., Args...>,
		                           std::is_nothrow_constructible<functional::curry<F, BoundArgs..., Args...>,
		                                                         F, BoundArgs..., Args...>>>)
		    -> decltype(auto) {

			if constexpr (std::is_invocable_v<F, BoundArgs..., Args...>)
				return partial_t::operator()(std::forward<Args>(args)...);
			else
				return curry_args(
				    tuple::traits::make_index_sequence_from_tuple<typename partial_t::tuple_t>{},
				    std::forward<Args>(args)...);
		}

	private:
		template <std::size_t... Is, typename... Args>
		auto constexpr curry_args(std::index_sequence<Is...>, Args&&... args) const
		    noexcept(std::is_nothrow_constructible_v<functional::curry<F, BoundArgs..., Args...>, F,
		                                             BoundArgs..., Args...>) -> decltype(auto) {

			return functional::curry{this->m_f, std::get<Is>(this->m_args)..., std::forward<Args>(args)...};
		}
	};

	template <typename F, typename... BoundArgs>
	curry(F&&, BoundArgs&&...)->curry<F, BoundArgs...>;


	template <typename F>
	class flip {
	public:
		explicit constexpr flip(F&& f) noexcept(std::is_nothrow_move_constructible_v<F>)
		    : m_f{std::forward<F>(f)} {}

		template <typename First, typename Second, typename... Rest>
		auto constexpr operator()(First&& first, Second&& second, Rest&&... rest) const
		    noexcept(std::is_nothrow_invocable_v<F, Second, First, Rest...>) -> decltype(auto) {

			return functional::invoke(m_f, std::forward<Second>(second), std::forward<First>(first),
			                          std::forward<Rest>(rest)...);
		}

	private:
		F m_f;
	};

	template <typename F>
	flip(F &&)->flip<F>;


	template <typename F>
	class discard {
	public:
		explicit constexpr discard(F&& f) noexcept(std::is_nothrow_move_constructible_v<F>)
		    : m_f{std::forward<F>(f)} {}

		template <typename... Args>
		void constexpr operator()(Args&&... args) const noexcept(std::is_nothrow_invocable_v<F, Args...>) {
			static_cast<void>(functional::invoke(m_f, std::forward<Args>(args)...));
		}

	private:
		F m_f;
	};

	template <typename F>
	discard(F &&)->discard<F>;


	// map

	template <typename UnaryOp, typename... Args>
	auto constexpr map(UnaryOp&& op, Args&&... args) noexcept(map_result_v<UnaryOp, Args...>)
	    -> map_result_t<UnaryOp, Args...> {

		using return_t = map_result_t<UnaryOp, Args...>;
		if constexpr (std::is_void_v<return_t>)
			(functional::invoke(discard{std::forward<UnaryOp>(op)}, std::forward<Args>(args)), ...);
		else
			return return_t{functional::invoke(std::forward<UnaryOp>(op), std::forward<Args>(args))...};
	}


	// folds

	// foldl : op(op(op(op(i, e_1), e_2), e_3), ... e_n)
	template <typename Init, typename BinaryOp, typename... Args>
	auto constexpr foldl(BinaryOp&& op, Init&& init,
	                     Args&&... args) noexcept(fold_result_v<Init, BinaryOp, id, Args...>)
	    -> fold_result_t<Init, BinaryOp, id, Args...> {
		return functional::foldl_map(std::forward<BinaryOp>(op), id{}, std::forward<Init>(init),
		                             std::forward<Args>(args)...);
	}

	// foldr : op(e_1, op(e_2, op(e_3, ... op(e_n, i))))
	template <typename Init, typename BinaryOp, typename... Args>
	auto constexpr foldr(BinaryOp&& op, Init&& init,
	                     Args&&... args) noexcept(fold_result_v<Init, flip<BinaryOp>, id, Args...>)
	    -> fold_result_t<Init, flip<BinaryOp>, id, Args...> {
		return tuple::foldr(std::forward<BinaryOp>(op), std::forward<Init>(init),
		                    std::forward_as_tuple(std::forward<Args>(args)...));
	}


	// foldl_map : b_op(b_op(b_op(b_op(u_op(i), u_op(e_1)), u_op(e_2)), u_op(e_3)), ... u_op(e_n))
	template <typename Init, typename BinaryOp, typename UnaryOp, typename... Args>
	auto constexpr foldl_map(BinaryOp&& b_op, UnaryOp&& u_op, Init&& init,
	                         Args&&... args) noexcept(fold_result_v<Init, BinaryOp, UnaryOp, Args...>)
	    -> fold_result_t<Init, BinaryOp, UnaryOp, Args...> {

		auto b_init = functional::invoke(std::forward<UnaryOp>(u_op), std::forward<Init>(init));
		auto unary_accumulator
		    = discard{compose{partial{assign{}, b_init}, partial{std::forward<BinaryOp>(b_op), b_init},
		                      std::forward<UnaryOp>(u_op)}};
		functional::map(unary_accumulator, std::forward<Args>(args)...);
		return b_init;
	}


	// foldr_map : b_op(u_op(e_1), b_op(u_op(e_2), b_op(u_op(e_3), ... b_op(u_op(e_n), u_op(i)))))
	template <typename Init, typename BinaryOp, typename UnaryOp, typename... Args>
	auto constexpr foldr_map(BinaryOp&& b_op, UnaryOp&& u_op, Init&& init,
	                         Args&&... args) noexcept(fold_result_v<Init, flip<BinaryOp>, UnaryOp, Args...>)
	    -> fold_result_t<Init, flip<BinaryOp>, UnaryOp, Args...> {

		return tuple::foldr_map(std::forward<BinaryOp>(b_op), std::forward<UnaryOp>(u_op),
		                        std::forward<Init>(init), std::forward_as_tuple(std::forward<Args>(args)...));
	}


	template <typename UnaryPred, typename... Args>
	bool constexpr all(UnaryPred&& pred,
	                   Args&&... args) noexcept(traits::is_nothrow_boolean_fold_v<UnaryPred, Args...>) {
		return functional::foldl_map(std::logical_and<bool>{}, std::forward<UnaryPred>(pred),
		                             std::forward<Args>(args)...);
	}

	template <typename UnaryPred, typename... Args>
	bool constexpr any(UnaryPred&& pred,
	                   Args&&... args) noexcept(traits::is_nothrow_boolean_fold_v<UnaryPred, Args...>) {
		return functional::foldl_map(std::logical_or<bool>{}, std::forward<UnaryPred>(pred),
		                             std::forward<Args>(args)...);
	}

	template <typename UnaryPred, typename... Args>
	bool constexpr none(UnaryPred&& pred,
	                    Args&&... args) noexcept(traits::is_nothrow_boolean_fold_v<UnaryPred, Args...>) {
		return functional::all(not_fn{std::forward<UnaryPred>(pred)}, std::forward<Args>(args)...);
	}


	// No CTAD for aliases (yet)

	template <typename F, typename... BoundArgs>
	struct bind_front : partial<F, BoundArgs...> {
		using partial<F, BoundArgs...>::partial;
	};

	template <typename F, typename... BoundArgs>
	bind_front(F&&, BoundArgs&&...)->bind_front<F, BoundArgs...>;


	template <typename F, typename T>
	class not_fn : public compose<std::logical_not<T>, F> {
	private:
		using not_t = std::logical_not<T>;
		using compose_t = compose<not_t, F>;

	public:
		explicit constexpr not_fn(F&& f) noexcept(std::is_nothrow_constructible_v<compose_t, not_t, F>)
		    : compose_t{{}, std::forward<F>(f)} {};
	};

	template <typename F>
	not_fn(F &&)->not_fn<F>;

}  // namespace nxx::functional

#include <nxx/tuple.hxx>

#endif  // ifndef NXX__FUNCTIONAL_HXX
