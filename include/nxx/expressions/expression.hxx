#ifndef NXX__EXPRESSION_EXPRESSION_HXX
#define NXX__EXPRESSION_EXPRESSION_HXX
#ifndef NXX__EXPRESSION_HXX
#	error "please only include nxx/expression.hxx; this is an implementation header"
#else  // ifndef NXX__EXPRESSION_HXX

namespace nxx::expressions {

	template <typename F, typename... Ts>
	class expression {
	public:
		explicit constexpr expression(F&& f, Ts&&... ts)
		    : m_f{std::forward<F>(f)}
		    , m_ts{std::forward_as_tuple(ts...)} {}

		explicit constexpr expression(Ts&&... ts)
		    : expression(F{}, std::forward<Ts>(ts)...) {}

	protected:
		F m_f;
		std::tuple<Ts...> m_ts;
	};


	// arithmetic expressions

	// plus

	template <template <typename...> typename E, typename T, typename Lhs, typename Rhs, typename... Args>
	auto constexpr make_plus(Lhs&& lhs, Rhs&& rhs) {
		return E<plus<Lhs, Rhs, T>, Args...>{std::forward<Lhs>(lhs), std::forward<Rhs>(rhs)};
	}

	template <template <typename...> typename E, typename Lhs, typename Rhs, typename... Args>
	auto constexpr make_plus(Lhs&& lhs, Rhs&& rhs) {
		return make_plus<E, void, Lhs, Rhs, Args...>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs));
	}


	// minus

	template <template <typename...> typename E, typename T, typename Lhs, typename Rhs, typename... Args>
	auto constexpr make_minus(Lhs&& lhs, Rhs&& rhs) {
		return E<minus<Lhs, Rhs, T>, Args...>{std::forward<Lhs>(lhs), std::forward<Rhs>(rhs)};
	}

	template <template <typename...> typename E, typename Lhs, typename Rhs, typename... Args>
	auto constexpr make_minus(Lhs&& lhs, Rhs&& rhs) {
		return make_minus<E, void, Lhs, Rhs, Args...>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs));
	}


	// multiplies

	template <template <typename...> typename E, typename T, typename Lhs, typename Rhs, typename... Args>
	auto constexpr make_multiplies(Lhs&& lhs, Rhs&& rhs) {
		return E<multiplies<Lhs, Rhs, T>, Args...>{std::forward<Lhs>(lhs), std::forward<Rhs>(rhs)};
	}

	template <template <typename...> typename E, typename Lhs, typename Rhs, typename... Args>
	auto constexpr make_multiplies(Lhs&& lhs, Rhs&& rhs) {
		return make_multiplies<E, void, Lhs, Rhs, Args...>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs));
	}


	// divides

	template <template <typename...> typename E, typename T, typename Lhs, typename Rhs, typename... Args>
	auto constexpr make_divides(Lhs&& lhs, Rhs&& rhs) {
		return E<divides<Lhs, Rhs, T>, Args...>{std::forward<Lhs>(lhs), std::forward<Rhs>(rhs)};
	}

	template <template <typename...> typename E, typename Lhs, typename Rhs, typename... Args>
	auto constexpr make_divides(Lhs&& lhs, Rhs&& rhs) {
		return make_divides<E, void, Lhs, Rhs, Args...>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs));
	}


	// modulus

	template <template <typename...> typename E, typename T, typename Lhs, typename Rhs, typename... Args>
	auto constexpr make_modulus(Lhs&& lhs, Rhs&& rhs) {
		return E<modulus<Lhs, Rhs, T>, Args...>{std::forward<Lhs>(lhs), std::forward<Rhs>(rhs)};
	}

	template <template <typename...> typename E, typename Lhs, typename Rhs, typename... Args>
	auto constexpr make_modulus(Lhs&& lhs, Rhs&& rhs) {
		return make_modulus<E, void, Lhs, Rhs, Args...>(std::forward<Lhs>(lhs), std::forward<Rhs>(rhs));
	}


	// modulus

	template <template <typename...> typename E, typename T, typename V, typename... Args>
	auto constexpr make_negate(V&& v) {
		return E<negate<V, T>, Args...>{std::forward<V>(v)};
	}

	template <template <typename...> typename E, typename V, typename... Args>
	auto constexpr make_negate(V&& v) {
		return make_negate<E, void, V, Args...>(std::forward<V>(v));
	}

}  // namespace nxx::expressions

#endif  // ifndef NXX__EXPRESSION_HXX
#endif  // ifndef NXX__EXPRESSION_EXPRESSION_HXX
