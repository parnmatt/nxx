#ifndef NXX__EXPRESSION_TYPE_TRAITS_HXX
#define NXX__EXPRESSION_TYPE_TRAITS_HXX
#ifndef NXX__EXPRESSION_HXX
#	error "please only include nxx/expression.hxx; this is an implementation header"
#else  // ifndef NXX__EXPRESSION_HXX

namespace nxx::expressions::traits {

	template <typename T>
	using has_order_t = decltype(T::order);

	template <typename T>
	using has_order = nxx::traits::is_detected_convertible<type::tensor_order, has_order_t, T>;

	template <typename T>
	bool constexpr has_order_v = has_order<T>::value;


	template <typename T, typename = void>
	struct is_tensor : std::false_type {};

	template <typename T>
	bool constexpr is_tensor_v = is_tensor<T>::value;

	template <typename T>
	struct is_tensor<T, std::enable_if_t<has_order_v<T>>> : std::is_base_of<type::tensor<T::order, T>, T> {};


	template <typename T>
	struct tensor {
		using size_type = nxx::traits::detected_t<nxx::traits::has_size_type_t, T>;
		using reference = nxx::traits::detected_t<nxx::traits::has_reference_t, T>;
		using const_reference = nxx::traits::detected_t<nxx::traits::has_const_reference_t, T>;
		using iterator = nxx::traits::detected_t<nxx::traits::has_iterator_t, T>;
		using const_iterator = nxx::traits::detected_t<nxx::traits::has_const_iterator_t, T>;
	};

}  // namespace nxx::expressions::traits

#endif  // ifndef NXX__EXPRESSION_HXX
#endif  // ifndef NXX__EXPRESSION_TYPE_TRAITS_HXX
