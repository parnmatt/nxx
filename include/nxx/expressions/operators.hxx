#ifndef NXX__EXPRESSION_OPERATORS_HXX
#define NXX__EXPRESSION_OPERATORS_HXX
#ifndef NXX__EXPRESSION_HXX
#	error "please only include nxx/expression.hxx; this is an implementation header"
#else  // ifndef NXX__EXPRESSION_HXX

namespace nxx::expressions {

	// {scalar}

	template <typename V>
	auto constexpr operator-(type::scalar<V> const& v) -> decltype(auto) {
		return make_negate<element_wise::scalar_expression>(v);
	}


	// {scalar, scalar}
	template <typename Lhs, typename Rhs>
	auto constexpr operator+(type::scalar<Lhs> const& lhs, type::scalar<Rhs> const& rhs) -> decltype(auto) {
		return make_plus<element_wise::scalar_expression>(lhs, rhs);
	}

	template <typename Lhs, typename Rhs>
	auto constexpr operator-(type::scalar<Lhs> const& lhs, type::scalar<Rhs> const& rhs) -> decltype(auto) {
		return make_minus<element_wise::scalar_expression>(lhs, rhs);
	}

	template <typename Lhs, typename Rhs>
	auto constexpr operator*(type::scalar<Lhs> const& lhs, type::scalar<Rhs> const& rhs) -> decltype(auto) {
		return make_multiplies<element_wise::scalar_expression>(lhs, rhs);
	}

	template <typename Lhs, typename Rhs>
	auto constexpr operator/(type::scalar<Lhs> const& lhs, type::scalar<Rhs> const& rhs) -> decltype(auto) {
		return make_divides<element_wise::scalar_expression>(lhs, rhs);
	}

	template <typename Lhs, typename Rhs>
	auto constexpr operator%(type::scalar<Lhs> const& lhs, type::scalar<Rhs> const& rhs) -> decltype(auto) {
		return make_modulus<element_wise::scalar_expression>(lhs, rhs);
	}


	// {vector}

	template <typename V>
	auto constexpr operator-(type::vector<V> const& v) -> decltype(auto) {
		return make_negate<element_wise::vector_expression>(v);
	}


	// {scalar, vector}

	template <typename Lhs, typename Rhs>
	auto constexpr operator*(type::scalar<Lhs> const& lhs, type::vector<Rhs> const& rhs) -> decltype(auto) {
		auto const lhs_as_vector = scalar_as_vector{lhs};
		return make_multiplies<element_wise::vector_expression>(std::move(lhs_as_vector), rhs);
	}

	template <typename Lhs, typename Rhs>
	auto constexpr operator*(type::vector<Lhs> const& lhs, type::scalar<Rhs> const& rhs) -> decltype(auto) {
		return rhs * lhs;
	}

	template <typename Lhs, typename Rhs>
	auto constexpr operator/(type::vector<Lhs> const& lhs, type::scalar<Rhs> const& rhs) -> decltype(auto) {
		auto const rhs_as_vector = scalar_as_vector{rhs};
		return make_divides<element_wise::vector_expression>(lhs, std::move(rhs_as_vector));
	}


	// {vector, vector}

	template <typename Lhs, typename Rhs>
	auto constexpr operator+(type::vector<Lhs> const& lhs, type::vector<Rhs> const& rhs) -> decltype(auto) {
		return make_plus<element_wise::vector_expression>(lhs, rhs);
	}

	template <typename Lhs, typename Rhs>
	auto constexpr operator-(type::vector<Lhs> const& lhs, type::vector<Rhs> const& rhs) -> decltype(auto) {
		return make_minus<element_wise::vector_expression>(lhs, rhs);
	}

	namespace element_wise {

		template <typename Lhs, typename Rhs>
		auto constexpr operator*(vector<Lhs> const& lhs, type::vector<Rhs> const& rhs) -> decltype(auto) {
			return make_multiplies<vector_expression>(lhs, rhs);
		}

		template <typename Lhs, typename Rhs>
		auto constexpr operator*(type::vector<Lhs> const& lhs, vector<Rhs> const& rhs) -> decltype(auto) {
			return rhs * lhs;
		}

		template <typename Lhs, typename Rhs>
		auto constexpr operator/(vector<Lhs> const& lhs, type::vector<Rhs> const& rhs) -> decltype(auto) {
			return make_divides<vector_expression>(lhs, rhs);
		}

		template <typename Lhs, typename Rhs>
		auto constexpr operator/(type::vector<Lhs> const& lhs, vector<Rhs> const& rhs) -> decltype(auto) {
			return make_divides<vector_expression>(lhs, rhs);
		}

		template <typename Lhs, typename Rhs>
		auto constexpr operator%(vector<Lhs> const& lhs, type::vector<Rhs> const& rhs) -> decltype(auto) {
			return make_modulus<vector_expression>(lhs, rhs);
		}

		template <typename Lhs, typename Rhs>
		auto constexpr operator%(type::vector<Lhs> const& lhs, vector<Rhs> const& rhs) -> decltype(auto) {
			return make_modulus<vector_expression>(lhs, rhs);
		}

	}  // namespace element_wise

}  // namespace nxx::expressions

#endif  // ifndef NXX__EXPRESSION_HXX
#endif  // ifndef NXX__EXPRESSION_OPERATORS_HXX
