#ifndef NXX__EXPRESSION_INTERFACE_HXX
#define NXX__EXPRESSION_INTERFACE_HXX
#ifndef NXX__EXPRESSION_HXX
#	error "please only include nxx/expression.hxx; this is an implementation header"
#else  // ifndef NXX__EXPRESSION_HXX

namespace nxx::expressions::type {

	// scalar<T> := tensor<tensor_order::scalar, T>

	template <typename T>
	struct tensor<tensor_order::scalar, T> {
		auto constexpr static order = tensor_order::scalar;
		using reference = typename traits::tensor<std::remove_reference_t<T>>::reference;
		using const_reference = typename traits::tensor<std::remove_reference_t<T>>::const_reference;

		auto constexpr operator()() -> decltype(auto) { return static_cast<T&>(*this)(); }
		auto constexpr operator()() const -> decltype(auto) { return static_cast<T const&>(*this)(); }
	};


	// vector<T> := tensor<tensor_order::vector, T>

	template <typename T>
	class tensor<tensor_order::vector, T> {
	private:
		using traits_t = traits::tensor<std::remove_reference_t<T>>;

	public:
		auto constexpr static order = tensor_order::vector;
		using size_type = typename traits_t::size_type;
		using reference = typename traits_t::reference;
		using const_reference = typename traits_t::const_reference;
		using iterator = typename traits_t::iterator;
		using const_iterator = typename traits_t::const_iterator;

		auto constexpr size() const -> size_type { return static_cast<T const&>(*this).size(); }

		auto constexpr begin() -> decltype(auto) { return static_cast<T const&>(*this).begin(); }
		auto constexpr begin() const -> decltype(auto) { return static_cast<T const&>(*this).begin(); }
		auto constexpr cbegin() const -> decltype(auto) { return static_cast<T const&>(*this).cbegin(); }

		auto constexpr end() -> decltype(auto) { return static_cast<T const&>(*this).end(); }
		auto constexpr end() const -> decltype(auto) { return static_cast<T const&>(*this).end(); }
		auto constexpr cend() const -> decltype(auto) { return static_cast<T const&>(*this).cend(); }

		auto constexpr operator[](size_type i) -> decltype(auto) { return static_cast<T&>(*this)[i]; }
		auto constexpr operator[](size_type i) const -> decltype(auto) {
			return static_cast<T const&>(*this)[i];
		}

		auto constexpr operator()() -> decltype(auto) { return static_cast<T&>(*this)(); }
		auto constexpr operator()() const -> decltype(auto) { return static_cast<T const&>(*this)(); }

		template <typename Container,
		          std::enable_if_t<std::is_convertible_v<
		                               typename std::iterator_traits<const_iterator>::reference,
		                               nxx::traits::detected_t<nxx::traits::has_value_type_t, Container>>,
		                           int> = 0>
		constexpr operator Container() const {
			return Container(begin(), end());
		}

		template <
		    typename V, std::size_t N,
		    std::enable_if_t<
		        std::is_convertible_v<typename std::iterator_traits<const_iterator>::reference, V>, int> = 0>
		constexpr operator std::array<V, N>() const {
			assert(N == size());
			auto array = std::array<V, N>{};
			std::copy(begin(), end(), std::begin(array));
			return array;
		}
	};

}  // namespace nxx::expressions::type

#endif  // ifndef NXX__EXPRESSION_HXX
#endif  // ifndef NXX__EXPRESSION_INTERFACE_HXX
