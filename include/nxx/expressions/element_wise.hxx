#ifndef NXX__EXPRESSION_ELEMENT_WISE_HXX
#define NXX__EXPRESSION_ELEMENT_WISE_HXX
#ifndef NXX__EXPRESSION_HXX
#	error "please only include nxx/expression.hxx; this is an implementation header"
#else  // ifndef NXX__EXPRESSION_HXX

namespace nxx::expressions {

	// scalar

	template <typename F, typename... Ts>
	struct traits::tensor<element_wise::scalar_expression<expression<F, Ts...>>> {
		using reference = std::invoke_result_t<F, typename tensor<std::remove_reference_t<Ts>>::reference...>;
		using const_reference
		    = std::invoke_result_t<F, typename tensor<std::remove_reference_t<Ts>>::const_reference...>;
	};

	namespace element_wise {

		template <typename E>
		struct scalar_expression
		    : std::enable_if_t<nxx::traits::is_specialization_of_v<expression, E>, E>
		    , type::tensor<type::tensor_order::scalar, scalar_expression<E>> {

			using E::E;  // constructors

			auto constexpr operator()() const {
				return std::apply(
				    this->m_f,
				    nxx::tuple::transform([](auto const& t) -> decltype(auto) { return t(); }, this->m_ts));
			}
		};

		template <typename F, typename... Ts>
		scalar_expression(F&&, Ts&&...)->scalar_expression<expression<F, Ts...>>;

		template <typename F, typename... Ts>
		auto constexpr make_scalar_expression(F&& f, Ts&&... ts) {
			return scalar_expression<expression<F, Ts...>>{std::forward<F>(f), std::forward<Ts>(ts)...};
		}

		template <typename F, typename... Ts>
		auto constexpr make_scalar_expression(Ts&&... ts) {
			return scalar_expression<expression<F, Ts...>>{std::forward<Ts>(ts)...};
		}

	}  // namespace element_wise


	// vector

	template <typename F, typename... Ts>
	class traits::tensor<element_wise::vector_expression<expression<F, Ts...>>> {
	private:
		using expression_t = element_wise::vector_expression<expression<F, Ts...>>;

	public:
		using size_type = std::common_type_t<typename tensor<std::remove_reference_t<Ts>>::size_type...>;
		using reference = std::invoke_result_t<F, typename tensor<std::remove_reference_t<Ts>>::reference...>;
		using const_reference
		    = std::invoke_result_t<F, typename tensor<std::remove_reference_t<Ts>>::const_reference...>;
		using iterator = nxx::iterator::subscript<expression_t, reference, size_type>;
		using const_iterator = nxx::iterator::subscript<expression_t const, const_reference, size_type>;
	};

	namespace element_wise {

		template <typename E>
		struct vector_expression
		    : std::enable_if_t<nxx::traits::is_specialization_of_v<expression, E>, E>
		    , type::tensor<type::tensor_order::vector, vector_expression<E>> {

			using size_type = typename vector_expression::size_type;
			using iterator = typename vector_expression::iterator;
			using const_iterator = typename vector_expression::const_iterator;
			using E::E;  // constructors

			auto constexpr size() const -> size_type {
				return nxx::tuple::transform_reduce(
				    this->m_ts, [](auto const& lhs, auto const& rhs) { return std::min(lhs, rhs); },
				    [](auto const& t) { return t.size(); });
			}

			auto constexpr begin() { return iterator{*this}; }
			auto constexpr begin() const { return const_iterator{*this}; }
			auto constexpr cbegin() const { return begin(); }

			auto constexpr end() { return iterator{}; }
			auto constexpr end() const { return const_iterator{}; }
			auto constexpr cend() const { return end(); }

			auto constexpr operator[](size_type i) const {
				return std::apply(
				    this->m_f,
				    nxx::tuple::transform([i](auto const& t) -> decltype(auto) { return t[i]; }, this->m_ts));
			}

			auto constexpr operator()() const { return std::vector(begin(), end()); }
		};

		template <typename F, typename... Ts>
		vector_expression(F&&, Ts&&...)->vector_expression<expression<F, Ts...>>;

		template <typename F, typename... Ts>
		auto constexpr make_vector_expression(F&& f, Ts&&... ts) {
			return vector_expression<expression<F, Ts...>>{std::forward<F>(f), std::forward<Ts>(ts)...};
		}

		template <typename F, typename... Ts>
		auto constexpr make_vector_expression(Ts&&... ts) {
			return vector_expression<expression<F, Ts...>>{std::forward<Ts>(ts)...};
		}

	}  // namespace element_wise
}  // namespace nxx::expressions

#endif  // ifndef NXX__EXPRESSION_HXX
#endif  // ifndef NXX__EXPRESSION_ELEMENT_WISE_HXX
