#ifndef NXX__EXPRESSION_WRAPPERS_HXX
#define NXX__EXPRESSION_WRAPPERS_HXX
#ifndef NXX__EXPRESSION_HXX
#	error "please only include nxx/expression.hxx; this is an implementation header"
#else  // ifndef NXX__EXPRESSION_HXX

namespace nxx::expressions {

	// scalar

	template <typename T>
	struct traits::tensor<scalar<T>> {
		using reference
		    = nxx::traits::detected_or_t<T&, nxx::traits::has_reference_t, std::remove_reference_t<T>>;
		using const_reference = nxx::traits::detected_or_t<T const&, nxx::traits::has_const_reference_t,
		                                                   std::remove_reference_t<T>>;
	};

	template <typename T>
	class scalar : public type::scalar<scalar<T>> {
	public:
		constexpr scalar() = default;

		explicit constexpr scalar(T&& scalar)
		    : m_scalar{std::forward<T>(scalar)} {}

		constexpr operator T&() { return m_scalar; }
		constexpr operator T const&() const { return m_scalar; }

		auto constexpr operator()() -> T& { return m_scalar; }
		auto constexpr operator()() const -> T const& { return m_scalar; }

	private:
		T m_scalar{};
	};

	template <typename T>
	scalar(T &&)->scalar<T>;


	// vector

	template <typename T>
	struct traits::tensor<vector<T>> {
		using size_type = nxx::traits::detected_or_t<std::ptrdiff_t, nxx::traits::has_size_type_t,
		                                             std::remove_reference_t<T>>;
		using reference
		    = nxx::traits::detected_or_t<decltype(std::declval<T>()[std::declval<size_type>()]),
		                                 nxx::traits::has_reference_t, std::remove_reference_t<T>>;
		using const_reference
		    = nxx::traits::detected_or_t<decltype(std::declval<T const>()[std::declval<size_type>()]),
		                                 nxx::traits::has_const_reference_t, std::remove_reference_t<T>>;
		using iterator = nxx::traits::detected_or_t<nxx::iterator::subscript<vector<T>, reference, size_type>,
		                                            nxx::traits::has_iterator_t, std::remove_reference_t<T>>;
		using const_iterator = nxx::traits::detected_or_t<
		    nxx::iterator::subscript<vector<T> const, const_reference, size_type>,
		    nxx::traits::has_const_iterator_t, std::remove_reference_t<T>>;
	};

	template <typename T>
	class vector : public type::vector<vector<T>> {
	public:
		using size_type = typename vector::size_type;
		using iterator = typename vector::iterator;
		using const_iterator = typename vector::const_iterator;

		constexpr vector() = default;

		explicit constexpr vector(T&& vector)
		    : m_vector{std::forward<T>(vector)} {}

		template <typename S, typename... Ss,
		          std::enable_if_t<std::is_same_v<std::array<S, 1 + sizeof...(Ss)>, T>, int> = 0>
		explicit constexpr vector(S&& scalar, Ss&&... scalars)
		    : m_vector{scalar, scalars...} {}

		constexpr operator T&() { return m_vector; }
		constexpr operator T const&() const { return m_vector; }

		auto constexpr size() const -> size_type { return m_vector.size(); };

		auto constexpr begin() {
			if constexpr (std::is_same_v<iterator, nxx::iterator::subscript<vector>>)
				return iterator{*this};
			else
				return std::begin(m_vector);
		}

		auto constexpr begin() const {
			if constexpr (std::is_same_v<const_iterator, nxx::iterator::subscript<vector const>>)
				return const_iterator{*this};
			else
				return std::cbegin(m_vector);
		}

		auto constexpr cbegin() const { return begin(); }

		auto constexpr end() {
			if constexpr (std::is_same_v<iterator, nxx::iterator::subscript<vector>>)
				return iterator{};
			else
				return std::end(m_vector);
		}

		auto constexpr end() const {
			if constexpr (std::is_same_v<const_iterator, nxx::iterator::subscript<vector const>>)
				return const_iterator{};
			else
				return std::cend(m_vector);
		}

		auto constexpr cend() const { return end(); }

		auto constexpr operator[](size_type i) -> decltype(auto) { return m_vector[i]; }
		auto constexpr operator[](size_type i) const -> decltype(auto) { return m_vector[i]; }

		auto constexpr operator()() -> T& { return m_vector; }
		auto constexpr operator()() const -> T const& { return m_vector; }

	private:
		T m_vector{};
	};

	template <typename T>
	vector(T &&)->vector<T>;

	template <typename T, typename... Ts>
	vector(T&&, Ts&&...)->vector<std::array<T, 1 + sizeof...(Ts)>>;


	// scalar_as_vector

	template <typename T>
	struct traits::tensor<scalar_as_vector<T>> {
		using size_type = std::size_t;
		using reference = typename traits::tensor<std::remove_reference_t<T>>::reference;
		using const_reference = typename traits::tensor<std::remove_reference_t<T>>::const_reference;
		using iterator = nxx::iterator::subscript<scalar_as_vector<T>, reference, size_type>;
		using const_iterator
		    = nxx::iterator::subscript<scalar_as_vector<T> const, const_reference, size_type>;
	};

	template <typename T>
	class scalar_as_vector : public type::vector<scalar_as_vector<T>> {
	public:
		using size_type = typename scalar_as_vector::size_type;
		using iterator = typename scalar_as_vector::iterator;
		using const_iterator = typename scalar_as_vector::const_iterator;

		constexpr scalar_as_vector() = default;

		template <typename U>
		explicit constexpr scalar_as_vector(U&& scalar)
		    : m_scalar{std::forward<U>(scalar)} {}

		auto constexpr size() const -> size_type { return -1; };

		auto constexpr begin(size_type n) { return iterator{*this, n}; }
		auto constexpr begin() { return begin(size()); }

		auto constexpr begin(size_type n) const { return const_iterator{*this, n}; }
		auto constexpr begin() const { return begin(size()); }

		auto constexpr cbegin(size_type n) const { return begin(n); }
		auto constexpr cbegin() const { return begin(); }

		auto constexpr end() { return iterator{}; }
		auto constexpr end() const { return const_iterator{}; }
		auto constexpr cend() const { return end(); }

		auto constexpr operator[](size_type) -> decltype(auto) { return m_scalar(); }
		auto constexpr operator[](size_type) const -> decltype(auto) { return m_scalar(); }

		auto constexpr operator()() -> decltype(auto) { return m_scalar(); }
		auto constexpr operator()() const -> decltype(auto) { return m_scalar(); }

	private:
		T m_scalar{};
	};

	template <typename T>
	scalar_as_vector(type::scalar<T>&)->scalar_as_vector<type::scalar<T>&>;

	template <typename T>
	scalar_as_vector(type::scalar<T> const&)->scalar_as_vector<type::scalar<T> const&>;

	template <typename T>
	scalar_as_vector(type::scalar<T> &&)->scalar_as_vector<type::scalar<T>>;

	template <typename T>
	scalar_as_vector(T &&)->scalar_as_vector<scalar<T>>;


	// element_wise::vector

	template <typename T>
	struct traits::tensor<element_wise::vector<T>> : traits::tensor<vector<T>> {
		using size_type = typename tensor::size_type;
		using reference = typename tensor::reference;
		using const_reference = typename tensor::const_reference;

		using iterator = nxx::traits::detected_or_t<
		    nxx::iterator::subscript<element_wise::vector<T>, reference, size_type>,
		    nxx::traits::has_iterator_t, std::remove_reference_t<T>>;

		using const_iterator = nxx::traits::detected_or_t<
		    nxx::iterator::subscript<element_wise::vector<T> const, const_reference, size_type>,
		    nxx::traits::has_const_iterator_t, std::remove_reference_t<T>>;
	};

	namespace element_wise {

		template <typename T>
		class vector
		    : nxx::expressions::vector<T>
		    , type::vector<vector<T>> {
		private:
			using vector_t = nxx::expressions::vector<T>;
			using traits_t = traits::tensor<vector>;

		public:
			using size_type = typename traits_t::size_type;
			using reference = typename traits_t::reference;
			using const_reference = typename traits_t::const_reference;
			using iterator = typename traits_t::iterator;
			using const_iterator = typename traits_t::const_iterator;

			using vector_t::vector;  // contructors

			using vector_t::begin;
			using vector_t::cbegin;
			using vector_t::cend;
			using vector_t::end;
			using vector_t::size;
			using vector_t::operator[];
			using vector_t::operator();
		};

		template <typename T>
		vector(T &&)->vector<T>;

		template <typename T, typename... Ts>
		vector(T&&, Ts&&...)->vector<std::array<T, 1 + sizeof...(Ts)>>;

	}  // namespace element_wise

}  // namespace nxx::expressions

#endif  // ifndef NXX__EXPRESSION_HXX
#endif  // ifndef NXX__EXPRESSION_WRAPPERS_HXX
