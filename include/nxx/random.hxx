#ifndef NXX__RANDOM_HXX
#define NXX__RANDOM_HXX

#include <cstddef>
#include <cstdint>
#include <initializer_list>
#include <iostream>
#include <limits>
#include <type_traits>
#include <utility>

#include <nxx/type_traits.hxx>

namespace nxx::random {

	namespace detail {

		template <typename T>
		using has_equality = decltype(std::declval<T&>() == std::declval<T&>());

		template <typename T>
		using has_not_equality = decltype(std::declval<T&>() != std::declval<T&>());

		template <typename T>
		using has_callable = decltype(std::declval<T&>()());

		template <typename T>
		using has_out_stream = decltype(std::declval<std::ostream&>() << std::declval<T&>());

		template <typename T>
		using has_in_stream = decltype(std::declval<std::istream&>() >> std::declval<T&>());

		template <typename T>
		using has_size = decltype(std::declval<T&>().size());

		template <typename T>
		using has_min = decltype(std::declval<T&>().min());

		template <typename T>
		using has_max = decltype(std::declval<T&>().max());

		template <typename T>
		using has_seed_void = decltype(std::declval<T&>().seed());

		template <typename T, typename Arg>
		using has_seed = decltype(std::declval<T&>().seed(std::declval<Arg>()));

		template <typename T>
		using has_discard = decltype(std::declval<T&>().seed(std::declval<unsigned long long>()));

	}  // namespace detail

	namespace traits {

		template <typename G, typename = void>
		struct is_uniform_random_bit_generator : std::false_type {};

		template <typename G>
		struct is_uniform_random_bit_generator<
		    G, typename std::enable_if<
		           // G::result_type
		           //     unsigned integers
		           std::is_unsigned<typename G::result_type>::value
		           // G::min()
		           && nxx::traits::is_detected_exact<typename G::result_type, detail::has_min, G>::value
		           // G::max()
		           && nxx::traits::is_detected_exact<typename G::result_type, detail::has_max, G>::value
		           // G::operator()
		           //     return value in [G::min(), G::max()]
		           && nxx::traits::is_detected_exact<typename G::result_type, detail::has_callable,
		                                             G>::value>::type> : std::true_type {};

		template <typename E, typename = void>
		struct is_random_number_engine : std::false_type {};

		template <typename E>
		struct is_random_number_engine<
		    E,
		    typename std::enable_if<
		        // RandomNumberEngine satisfies UniformRandomBitGenerator
		        is_uniform_random_bit_generator<E>::value
		        // E()
		        && std::is_default_constructible<E>::value
		        // E(E [const?])
		        && std::is_copy_constructible<E>::value
		        // E(E::result_type)
		        && std::is_constructible<E, typename E::result_type>::value
		        // TODO: E(SeedSequence)
		        //
		        // void E::seed()
		        && nxx::traits::is_detected_exact<void, detail::has_seed_void, E>::value
		        // void E::seed(E::result_type)
		        && nxx::traits::is_detected_exact<void, detail::has_seed, E, typename E::result_type>::value
		        // TODO void E::seed(SeedSequence)
		        //
		        // void E::discard(unsigned long long)
		        && nxx::traits::is_detected_exact<void, detail::has_discard, E>::value
		        // bool operator== [const?]
		        && nxx::traits::is_detected_exact<void, detail::has_equality, E>::value
		        // bool operator!= [const?]
		        && nxx::traits::is_detected_exact<void, detail::has_not_equality, E>::value
		        // OutputStream& operator<<
		        && nxx::traits::is_detected_exact<void, detail::has_out_stream, E>::value
		        // InputStream& operator>>
		        && nxx::traits::is_detected_exact<void, detail::has_in_stream, E>::value>::type>
		    : std::true_type {};

		template <typename S, typename = void, typename = void>
		struct is_seed_sequence : std::false_type {};

		template <typename S>
		struct is_seed_sequence<
		    S, typename std::enable_if<std::is_class<S>::value>::type,
		    typename std::enable_if<
		        // S::result_type
		        //     unsigned integers values with at least 32 bit
		        std::is_unsigned<typename S::result_type>::value
		        && std::numeric_limits<typename S::result_type>::max()
		               >= std::numeric_limits<std::uint32_t>::max()

		        // S()
		        && std::is_default_constructible<S>::value

		        // TODO S(InputIterator, InputIterator)
		        //     with: InputIterator::value_type
		        //           unsigned integers values with at least 32 bit
		        //

		        // S(std::initializer_list<S::result_type>)
		        && std::is_constructible<S, std::initializer_list<typename S::result_type>>::value

		        // TODO void S::generate(RandomAccessIterator, RandomAccessIterator)
		        //     with: RandomAccessIterator::value_type
		        //           unsigned integers values with at least 32 bit
		        //

		        // std::size_t S::size() [const?]
		        && nxx::traits::is_detected_exact<std::size_t, detail::has_size, S>::value

		        // TODO void S::param(OutputIterator) [const?]
		        //
		        >::type> : std::true_type {};


		template <typename S, typename Engine, typename = void>
		struct is_random_number_engine_seed_sequence : std::false_type {};

		template <typename S, typename Engine>
		struct is_random_number_engine_seed_sequence<
		    S, Engine,
		    typename std::enable_if<
		        // FIXME: find away to include the traits
		        // is_seed_sequence<S>::value
		        // is_random_number_engine<Engine>::value
		        !std::is_convertible<S, typename Engine::result_type>::value
		        && !std::is_same<typename std::remove_cv<S>::type, Engine>::value>::type> : std::true_type {};

	}  // namespace traits
}  // namespace nxx::random

#endif  //  #ifndef NXX__RANDOM_HXX
