#ifndef NXX__EXPRESSION_HXX
#define NXX__EXPRESSION_HXX

#include <cstddef>
#include <functional>

namespace nxx::expressions {
	namespace type {

		enum class tensor_order : std::size_t {
			scalar = 0,
			vector = 1,
		};

		template <tensor_order Order, typename T>
		struct tensor {
			auto constexpr static order = Order;
		};

		template <typename T>
		using scalar = tensor<tensor_order::scalar, T>;

		template <typename T>
		using vector = tensor<tensor_order::vector, T>;

	}  // namespace type


	template <typename>
	class scalar;

	template <typename>
	class vector;

	template <typename>
	class scalar_as_vector;

	namespace element_wise {

		template <typename>
		class vector;

	}  // element_wise


	// expressions

	template <typename, typename...>
	class expression;

	template <typename Lhs, typename Rhs, typename T = void>
	using plus = expression<std::plus<T>, Lhs, Rhs>;

	template <typename Lhs, typename Rhs, typename T = void>
	using minus = expression<std::minus<T>, Lhs, Rhs>;

	template <typename Lhs, typename Rhs, typename T = void>
	using multiplies = expression<std::multiplies<T>, Lhs, Rhs>;

	template <typename Lhs, typename Rhs, typename T = void>
	using divides = expression<std::divides<T>, Lhs, Rhs>;

	template <typename Lhs, typename Rhs, typename T = void>
	using modulus = expression<std::modulus<T>, Lhs, Rhs>;

	template <typename V, typename T = void>
	using negate = expression<std::negate<T>, V>;


	namespace element_wise {

		template <typename>
		struct scalar_expression;

		template <typename>
		struct vector_expression;

	}  // namespace element_wise

	template <typename E>
	using scalar_expression = element_wise::scalar_expression<E>;


}  // namespace nxx::expressions

#include <array>
#include <cassert>
#include <cmath>
#include <iterator>
#include <tuple>
#include <type_traits>
#include <utility>

#include <nxx/functional.hxx>
#include <nxx/iterator.hxx>
#include <nxx/tuple.hxx>
#include <nxx/type_traits.hxx>

// clang-format off
#include <nxx/expressions/type_traits.hxx>
#include <nxx/expressions/interface.hxx>
#include <nxx/expressions/wrappers.hxx>
#include <nxx/expressions/expression.hxx>
#include <nxx/expressions/element_wise.hxx>
#include <nxx/expressions/operators.hxx>

#endif  // ifndef NXX__EXPRESSION_HXX
