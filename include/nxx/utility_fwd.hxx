#ifndef NXX__UTIL_FWD_HXX
#define NXX__UTIL_FWD_HXX

#include <cstddef>

namespace nxx::util {
	namespace detail {

		template <std::size_t...>
		struct index_sequence_generator;

		template <std::size_t... Is>
		using index_sequence_generator_t = typename index_sequence_generator<Is...>::type;

	}  // namespace detail

	// (B < E) : {B, B + 1, B + 2, ... E - 1}
	// (B > E) : {E - 1, E - 2, E - 3, ..., B}
	// (B = E) : {}
	template <std::size_t B, std::size_t E>
	using make_index_sequence_from_range = detail::index_sequence_generator_t<B, E>;

}  // namespace nxx::util


// refactor
namespace nxx {

	template <typename>
	class fake_ptr;

	template <typename>
	class call_counter;

}  // namespace nxx

#endif  // #ifndef NXX__UTIL_FWD_HXX
