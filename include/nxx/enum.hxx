#ifndef NXX__ENUM_HXX
#define NXX__ENUM_HXX

#include <type_traits>

namespace nxx::underlying_enum {

	template <typename T>
	struct enable : std::false_type {};

	template <typename T>
	bool constexpr enable_v = enable<T>::value;

	namespace traits {

		template <typename T>
		using is_underlying_enum = std::conjunction<std::is_enum<T>, enable<T>>;

		template <typename T>
		bool constexpr is_underlying_enum_v = is_underlying_enum<T>::value;

	}  // namespace traits
}  // namespace nxx::underlying_enum

#define NXX_ENABLE_UNDERLYING_ENUM(enum_t)                                                                   \
	template <>                                                                                              \
	struct nxx::underlying_enum::enable<enum_t> : std::true_type {}

template <typename Enum,
          typename = std::enable_if_t<nxx::underlying_enum::traits::is_underlying_enum_v<Enum>>>
auto constexpr operator*(Enum const e) noexcept {
	return static_cast<std::underlying_type_t<Enum>>(e);
}

#endif  // ifndef NXX__ENUM_HXX
