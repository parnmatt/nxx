#ifndef NXX__UTILITY_HXX
#define NXX__UTILITY_HXX

#include <cstddef>
#include <utility>

#include <nxx/functional_fwd.hxx>
#include <nxx/utility_fwd.hxx>

namespace nxx::util::detail {

	template <std::size_t I, std::size_t E, std::size_t... Is>
	class index_sequence_generator<I, E, Is...> {
	private:
		auto constexpr static type_helper() noexcept {
			if constexpr (I < E)
				// increment
				return index_sequence_generator_t<I + 1, E, Is..., I>{};
			else
				// decrement
				return index_sequence_generator_t<I - 1, E, Is..., I - 1>{};
		}

	public:
		using type = std::invoke_result_t<decltype(type_helper)>;
	};

	template <std::size_t E, std::size_t... Is>
	struct index_sequence_generator<E, E, Is...> {
		using type = std::index_sequence<Is...>;
	};

}  // namespace nxx::util::detail


// refactor
namespace nxx {

	template <typename T>
	class fake_ptr {
	public:
		using element_type = T;

		fake_ptr()
		    : m_v{}
		    , m_ptr{} {}

		fake_ptr(fake_ptr const& fp)
		    : m_ptr{fp.m_ptr} {
			if (m_ptr)
				m_p = fp.m_p;
			else
				m_v = fp.m_v;
		}

		fake_ptr(T* p)
		    : m_p{p}
		    , m_ptr{true} {}

		fake_ptr(T const& v)
		    : m_v{v}
		    , m_ptr{false} {}

		~fake_ptr() {}

		auto get() -> T* { return m_ptr ? m_p : &m_v; }
		auto get() const -> T const* { return get(); }

		auto operator*() -> T& { return *get(); }
		auto operator*() const -> T const& { return *get(); }
		auto operator-> () -> T* { return get(); }
		auto operator-> () const -> T const* { return get(); }

		template <typename T1, typename T2>
		friend auto operator==(fake_ptr<T1> const& lhs, fake_ptr<T2> const& rhs) -> bool {
			return lhs.get() == rhs.get();
		}

		template <typename T1, typename T2>
		friend auto operator!=(fake_ptr<T1> const& lhs, fake_ptr<T2> const& rhs) -> bool {
			return !(lhs == rhs);
		}

	private:
		bool m_ptr{};
		union {
			T* m_p;
			T m_v;
		};
	};


	template <typename F>
	class call_counter {
	public:
		constexpr call_counter() = default;

		explicit constexpr call_counter(F&& f)
		    : m_f{std::forward<F>(f)} {}

		template <typename... Args>
		explicit constexpr call_counter(Args&&... args)
		    : m_f(std::forward<Args>(args)...) {}

		auto constexpr n_calls() const { return m_n_calls; }

		template <typename... Args>
		auto constexpr operator()(Args&&... args) -> decltype(auto) {
			++m_n_calls;
			return functional::invoke(m_f, std::forward<Args>(args)...);
		}

	private:
		F m_f;
		std::size_t m_n_calls{};
	};

}  // namespace nxx

#include <nxx/functional.hxx>

#endif  // #ifndef NXX__UTILITY_HXX
