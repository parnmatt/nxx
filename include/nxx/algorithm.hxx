#ifndef NXX__ALGORITHM_HXX
#define NXX__ALGORITHM_HXX

#include <iterator>
#include <utility>

#include <nxx/functional.hxx>

namespace nxx {

	template <typename InputIt, typename Sentinel, typename OutputIt>
	auto constexpr copy(InputIt first, Sentinel last, OutputIt d_first) {
		while (first != last)
			*d_first++ = *first++;
		return d_first;
	}

	template <typename Size, typename Callable, typename... Args>
	void repeat_n(Size count, Callable&& f, Args&&... args) {
		for (Size i{}; i < count; ++i)
			functional::invoke(std::forward<Callable>(f), std::forward<Args>(args)...);
	}

}  // namespace nxx

#endif  // #ifndef NXX__ALGORITHM_HXX
